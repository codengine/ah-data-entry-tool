from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        if not User.objects.filter(username="admin").exists():
            User.objects.create_superuser("admin", "admin@admin.com", "admin")

        if not User.objects.filter(username="sohel").exists():
            User.objects.create_superuser("sohel", "sohel@admin.com", "lapsso065")

        if not User.objects.filter(username="sajjad").exists():
            User.objects.create_superuser("sajjad", "sajjad@admin.com", "P@ssc0de")