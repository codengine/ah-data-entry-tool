from core.models.location_entity import LocationEntity


class City(LocationEntity):
    class Meta:
        app_label = 'core'