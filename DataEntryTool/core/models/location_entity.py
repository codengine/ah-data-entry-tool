from django.db import models
from core.models.base_entity import BaseEntity


class LocationEntity(BaseEntity):
    name = models.CharField(max_length=200)
    short_name = models.CharField(max_length=200)
    parent = models.ForeignKey('self', null=True, blank=True)

    class Meta:
        abstract = True