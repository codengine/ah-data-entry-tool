from core.models.location_entity import LocationEntity


class Country(LocationEntity):
    class Meta:
        app_label = 'core'