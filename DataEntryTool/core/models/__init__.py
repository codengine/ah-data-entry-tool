__author__ = "auto generated"

from core.models.base_entity import BaseEntity
from core.models.code_pointer import CodePointer


__all__ = ['CodePointer']
__all__ += ['BaseEntity']
