from datetime import datetime
from django.db import models
from django.contrib.auth.models import User

from core.manager.modelmanager.base_entity_manager import BaseEntityModelManager
from core.models.code_pointer import CodePointer
from engine.clock import Clock


class BaseEntity(models.Model):
    code = models.CharField(max_length=50)
    date_created = models.DateTimeField()
    last_updated = models.DateTimeField()
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, related_name='+', null=True)
    last_updated_by = models.ForeignKey(User, related_name='+', null=True)

    objects = BaseEntityModelManager(filter={"is_deleted": False})

    def date_created_verbose(self):
        return self.date_created.strftime("%d/%m/%Y")

    def get_code_prefix(self):
        prefix = ''.join([c for c in self.__class__.__name__ if c.isupper()])
        return prefix if prefix else self.__class__.__name__

    def field_name_as_verbose(self, field_name):
        return " ".join(field_name.split("_"))

    def as_text(self, fields=[], m2m_fields={}, date_formats={}, labels={}):
        text = ""
        m2m_names = m2m_fields.keys()
        for field in fields:
            field_split_colon = field.split(":")
            field_label = None
            if len(field_split_colon) >= 2:
                field = field_split_colon[0]
                field_label = self.field_name_as_verbose(field_split_colon[1])
            if "__" in field:
                field_split = field.split("__")
                f_key = field_split[0]
                f_key_field_name = field_split[1]
                f_field = getattr(self, f_key)
                if f_field:
                    f_field_value = getattr(f_field, f_key_field_name)
                    if field_label:
                        text += field_label + ": " + f_field_value if f_field_value else ""
                        text += "\n"
                    else:
                        value = self.field_name_as_verbose(f_key) + " " + self.field_name_as_verbose(f_key_field_name) + ":" + f_field_value if f_field_value else ""
                        text += value + "\n"
                else:
                    if field_label:
                        text += field_label + ":" + " \n"
                    else:
                        text += self.field_name_as_verbose(f_key) + " " + self.field_name_as_verbose(f_key_field_name) + ": \n"
            else:
                if field in m2m_names:
                    if field_label:
                        text += field_label + ":\n"
                    else:
                        text += self.field_name_as_verbose(field.capitalize()) + ":\n"
                    m2m_cols = m2m_fields.get(field)
                    m2m_field = getattr(self, field)
                    m2m_objects = m2m_field.all()
                    for m2m_object in m2m_objects:
                        text += m2m_object.as_text(fields=m2m_cols) + "\n"
                else:
                    value = getattr(self, field)
                    if value:
                        date_fmt_found = False
                        for field_name, date_format in date_formats.items():
                            if field_name == field:
                                date_fmt_found = True
                                value_type = type(value)
                                now_date = datetime.now().date()
                                datetime_instance = type(value) == type(now_date)
                                if datetime_instance:
                                    value = value.strftime(date_format)
                                    if field_label:
                                        text += field_label + ": " + value + "\n"
                                    else:
                                        text += self.field_name_as_verbose(field) + ": "+ value + "\n"
                                else:
                                    datetime_instance = datetime.strptime(str(value), date_format)
                                    value = datetime_instance.strftime(date_format)
                                    if field_label:
                                        text += field_label + ": " + value + "\n"
                                    else:
                                        text += self.field_name_as_verbose(field) + ": "+ value + "\n"
                        if not date_fmt_found:
                            if field_label:
                                text += field_label + ": " + str(value) + "\n"
                            else:
                                text += self.field_name_as_verbose(field) + ": "+ str(value) + "\n"
                    else:
                        if field_label:
                            text += field_label + ":\n"
                        else:
                            text += self.field_name_as_verbose(field) + ":\n"
        return text
        
    def as_json(self, fields=[], date_formats={}):
        data = {}
        try:
            for field in fields:
                value = getattr(self, field)
                if value:
                    for field_name, date_format in date_formats.items():
                        if field_name == field:
                            value_type = type(value)
                            now_date = datetime.now().date()
                            datetime_instance = type(value) == type(now_date)
                            if datetime_instance:
                                value = value.strftime(date_format)
                                data[field] = value
                            else:
                                datetime_instance = datetime.strptime(str(value), date_format)
                                value = datetime_instance.strftime(date_format)
                                data[field] = value
                    else:
                        data[field] = str(value)
                else:
                    data[field] = ""
        except Exception as exp:
            return {}
        return data

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if not self.pk:
            self.date_created = Clock.utc_datetime()

            last_code_objects = CodePointer.objects.filter(model_name=self.__class__.__name__)
            if not last_code_objects.exists():
                last_code_object = CodePointer()
                last_code_object.model_name = self.__class__.__name__
                last_code_object.value = 1
                last_code_object.save()
            else:
                last_code_object = last_code_objects.first()
                last_code_object.model_name = self.__class__.__name__
                last_code_object.value += 1
                last_code_object.save()

            self.code = self.get_code_prefix() + "-" + str(format(last_code_object.value, '08d'))

        self.last_updated = Clock.utc_datetime()
        super(BaseEntity, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        abstract = True