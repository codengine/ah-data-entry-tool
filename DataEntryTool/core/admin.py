from django.contrib import admin
from core.models.city import City
from core.models.country import Country

admin.site.register(City)
admin.site.register(Country)