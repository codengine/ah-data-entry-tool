import json
from django.contrib.auth.models import User
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models.aggregates import Count
from django.db.models.query_utils import Q
from django.http.response import HttpResponseRedirect, HttpResponse
from django.urls.base import reverse
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from ahdms.forms.modelforms.document_upload_model_form import DocumentUploadModelForm
from ahdms.forms.modelforms.radiology_forms import RadiologyModelForm
from ahdms.forms.modelforms.signup_form import UserSignupForm
from ahdms.helpers.aws_s3 import AWSS3
from ahdms.helpers.enums import DocumentStatus, DocumentApprovalSource
from ahdms.helpers.manage import MenuHelper
from ahdms.managers.form_manager import FormManager
from ahdms.models.aws_resource import AWSResource
from ahdms.models.aws_resource_archive import AWSResourceArchive
from ahdms.models.doctors_note import DoctorsNote
from ahdms.models.document_type import DocumentType
from ahdms.models.document_upload import DocumentUpload
from ahdms.models.laboratory import Laboratory
from ahdms.models.order_ticket import OrderTicket
from ahdms.models.other_doc import OtherDoc
from ahdms.models.prescription import Prescription
from ahdms.models.radiology import Radiology
from ahdms.models.user_resource import UserResource
from core.views.base_list_view import BaseListView
from core.views.base_template_view import BaseTemplateView
from core.views.base_update_view import BaseUpdateView


class DocumentListView(BaseListView):
    template_name = "manage_documents.html"
    model = Radiology

    def get_template_names(self):
        manage_area = self.kwargs.get("manage_area")
        if manage_area == "users":
            return ["manage_users_list.html"]
        elif manage_area == "documents_v1":
            return ["manage_documents.html"]
        elif manage_area == "documents":
            return ["manage_resource.html"]
        elif manage_area == "document-upload":
            return ["document_upload.html"]

    def get_queryset(self):
        doc_type = self.request.GET.get("doc-type", "radiology")
        status = self.request.GET.get("status", "unassigned")
        manage_area = self.kwargs.get("manage_area")
        if manage_area == "documents_v1":
            allowed_status = [DocumentStatus.UNASSIGNED.value,
                              DocumentStatus.PENDING.value,
                              DocumentStatus.ASSIGNED.value,
                              DocumentStatus.UNMATCHED.value,
                              DocumentStatus.MATCHED.value,
                              DocumentStatus.PROCESSED.value,
                              DocumentStatus.FINAL.value]

            if doc_type:
                doc_type = MenuHelper.url_to_name(doc_type)

            if doc_type == "radiology":
                document_instances = Radiology.objects.all()
                if status in allowed_status:
                    document_instances = document_instances.filter(status=status)
                    return document_instances
                else:
                    return Radiology.objects.none()
            elif doc_type == "prescription":
                document_instances = Prescription.objects.all()
                if status in allowed_status:
                    document_instances = document_instances.filter(status=status)
                    return document_instances
                else:
                    return Prescription.objects.none()
            elif doc_type == "laboratory":
                document_instances = Laboratory.objects.all()
                if status in allowed_status:
                    document_instances = document_instances.filter(status=status)
                    return document_instances
                else:
                    return Laboratory.objects.none()
            elif doc_type == "doctors_note":
                document_instances = DoctorsNote.objects.all()
                if status in allowed_status:
                    document_instances = document_instances.filter(status=status)
                    return document_instances
                else:
                    return DoctorsNote.objects.none()
            elif doc_type == "doc_type_other_document":
                document_instances = OtherDoc.objects.all()
                if status in allowed_status:
                    document_instances = document_instances.filter(status=status)
                    return document_instances
                else:
                    return OtherDoc.objects.none()
        elif manage_area == "users":
            user_instances = User.objects.filter(is_staff=False)
            return user_instances
        elif manage_area == "documents":
            filter = self.request.GET.get("filter", "no")
            aws_resource_objects = AWSResource.objects.all()
            if filter == "archived":
                aws_resource_objects = AWSResourceArchive.objects.all()
            elif filter == "processed":
                aws_resource_objects = aws_resource_objects.filter(status=DocumentStatus.PROCESSED.value)
            elif filter == "assigned":
                aws_resource_objects = aws_resource_objects.filter(status=DocumentStatus.ASSIGNED.value)
            elif filter == "unassigned":
                aws_resource_objects = aws_resource_objects.filter(status=DocumentStatus.UNASSIGNED.value)
            return aws_resource_objects

        return super(DocumentListView, self).get_queryset()

    def create_object_groups(self, object_list):
        groups = {}
        for object in object_list:
            resource_uuid = object.aws_resource.resource_uuid
            if resource_uuid not in groups:
                groups[resource_uuid] = [object]
            else:
                groups[resource_uuid] += [object]
        return groups

    def get_context_data(self, **kwargs):
        context = super(DocumentListView, self).get_context_data(**kwargs)
        doc_type = self.request.GET.get("doc-type", "radiology")
        status = self.request.GET.get("status", "unassigned")
        context["active_document_type"] = MenuHelper.url_to_name(doc_type)
        context["active_label"] = status
        object_list = self.get_queryset()
        manage_area = self.kwargs.get("manage_area")
        if manage_area == "users":
            context["object_list"] = object_list
        elif manage_area == "documents_v1":
            context["object_groups"] = self.create_object_groups(object_list)
        elif manage_area == "resources":
            context["object_list"] = object_list

        context["active_menu"] = manage_area
        if manage_area == "users":
            context["active_left_item"] = "user_list"
        elif manage_area == "documents":
            resource_ids = object_list.values_list('pk', flat=True)

            dictinct_doc_type_ids = object_list.values('resource_type_id').distinct().values_list('resource_type_id', flat=True)

            doc_type_names = DocumentType.objects.filter(pk__in=dictinct_doc_type_ids).values_list('doc_type', flat=True)

            unmatched_resource_ids = []

            for doc_type_name in doc_type_names:
                DocumentModel = None
                if doc_type_name == Radiology.__name__:
                    DocumentModel = Radiology
                elif doc_type_name == Laboratory.__name__:
                    DocumentModel = Laboratory
                elif doc_type_name == Prescription.__name__:
                    DocumentModel = Prescription
                elif doc_type_name == DoctorsNote.__name__:
                    DocumentModel = DoctorsNote
                elif doc_type_name == OtherDoc.__name__:
                    DocumentModel = OtherDoc

                if DocumentModel:
                    unmatched_resource_ids += DocumentModel.objects.filter(status=DocumentStatus.UNMATCHED.value,
                                                                      aws_resource_id__in=resource_ids). \
                        values_list('pk', 'aws_resource_id')

            unmatch_rl = []
            unmatched_document_dict = {}
            for u_r_l in unmatched_resource_ids:
                unmatch_rl += [u_r_l[1]]
                unmatched_document_dict[u_r_l[1]] = u_r_l[0]

            context["unmatched_resource_ids"] = unmatch_rl
            context["unmatched_document_dict"] = unmatched_document_dict

            filter = self.request.GET.get("filter", "no")
            active_left_menu_item = "no"
            if filter == "archived":
                active_left_menu_item = "archived"
            elif filter == "processed":
                active_left_menu_item = "processed"
            elif filter == "assigned":
                active_left_menu_item = "assigned"
            elif filter == "unassigned":
                active_left_menu_item = "unassigned"
            context["active_left_item"] = active_left_menu_item
        elif manage_area == "document-upload":
            context["form"] = DocumentUploadModelForm()
            context["active_menu"] = "documents"
            context["active_left_item"] = "upload"
        context["editable"] = status == "unmatched"
        context["left_menu_document_items"] = MenuHelper.prepare_left_menu_document_items(manage_area)
        return context


class ManageUserCreateView(BaseTemplateView):
    template_name = "manage_users_create.html"

    def get_context_data(self, **kwargs):
        context = super(ManageUserCreateView, self).get_context_data(**kwargs)
        context["active_menu"] = "users"
        context["active_left_item"] = "user_create"
        context["signup_form"] = UserSignupForm()
        return context

    def get(self, request, *args, **kwargs):
        return super(ManageUserCreateView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        signup_form = UserSignupForm(request.POST)
        if signup_form.is_valid():
            username = signup_form.cleaned_data["username"]
            email = signup_form.cleaned_data["email"]
            password = signup_form.cleaned_data["password"]
            password2 = signup_form.cleaned_data["password2"]
            is_active = signup_form.cleaned_data["is_active"]

            User.objects.create_user(username=username, email=email, password=password, is_active=is_active)

            messages.add_message(request, messages.SUCCESS, 'User created successfully')
        else:
            err_list = []
            for key, value in signup_form.errors.items():
                err_list += value
            msg = "".join(err_list)
            messages.add_message(request, messages.ERROR, msg)
        return HttpResponseRedirect(reverse("manage_users_create_view"))


class FetchAllSubmissionView(BaseTemplateView):

    def get(self, request, *args, **kwargs):

        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {
                'current_doc_id': None,
                'items': []
            }
        }

        doc_type = kwargs.get("doc_type")
        doc_id = kwargs.get("doc_id")
        doc_uuid = None
        try:
            doc_id = int(doc_id)
        except Exception as exp:
            response['status'] = "FAILURE"
            response["message"] = "Invalid document id"
            return False

        data = []
        select_data = []
        select1, select2 = None, None
        text1, text2 = "", ""
        text1_heading, text2_heading = "", ""
        if doc_type == "radiology":
            document_object = Radiology.objects.get(pk=doc_id)
            doc_uuid = document_object.aws_resource.resource_uuid
            all_submission_instances = Radiology.objects.filter(aws_resource_id=document_object.aws_resource_id)

            for instance in all_submission_instances:
                data += [
                    {
                        "id": instance.pk,
                        "uuid": instance.aws_resource.resource_uuid,
                        "code": instance.code,
                        "doc_type": instance.type.doc_type,
                        "submitted_by": instance.user.email,
                        "status": instance.status,
                        "comment": instance.comment,
                        "edit_link": "<a href='%s'>Edit</a>" % reverse("aws_manage_edit_content_view", kwargs={"doc_type": instance.type.doc_type.lower(), "pk": instance.pk}),
                        "approve_link": "<a data-doc-type='%s' data-doc-id='%s' href='#' class='unmatch-approve'>Approve</a>" % (instance.type.doc_type.lower(), instance.pk)
                    }
                ]
                select_data += [{"id": instance.pk, "label": instance.code + "(%s)" % instance.user.email}]

                if not select1:
                    select1 = instance.pk
                    text1 = instance.as_text()
                    text1_heading = instance.code + "(%s)" % instance.user.email
                elif not select2:
                    select2 = instance.pk
                    text2 = instance.as_text()
                    text2_heading = instance.code + "(%s)" % instance.user.email

            response["data"]["resource_url"] = document_object.aws_resource.resource_url

        response["data"]["select_data"] = select_data
        response["data"]["select1"] = select1
        response["data"]["select2"] = select2
        response["data"]["text1"] = text1
        response["data"]["text2"] = text2
        response["data"]["text1_heading"] = text1_heading
        response["data"]["text2_heading"] = text2_heading
        response["data"]["current_doc_id"] = doc_uuid if doc_uuid else doc_id
        response['data']['items'] = data

        return HttpResponse(json.dumps(response))


@method_decorator(csrf_exempt, name='dispatch')
class ApproveDocumentSubmission(BaseTemplateView):

    def post(self, request, *args, **kwargs):
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {}
        }

        if not request.user.is_authenticated() or not request.user.is_superuser:
            response["status"] = "FAILURE"
            response["message"] = "Required authorized access"
            return HttpResponse(json.dumps(response))

        action_type = kwargs.get("action_type", None)

        if action_type == "approve":

            doc_type = request.POST.get("doc_type")
            doc_id = request.POST.get("doc_id")

            try:
                doc_id = int(doc_id)
            except Exception as exp:
                response['status'] = 'FAILURE'
                response['message'] = 'Invalid document id'
                return HttpResponse(json.dumps(response))

            instance = None
            if doc_type == "radiology":
                instance = Radiology.objects.filter(pk=doc_id).first()
            elif doc_type == "laboratory":
                instance = Laboratory.objects.filter(pk=doc_id).first()
            elif doc_type == "prescription":
                instance = Prescription.objects.filter(pk=doc_id).first()
            elif doc_type == "doctorsnote":
                instance = DoctorsNote.objects.filter(pk=doc_id).first()
            elif doc_type == "otherdoc":
                instance = OtherDoc.objects.filter(pk=doc_id).first()
            elif doc_type == "orderticket":
                instance = OrderTicket.objects.filter(pk=doc_id).first()

            if instance:

                new_instance = instance
                new_instance.pk = None
                new_instance.user_id = request.user.pk
                new_instance.source_document_type = instance.__class__.__name__
                new_instance.source_document_id = instance.aws_resource_id
                new_instance.approval_source = DocumentApprovalSource.SOURCE_APPROVE.value
                new_instance.status = DocumentStatus.FINAL.value
                new_instance.save()

                all_radiology_documents = Radiology.objects.filter(aws_resource_id=instance.aws_resource_id).exclude(pk=new_instance.pk)
                all_radiology_documents.update(status=DocumentStatus.PROCESSED.value, processed_document_id=new_instance.pk)

                all_laboratory_documents = Laboratory.objects.filter(aws_resource_id=instance.aws_resource_id).exclude(pk=new_instance.pk)
                all_laboratory_documents.update(status=DocumentStatus.PROCESSED.value, processed_document_id=new_instance.pk)

                all_prescription_documents = Prescription.objects.filter(aws_resource_id=instance.aws_resource_id).exclude(
                    pk=new_instance.pk)
                all_prescription_documents.update(status=DocumentStatus.PROCESSED.value,
                                                  processed_document_id=new_instance.pk)

                all_doctors_note_documents = DoctorsNote.objects.filter(aws_resource_id=instance.aws_resource_id).exclude(
                    pk=new_instance.pk)
                all_doctors_note_documents.update(status=DocumentStatus.PROCESSED.value,
                                                  processed_document_id=new_instance.pk)

                all_other_doc_documents = OtherDoc.objects.filter(aws_resource_id=instance.aws_resource_id).exclude(
                    pk=new_instance.pk)
                all_other_doc_documents.update(status=DocumentStatus.PROCESSED.value,
                                               processed_document_id=new_instance.pk)

                all_order_ticket_documents = OrderTicket.objects.filter(aws_resource_id=instance.aws_resource_id).exclude(
                    pk=new_instance.pk)
                all_order_ticket_documents.update(status=DocumentStatus.PROCESSED.value,
                                                  processed_document_id=new_instance.pk)

                aws_resource = new_instance.aws_resource
                aws_resource.status = DocumentStatus.PROCESSED.value
                aws_resource.save()

            messages.add_message(request, messages.INFO, "Approved Successfully")

            response["data"]["redirect_url"] = reverse("manage_view", kwargs={"manage_area":"documents"}) + "?filter=no"

        elif action_type == "approve-doc-upload":
            upload_id = request.POST.get("upload_id")
            try:
                upload_id = int(upload_id)
            except:
                response["status"] = "FAILURE"
                response["message"] = "Invalid upload id"
                messages.add_message(request, messages.INFO, "Upload approved failed. Invalid upload id.")

            doc_upload_instances = DocumentUpload.objects.filter(pk=upload_id)
            if not doc_upload_instances.exists():
                response["status"] = "FAILURE"
                response["message"] = "Invalid upload id"
                messages.add_message(request, messages.INFO, "Upload approved failed. Invalid upload id.")
            else:
                doc_upload_instance = doc_upload_instances.first()
                doc_upload_instance.is_approved = True
                doc_upload_instance.save()

                messages.add_message(request, messages.INFO, "Upload approved Successfully")

        elif action_type == "upload-document":
            upload_id = request.POST.get("upload_id")
            try:
                upload_id = int(upload_id)
            except:
                response["status"] = "FAILURE"
                response["message"] = "Invalid upload id"
                messages.add_message(request, messages.INFO, "Upload approved failed. Invalid upload id.")

            doc_upload_instances = DocumentUpload.objects.filter(pk=upload_id)
            if not doc_upload_instances.exists():
                response["status"] = "FAILURE"
                response["message"] = "Invalid upload id"
                messages.add_message(request, messages.INFO, "Upload approved failed. Invalid upload id.")
            else:
                doc_upload_instance = doc_upload_instances.first()
                uploaded = AWSS3.upload_file(doc_upload_instance.file,
                                             file_name=doc_upload_instance.file_name,
                                             resource_type=doc_upload_instance.document.doc_type)
                if uploaded:
                    doc_upload_instance.aws_upload_status = True
                else:
                    doc_upload_instance.aws_upload_status = False
                doc_upload_instance.save()

                messages.add_message(request, messages.INFO, "Uploaded to AWS Successfully")

        return HttpResponse(json.dumps(response))


class ManageDocumentFetchListView(BaseListView):
    paginate_by = 10

    def get_document_list_with_status(self, object_list, status=DocumentStatus.UNMATCHED.value, queryset=True):
        if queryset:
            resource_ids = object_list.values_list('pk', flat=True)
            dictinct_doc_type_ids = object_list.values('resource_type_id').distinct().values_list('resource_type_id',
                                                                                                  flat=True)
        else:
            resource_ids = object_list
            dictinct_doc_type_ids = AWSResource.objects.filter(pk__in=object_list).values('resource_type_id').distinct().values_list('resource_type_id',
                                                                                                 flat=True)

        doc_type_names = DocumentType.objects.filter(pk__in=dictinct_doc_type_ids).values_list('doc_type', flat=True)

        unmatched_resource_ids = []

        for doc_type_name in doc_type_names:
            DocumentModel = None
            if doc_type_name == Radiology.__name__:
                DocumentModel = Radiology
            elif doc_type_name == Laboratory.__name__:
                DocumentModel = Laboratory
            elif doc_type_name == Prescription.__name__:
                DocumentModel = Prescription
            elif doc_type_name == DoctorsNote.__name__:
                DocumentModel = DoctorsNote
            elif doc_type_name == OtherDoc.__name__:
                DocumentModel = OtherDoc
            elif doc_type_name == OrderTicket.__name__:
                DocumentModel = OrderTicket

            if DocumentModel:
                unmatched_resource_ids += DocumentModel.objects.filter(status=status,
                                                                       aws_resource_id__in=resource_ids). \
                    values_list('pk', 'aws_resource_id')

        unmatch_rl = []
        unmatched_document_dict = {}
        for u_r_l in unmatched_resource_ids:
            unmatch_rl += [u_r_l[1]]
            unmatched_document_dict[u_r_l[1]] = u_r_l[0]
        return unmatch_rl, unmatched_document_dict

    def get(self, request, *args, **kwargs):
        response = {

        }
        if request.user.is_authenticated():
            page_start = request.GET.get("start", 0)
            try:
                page_start = int(page_start)
            except:
                return HttpResponse(json.dumps(response))
            page = int((self.paginate_by + page_start)/self.paginate_by)

            allowed_status = [DocumentStatus.UNASSIGNED.value,
                              DocumentStatus.PENDING.value,
                              DocumentStatus.ASSIGNED.value,
                              DocumentStatus.UNMATCHED.value,
                              DocumentStatus.MATCHED.value,
                              DocumentStatus.PROCESSED.value,
                              DocumentStatus.FINAL.value]

            status = request.GET.get("status")

            resource_list = AWSResource.objects.all()

            if status in allowed_status:
                resource_list = resource_list.filter(status=status)
            elif status == "archived":
                resource_list = AWSResourceArchive.objects.all()

            paginator = Paginator(resource_list, self.paginate_by)

            try:
                resource_list = paginator.page(page)
            except PageNotAnInteger:
                resource_list = paginator.page(1)
            except EmptyPage:
                resource_list = paginator.page(1)

            records = AWSResource.objects.all()

            if status in allowed_status:
                records = records.filter(status=status)
            elif status == "archived":
                records = AWSResourceArchive.objects.all()

            object_list = AWSResource.objects.all()
            if status in allowed_status:
                object_list = object_list.filter(status=status)
            elif status == "archived":
                object_list = AWSResourceArchive.objects.all()

            unmatch_rl, unmatch_document_dict = self.get_document_list_with_status(object_list)

            submitted_rl, submitted_document_dict = self.get_document_list_with_status(object_list, status=DocumentStatus.ASSIGNED.value)
            records_total = records.count()

            # Find all submitted resources
            all_submitted_resource_ids = UserResource.objects.filter(Q(status=DocumentStatus.PENDING.value) & ~Q(resource_id__in=unmatch_rl))\
                .annotate(resource_id_count=Count('resource_id'))\
                .filter(resource_id_count__lt=settings.MAX_OPERATOR_FORM_MATCH)\
                .values_list('resource_id', flat=True)

            all_submitted_resource_ids = list(set(all_submitted_resource_ids))

            pending_rl, pending_documents_dict = self.get_document_list_with_status(all_submitted_resource_ids, status=DocumentStatus.PENDING.value, queryset=False)

            response["page"] = page
            response["recordsTotal"] = records_total
            response["recordsFiltered"] = records_total
            data = []
            for resource_instance in resource_list:
                view_link = ""
                resource_status = ""
                if resource_instance.pk in unmatch_rl:
                    resource_status = "Unmatched"
                    view_link = "<a data-doc-id='%s' data-doc-type='%s' href='#'  class='diff-detail'>View Unmatch</a>" % (unmatch_document_dict.get(resource_instance.pk), resource_instance.resource_type.doc_type.lower())
                elif resource_instance.status == DocumentStatus.ASSIGNED.value:
                    try:
                        if resource_instance.pk in pending_rl:
                            resource_status = "Submitted"
                            view_link = "<a href='%s'>View Data</a>" % (reverse("aws_manage_preview_content_view", kwargs={"doc_type": resource_instance.resource_type.doc_type.lower(), "pk": pending_documents_dict.get(resource_instance.pk)}))
                        else:
                            view_link = ""
                    except Exception as exp:
                        view_link = ""
                data += [
                    [
                        resource_instance.get_date_added(),
                        resource_instance.resource_type.doc_type,
                        resource_instance.status if not resource_status else resource_status,
                        view_link
                    ]
                ]

            response["data"] = data

        return HttpResponse(json.dumps(response))


class FetchDocumentSubmissionContent(BaseTemplateView):

    def get(self, request, *args, **kwargs):
        response = {
            "status": "SUCCESS",
            "message": "Successful",
            "data": {}
        }

        doc_type = kwargs.get("doc_type")
        doc_id = request.GET.get("id")
        try:
            doc_id = int(doc_id)
        except Exception as exp:
            response['status'] = 'FAILURE'
            response['message'] = 'Invalid document id'
            return HttpResponse(json.dumps(response))

        instance = None
        if doc_type == "radiology":
            instance = Radiology.objects.filter(pk=doc_id).first()
        elif doc_type == "laboratory":
            instance = Laboratory.objects.filter(pk=doc_id).first()
        elif doc_type == "prescription":
            instance = Prescription.objects.filter(pk=doc_id).first()
        elif doc_type == "doctorsnote":
            instance = DoctorsNote.objects.filter(pk=doc_id).first()
        elif doc_type == "otherdoc":
            instance = OtherDoc.objects.filter(pk=doc_id).first()
        if instance:
            text = instance.as_text()
            response["data"]["heading"] = instance.code + "(%s)" % instance.user.email
            response["data"]["text"] = text

        return HttpResponse(json.dumps(response))


class DocumentEditView(BaseUpdateView):
    template_name = "document.html"
    model = Radiology
    form_class = RadiologyModelForm

    def get_object(self, queryset=None):
        doc_type = self.kwargs.get('doc_type')
        id = self.kwargs.get("pk")
        if doc_type == "radiology":
            return Radiology.objects.get(pk=id)
        elif doc_type == "prescription":
            return Prescription.objects.get(pk=id)
        elif doc_type == "laboratory":
            return Laboratory.objects.get(pk=id)
        elif doc_type == "doctors-note":
            return DoctorsNote.objects.get(pk=id)
        elif doc_type == "other-doc":
            return OtherDoc.objects.get(pk=id)
        elif doc_type == "order-ticket":
            return OrderTicket.objects.get(pk=id)

    def get_form_template_names(self):
        doc_type = self.kwargs.get('doc_type')
        if doc_type == "radiology":
            return ["documents/radiology.html"]
        elif doc_type == "prescription":
            return ["documents/prescription.html"]
        elif doc_type == "laboratory":
            return ["documents/laboratory.html"]
        elif doc_type == "doctors-note":
            return ["documents/doctors_note.html"]
        elif doc_type == "other-doc":
            return ["documents/other_documents.html"]
        elif doc_type == "order-ticket":
            return ["documents/order_ticket_document.html"]

    def get_context_data(self, **kwargs):
        context = super(DocumentEditView, self).get_context_data(**kwargs)
        if "preview" in kwargs:
            preview = kwargs.pop("preview")
        else:
            preview = False
        doc_type = self.kwargs.get('doc_type')
        context["active_menu"] = "documents"
        context["document_forms"] = FormManager.get_document_forms(request=self.request, instance=self.object, preview=preview)
        context["document_type_objects"] = DocumentType.objects.all()
        context["resource_type"] = self.object.type.doc_type
        context["patient_id"] = self.object.aws_resource.resource_id
        context["is_manage"] = 1 if self.request.user.is_superuser else 0
        context["document_id"] = self.object.aws_resource.resource_uuid
        context["resource_id"] = self.object.aws_resource.pk
        context["doc_id"] = self.object.pk
        context["resource_url"] = self.object.aws_resource.resource_url
        return context

    def post(self, request, *args, **kwargs):
        return super(DocumentEditView, self).post(request, *args, **kwargs)


class ManageDocumentPreview(DocumentEditView):
    def get_context_data(self, **kwargs):
        context = super(ManageDocumentPreview, self).get_context_data(preview=True, **kwargs)
        context["preview"] = True
        return context


class DocumentUploadView(BaseTemplateView):

    def post(self, request, *args, **kwargs):
        document_upload_form = DocumentUploadModelForm(request.POST, request.FILES)
        if document_upload_form.is_valid():
            uploaded = document_upload_form.save(request=request)
            if uploaded:
                messages.add_message(request, messages.INFO, "Uploaded Successfully")
            else:
                messages.add_message(request, messages.INFO, "Upload Failed")
        return HttpResponseRedirect(reverse("manage_view", kwargs={"manage_area": "document-upload"}))


class ManageDocumentListView(BaseListView):
    template_name = "document_list.html"
    model = DocumentUpload

    def get_context_data(self, **kwargs):
        context = super(ManageDocumentListView, self).get_context_data(**kwargs)
        context["active_menu"] = "documents"
        context["active_left_item"] = "approve"
        return context
