from django.contrib.auth.models import User
from django.http.response import HttpResponse
import subprocess
import os
import shutil

from ahdms.helpers.aws_s3 import AWSS3
from ahdms.models.scheduler_status import SchedulerStatus
from core.views.base_template_view import BaseTemplateView


class RunMigrationsView(BaseTemplateView):

    def copy_sh_script_and_run(self):
        os.makedirs("mkdir -p /opt/elasticbeanstalk/hooks/appdeploy/post")
        shutil.copy('/opt/python/current/app/ebfiles/files/celery_configuration.txt', '/opt/elasticbeanstalk/hooks/appdeploy/post/run_supervised_celeryd.sh')
        os.chmod('/opt/elasticbeanstalk/hooks/appdeploy/post/run_supervised_celeryd.sh', '744')
        # subprocess.run('cat /opt/python/current/app/ebfiles/files/celery_configuration.txt > /opt/elasticbeanstalk/hooks/appdeploy/post/run_supervised_celeryd.sh && chmod 744 /opt/elasticbeanstalk/hooks/appdeploy/post/run_supervised_celeryd.sh')
        # subprocess.run("/opt/elasticbeanstalk/hooks/appdeploy/post/run_supervised_celeryd.sh")

    def get(self, request, *args, **kwargs):
        skip = request.GET.get("skip", None)
        if not skip:
            from django.core import management
            management.call_command('migrate', 'django_celery_results')
            management.call_command('sqlflush')
            management.call_command('migrate')
            if not User.objects.filter(username="admin").exists():
                User.objects.create_superuser("admin", "admin@admin.com", "admin")

            if not User.objects.filter(username="sohel").exists():
                User.objects.create_superuser("sohel", "sohel@admin.com", "lapsso065")

            if not User.objects.filter(username="sajjad").exists():
                User.objects.create_superuser("sajjad", "sajjad@admin.com", "P@ssc0de")

            # SchedulerStatus.update_status(text="Starting download aws resource")
            # resources = AWSS3.read_resources()
            # AWSS3.save_resources(resources)
            # SchedulerStatus.update_status(text="Completed download aws resource")

        # self.copy_sh_script_and_run()

        # s = ""
        #
        # with open('/opt/elasticbeanstalk/hooks/appdeploy/post/run_supervised_celeryd.sh', 'r') as f:
        #     s = f.read()
        # return HttpResponse(s)

        last_schedule_time = SchedulerStatus.get_last_schedule_time()
        return HttpResponse(last_schedule_time)