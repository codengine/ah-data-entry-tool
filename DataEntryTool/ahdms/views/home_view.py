import json
from django.contrib.auth.decorators import login_required
from django.db.models.query_utils import Q
from django.http.response import HttpResponse, HttpResponseRedirect
from django.urls.base import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from ahdms.helpers.aws_s3 import AWSS3
from ahdms.helpers.enums import DocumentStatus
from ahdms.managers.doctors_note_form_manager import DoctorsNoteFormManager
from ahdms.managers.form_manager import FormManager
from ahdms.managers.laboratory_form_manager import LaboratoryFormManager
from ahdms.managers.order_ticket_form_manager import OrderTicketFormManager
from ahdms.managers.other_doc_form_manager import OtherDocFormManager
from ahdms.managers.prescription_form_manager import PrescriptionFormManager
from ahdms.managers.radiology_form_manager import RadiologyFormManager
from ahdms.models.doctors_note import DoctorsNote
from ahdms.models.document_type import DocumentType
from ahdms.models.facility import Facility
from ahdms.models.laboratory import Laboratory
from ahdms.models.order_ticket import OrderTicket
from ahdms.models.other_doc import OtherDoc
from ahdms.models.patient import Patient
from ahdms.models.physician import Physician
from ahdms.models.prescription import Prescription
from ahdms.models.radiology import Radiology
from ahdms.models.user_resource import UserResource
from core.views.base_template_view import BaseTemplateView
from engine.clock import Clock


@method_decorator(login_required, name='dispatch')
@method_decorator(csrf_exempt, name='dispatch')
class HomeView(BaseTemplateView):
    template_name = "document.html"
    
    def get(self, request, *args, **kwargs):

        if request.user.is_superuser:
            return HttpResponseRedirect(reverse("manage_view", kwargs={"manage_area": "documents"}))
        else:
            self.template_name = "document.html"
        return super(HomeView, self).get(request, *args, **kwargs)

    def dispatch(self, *args, **kwargs):
        return super(HomeView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        # Prepare all empty form
        context["document_forms"] = FormManager.get_document_forms()
        context["document_type_objects"] = DocumentType.objects.all()
        return context
        
        
class AWSImageDownloadView(BaseTemplateView):

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            response = {
                "status": "FAILURE",
                "message": "Login Required"
            }
            return HttpResponse(json.dumps(response))
        # resources = AWSS3.read_resources()
        # AWSS3.save_resources(resources)
        response = {
            "status": "SUCCESS",
            "message": "Successful"
        }
        return HttpResponse(json.dumps(response))
        
        
class AWSFetchAWSResource(BaseTemplateView):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            response = {
                "status": "FAILURE",
                "message": "Login Required"
            }
            return HttpResponse(json.dumps(response))
        user = request.user
        tier = request.GET.get('tier', 1)
        tier = int(tier)
        aws_resource = AWSS3.get_next_resource(user_id=user.pk)
        if aws_resource:
            serialized_resource = aws_resource.object2serialized()
            response = {
                "status": "SUCCESS",
                "message": "Successful"
            }
            response = {**serialized_resource, **response, **{"user_id": request.user.pk, **{"is_superuser": 1 if request.user.is_superuser else 0}}}
            return HttpResponse(json.dumps(response))
        else:
            response = {
                "status": "FAILED",
                "code": 4004,
                "message": "No Data"
            }
            return HttpResponse(json.dumps(response))


@method_decorator(csrf_exempt, name='dispatch')
class DocumentFormSubmitView(BaseTemplateView):

    def post(self, request, *args, **kwargs):
        response = {
            "status": "SUCCESS",
            "code": 1000,
            "message": "Successful",
            "redirect": 1 if request.user.is_superuser else 0
        }
        
        if not request.user.is_authenticated():
            response["status"] = "FAILURE"
            response["code"] = 1001
            response["message"] = "Authentication required"
            return HttpResponse(json.dumps(response))
            
        document_type = request.POST.get("doc_type_id", None)
        if not document_type:
            response["status"] = "FAILURE"
            response["code"] = 1002
            response["message"] = "Authentication required"
            return HttpResponse(json.dumps(response))
            
        if document_type == "Radiology":  # Radiology
            radio_form_manager = RadiologyFormManager()
            instance = radio_form_manager.save_form(request=request, document_class=Radiology)
            if not instance:
                response["status"] = "FAILURE"
                response["code"] = 1003
                response["message"] = "Form save failed"
                return HttpResponse(json.dumps(response))
        elif document_type == "Laboratory":  # Laboratory
            laboratory_form_manager = LaboratoryFormManager()
            instance = laboratory_form_manager.save_form(request=request, document_class=Laboratory)
            if not instance:
                response["status"] = "FAILURE"
                response["code"] = 1003
                response["message"] = "Form save failed"
                return HttpResponse(json.dumps(response))
        elif document_type == "DoctorsNote":  # DoctorsNote
            doctors_note_form_manager = DoctorsNoteFormManager()
            instance = doctors_note_form_manager.save_form(request=request, document_class=DoctorsNote)
            if not instance:
                response["status"] = "FAILURE"
                response["code"] = 1003
                response["message"] = "Form save failed"
                return HttpResponse(json.dumps(response))
        elif document_type == "Prescription":
            prescription_form_manager = PrescriptionFormManager()
            instance = prescription_form_manager.save_form(request=request, document_class=Prescription)
            if not instance:
                response["status"] = "FAILURE"
                response["code"] = 1003
                response["message"] = "Form save failed"
                return HttpResponse(json.dumps(response))
        elif document_type == "OtherDoc":
            other_doc_form_manager = OtherDocFormManager()
            instance = other_doc_form_manager.save_form(request=request, document_class=OtherDoc)
            if not instance:
                response["status"] = "FAILURE"
                response["code"] = 1003
                response["message"] = "Form save failed"
                return HttpResponse(json.dumps(response))
        elif document_type == "OrderTicket":
            order_ticket_form_manager = OrderTicketFormManager()
            instance = order_ticket_form_manager.save_form(request=request, document_class=OrderTicket)
            if not instance:
                response["status"] = "FAILURE"
                response["code"] = 1003
                response["message"] = "Form save failed"
                return HttpResponse(json.dumps(response))
        
        return HttpResponse(json.dumps(response))


class FetchEntityView(BaseTemplateView):

    def get(self, request, *args, **kwargs):
        data = []
        q = request.GET.get("term", None)
        doc_type = request.GET.get("doc", None)
        if doc_type == "patient":
            patient_objects = Patient.objects.filter(name__istartswith=q)
            patient_objects = patient_objects[:10]
            data = [patient_instance.as_json(
                fields=["id", "name", "patient_id", "date_of_birth", "phone"],
                date_formats = { "date_of_birth": "%m/%d/%Y" }
            ) for patient_instance in patient_objects]
        elif doc_type == "facility":
            facility_objects = Facility.objects.filter(name__istartswith=q)
            facility_objects = facility_objects[:10]
            data = [facility_instance.as_json(
                fields=["id", "name", "facility_id", "country_id", "city_id", "phone", "email", "address"]
            ) for facility_instance in facility_objects]
        elif doc_type == "physician":
            physician_objects = Physician.objects.filter(name__istartswith=q)
            physician_objects = physician_objects[:10]
            data = [physician_instance.as_json(
                fields=["id", "name", "physician_id", "phone", "affilicated_facility", "spe", "email", "address"]
            ) for physician_instance in physician_objects]
        return HttpResponse(json.dumps(data))


class PingResourceView(BaseTemplateView):

    def get(self, request, *args, **kwargs):
        response = {
            "status": "SUCCESS",
            "message": "Successful"
        }
        if request.user.is_authenticated():
            resource_id = request.GET.get('resource-id')
            try:
                resource_id = int(resource_id)
            except Exception as exp:
                response['status'] = 'FAILURE'
                response['message'] = 'Invalid resource id'
                return HttpResponse(json.dumps(response))
            user_id = request.user.pk

            all_user_resources = UserResource.objects.filter(Q(user_id=user_id) & Q(resource_id=resource_id) & ~Q(resource__status=DocumentStatus.PROCESSED.value))
            if all_user_resources.exists():
                all_user_resources.update(last_ping_time=Clock.utc_now())

                return HttpResponse(json.dumps(response))
            else:
                response['status'] = 'FAILURE'
                response['message'] = 'No such resource found'
                return HttpResponse(json.dumps(response))
        else:
            response['status'] = 'FAILURE'
            response['message'] = 'Authentication required'
            return HttpResponse(json.dumps(response))


class SkipResourceView(BaseTemplateView):
    def get(self, request, *args, **kwargs):
        response = {
            "status": "SUCCESS",
            "message": "Successful"
        }
        if request.user.is_authenticated():
            resource_id = request.GET.get('resource-id')
            try:
                resource_id = int(resource_id)
            except Exception as exp:
                response['status'] = 'FAILURE'
                response['message'] = 'Invalid resource id'
                return HttpResponse(json.dumps(response))

            user_id = request.user.pk

            user_resources = UserResource.objects.filter(user_id=user_id, resource_id=resource_id)
            if user_resources.exists():
                user_resource = user_resources.first()
                user_resource.status = DocumentStatus.SKIPPED.value
                user_resource.save()
                return HttpResponse(json.dumps(response))
            else:
                return HttpResponse(json.dumps(response))
        else:
            response['status'] = 'FAILURE'
            response['message'] = 'Authentication required'
            return HttpResponse(json.dumps(response))
