from django import template

__author__ = 'codengine'

register = template.Library()


@register.filter(name='access_dict_by_key')
def access_dict_by_key(object, key):
    try:
        return object.get(key, 0)
    except Exception as exp:
        pass

@register.filter(name='found_in')
def found_in(object, item):
    try:
        return item in object
    except Exception as exp:
        pass