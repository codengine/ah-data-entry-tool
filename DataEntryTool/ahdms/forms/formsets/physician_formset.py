from django.forms.formsets import formset_factory
from ahdms.forms.modelforms.physician_form import PhysicianModelForm


PhysicianFormSet = formset_factory(PhysicianModelForm,max_num=100)