from django.forms.formsets import formset_factory
from ahdms.forms.modelforms.doctor_form import DoctorModelForm


DoctorModelFormSet = formset_factory(DoctorModelForm,max_num=100)