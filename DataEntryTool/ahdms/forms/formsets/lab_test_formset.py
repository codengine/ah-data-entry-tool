from django.forms.formsets import formset_factory
from ahdms.forms.modelforms.laboratory_test_forms import LaboratoryTestModelForm

LaboratoryTestFormSet = formset_factory(LaboratoryTestModelForm, max_num=100)