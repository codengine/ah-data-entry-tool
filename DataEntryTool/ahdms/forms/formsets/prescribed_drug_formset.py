from django.forms.formsets import formset_factory
from ahdms.forms.modelforms.prescribe_drug_forms import PrescribedDrugModelForm


PrescribedDrugFormSet = formset_factory(PrescribedDrugModelForm,max_num=100)