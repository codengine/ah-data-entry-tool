from django.forms.formsets import formset_factory
from ahdms.forms.modelforms.patient_forms import PatientModelForm


PatientFormSet = formset_factory(PatientModelForm,max_num=100)