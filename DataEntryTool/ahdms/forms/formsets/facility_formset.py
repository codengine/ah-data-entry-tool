from django.forms.formsets import formset_factory
from ahdms.forms.modelforms.facility_forms import FacilityModelForm

FacilityFormSet = formset_factory(FacilityModelForm,max_num=100)