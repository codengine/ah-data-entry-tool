from django import forms
from datetime import datetime
from django.contrib.auth.models import User
from ahdms.forms.modelforms.base.document_base_model_form import DocumentBaseModelForm
from ahdms.models.aws_resource import AWSResource
from ahdms.models.document_type import DocumentType
from ahdms.models.radiology import Radiology


class RadiologyModelForm(DocumentBaseModelForm):

    radio_type_choices = (
        ("", "Select Modality Type"),
        ("HSG", "HSG"),
        ("MG", "MG"),
        ("MRI", "MRI"),
        ("X-Ray", "X-Ray"),
        ("Ultrasound", "Ultrasound"),
        ("CI", "CI")
    )

    radio_type = forms.ChoiceField(choices=radio_type_choices, required=False,
                                       widget=forms.Select(attrs={"class": "form-control"}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        else:
            self.request = None
        if kwargs.get('pk'):
            self.pk = kwargs.pop('pk')
        else:
            self.pk = None

        super(RadiologyModelForm, self).__init__(*args, **kwargs)
        self.fields["clinic_info"].widget.attrs["class"] = "form-control"
        self.fields["clinic_info"].widget.attrs["placeholder"] = "Clinical Information"
        
        self.fields["other_information"].widget.attrs["class"] = "form-control"
        self.fields["other_information"].widget.attrs["placeholder"] = "Other Information"
        
        self.fields["procedure"].widget.attrs["class"] = "form-control"
        self.fields["procedure"].widget.attrs["placeholder"] = "Procedure"

        self.fields["comparison"].widget.attrs["class"] = "form-control"
        self.fields["comparison"].widget.attrs["placeholder"] = "Comparison"

        self.fields["findings"].widget.attrs["class"] = "form-control"
        self.fields["findings"].widget.attrs["placeholder"] = "Findings"

        self.fields["impression"].widget.attrs["class"] = "form-control"
        self.fields["impression"].widget.attrs["placeholder"] = "Impression"

        self.fields["reporting_radiologist_name"].widget.attrs["class"] = "form-control"
        self.fields["reporting_radiologist_name"].widget.attrs["placeholder"] = "Reporting Radiologist Name"

        self.fields["imaging_date"].widget.attrs["class"] = "form-control datepicker"
        self.fields["imaging_date"].widget.attrs["placeholder"] = "Imaging Date"

        self.fields["reporting_at_date"].widget.attrs["class"] = "form-control datepicker"
        self.fields["reporting_at_date"].widget.attrs["placeholder"] = "Reporting at Date"

        self.fields["notes"].widget.attrs["class"] = "form-control"
        self.fields["notes"].widget.attrs["placeholder"] = "Comment"

        self.fields["doc_id"].widget.attrs["class"] = "form-control"
        self.fields["ah_user_id"].widget.attrs["class"] = "form-control"
        self.fields["doc_type_id"].widget.attrs["class"] = "form-control"
        self.fields["ah_aws_resource_id"].widget.attrs["class"] = "form-control"
        self.fields["radio_type"].widget.attrs["class"] = "form-control"
        self.fields["comment"].widget.attrs["class"] = "form-control"

        self.fields["comment"].widget = forms.HiddenInput()
        
        # Make fields required explicitely
        self.fields["ah_user_id"].widget.attrs["required"] = "required"
        self.fields["doc_type_id"].widget.attrs["required"] = "required"
        self.fields["ah_aws_resource_id"].widget.attrs["required"] = "required"
        self.fields["comment"].widget.attrs["required"] = "required"

    def is_valid(self):
        self.error_messages = []
        self.cleaned_data = {}
        doc_id = self.data.get("doc_id")
        clinic_info = self.data.get("clinic_info")
        procedure = self.data.get("procedure")
        findings = self.data.get("findings")
        impression = self.data.get("impression")
        imaging_date = self.data.get("imaging_date")
        comment = self.data.get("comment")
        ah_user_id = self.data.get("ah_user_id")
        doc_type_id = self.data.get("doc_type_id")
        ah_aws_resource_id = self.data.get("ah_aws_resource_id")
        radio_type = self.data.get("radio_type")
        reporting_at_date = self.data.get("reporting_at_date")
        reporting_radiologist_name = self.data.get("reporting_radiologist_name")
        mandatory_list = [  ]
        mandatory_list_name = [  ]
        if any([not field for field in mandatory_list]):
            self.errors_messages += [" ".join([word.capitalize() for word in field.split("_")]) + " is required" for field in mandatory_list_name if not field ]
            return False

        try:
            if doc_id:
                doc_id = int(doc_id)
            else:
                doc_id = None
        except:
            doc_id = None
            
        try:
            if imaging_date:
                imaging_date = datetime.strptime(imaging_date, "%m/%d/%Y").date()
            else:
                imaging_date = None
        except:
            self.error_messages += [ "Invalid imaging date" ]
            return False

        try:
            if reporting_at_date:
                reporting_at_date = datetime.strptime(reporting_at_date, "%m/%d/%Y").date()
            else:
                reporting_at_date = None
        except:
            self.error_messages += ["Invalid Reporting date"]
            return False
            
        try:
            ah_user_id = int(ah_user_id)
            user = User.objects.get(pk=ah_user_id)
            ah_user_id = user.pk
        except:
            self.error_messages += [ "Invalid user id" ]
            return False
            
        try:
            doc_type = DocumentType.objects.get(doc_type=doc_type_id)
            doc_type_id = doc_type.pk
        except:
            self.error_messages += [ "Invalid document type" ]
            return False
            
        try:
            if ah_aws_resource_id:
                ah_aws_resource_id = int(ah_aws_resource_id)
                aws_resource = AWSResource.objects.get(pk=ah_aws_resource_id)
                ah_aws_resource_id = aws_resource.pk
        except:
            self.error_messages += [ "Invalid resource id" ]
            return False

        self.cleaned_data["doc_id"] = doc_id
        self.cleaned_data["clinic_info"] = clinic_info
        self.cleaned_data["procedure"] = procedure
        self.cleaned_data["findings"] = findings
        self.cleaned_data["impression"] = impression
        self.cleaned_data["imaging_date"] = imaging_date
        self.cleaned_data["comment"] = comment
        self.cleaned_data["ah_user_id"] = ah_user_id
        self.cleaned_data["doc_type_id"] = doc_type_id
        self.cleaned_data["ah_aws_resource_id"] = ah_aws_resource_id
        self.cleaned_data["radio_type"] = radio_type
        self.cleaned_data["reporting_at_date"] = reporting_at_date
        self.cleaned_data["reporting_radiologist_name"] = reporting_radiologist_name
        
        return True
        
    def save(self, commit=True):
        if self.pk:
            instance = Radiology.objects.get(pk=self.pk)
        else:
            instance = Radiology()
            
        clinic_info = self.cleaned_data["clinic_info"]
        procedure = self.cleaned_data["procedure"]
        findings = self.cleaned_data["findings"]
        impression = self.cleaned_data["impression"]
        imaging_date = self.cleaned_data["imaging_date"]
        comment = self.cleaned_data["comment"]
        ah_user_id = self.cleaned_data["ah_user_id"]
        doc_type_id = self.cleaned_data["doc_type_id"]
        ah_aws_resource_id = self.cleaned_data["ah_aws_resource_id"]
        radio_type = self.cleaned_data["radio_type"]
        reporting_at_date = self.cleaned_data["reporting_at_date"]
        reporting_radiologist_name = self.cleaned_data["reporting_radiologist_name"]
            
        instance.clinic_info = clinic_info
        instance.procedure = procedure
        instance.findings = findings
        instance.impression = impression
        if imaging_date:
            instance.imaging_date = imaging_date
        instance.comment = comment
        instance.user_id = ah_user_id
        instance.type_id = doc_type_id
        instance.aws_resource_id = ah_aws_resource_id
        instance.radio_type = radio_type
        if reporting_at_date:
            instance.reporting_at_date = reporting_at_date
        instance.reporting_radiologist_name = reporting_radiologist_name
        instance.save()
        return instance

    class Meta:
        model = Radiology
        fields = ('doc_id', 'is_managed', 'ah_user_id', 'doc_type_id', 'ah_aws_resource_id', 'radio_type', 'procedure',
                  'clinic_info', 'other_information', 'comparison', 'findings', 'impression',
                  'imaging_date', "comment", "notes", "reporting_at_date", "reporting_radiologist_name")