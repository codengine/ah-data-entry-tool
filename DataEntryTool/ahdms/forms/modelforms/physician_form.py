from django import forms
from ahdms.models.physician import Physician
from core.forms.modelform.basemodelform import BaseModelForm


class PhysicianModelForm(BaseModelForm):
    id = forms.IntegerField(widget=forms.HiddenInput(attrs={"class": "form-control"}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        else:
            self.request = None
        if kwargs.get('pk'):
            self.pk = kwargs.pop('pk')
        else:
            self.pk = None

        super(PhysicianModelForm, self).__init__(*args, **kwargs)
        self.fields["name"].widget.attrs["class"] = "form-control typeahead physician-typeahead"
        self.fields["name"].widget.attrs["placeholder"] = "Physician Name"

        self.fields["physician_id"].widget.attrs["class"] = "form-control"
        self.fields["physician_id"].widget.attrs["placeholder"] = "Physician ID"

        self.fields["phone"].widget.attrs["class"] = "form-control"
        self.fields["phone"].widget.attrs["placeholder"] = "Physician Phone"

        self.fields["affilicated_facility"].widget.attrs["class"] = "form-control"
        self.fields["affilicated_facility"].widget.attrs["placeholder"] = "Affiliated Facility Name"
        
    def is_valid(self):
        self.errors_messages = []
        self.cleaned_data = {}
        prefix = self.prefix
        id = self.data.get(prefix + "-id")
        name = self.data.get(prefix + "-name")
        physician_id = self.data.get(prefix + "-physician_id")
        phone = self.data.get(prefix + "-phone")
        affilicated_facility = self.data.get(prefix + "-affilicated_facility")
        mandatory_list = [ ]
        mandatory_list_name = [ ]
        if any([not field for field in mandatory_list]):
            self.errors_messages += [" ".join([word.capitalize() for word in field.split("_")]) + " is required" for field in mandatory_list_name if not field ]
            return False

        try:
            if id:
                id = int(id)
                instance = Physician.objects.get(pk=id)
                id = instance.pk
        except:
            self.error_messages += ["Invalid physician id"]
            return False

        self.cleaned_data["id"] = id
        self.cleaned_data["name"] = name
        self.cleaned_data["physician_id"] = physician_id
        self.cleaned_data["phone"] = phone
        self.cleaned_data["affilicated_facility"] = affilicated_facility
        
        return True
        
    def save(self, commit=True):
        if self.pk:
            instance = Physician.objects.get(pk=self.pk)
        else:
            instance = Physician()
            
        id = self.cleaned_data["id"]
        name = self.cleaned_data["name"]
        physician_id = self.cleaned_data["physician_id"]
        phone = self.cleaned_data["phone"]
        affilicated_facility = self.cleaned_data["affilicated_facility"]

        if id:
            instance = Physician.objects.get(pk=id)
        else:
            instance = Physician()
        instance.name = name
        instance.physician_id = physician_id
        instance.phone = phone
        instance.affilicated_facility = affilicated_facility
        instance.save()
        return instance

    class Meta:
        model = Physician
        fields = ('id', 'name', 'physician_id', 'phone', 'affilicated_facility')