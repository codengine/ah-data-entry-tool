from django import forms
from ahdms.forms.fields.city_model_choice_field import CityModelChoiceField
from ahdms.forms.fields.country_model_choice_field import CountryModelChoiceField
from ahdms.models.facility import Facility
from core.forms.modelform.basemodelform import BaseModelForm
from core.models.city import City
from core.models.country import Country


class FacilityModelForm(BaseModelForm):
    id = forms.IntegerField(widget=forms.HiddenInput(attrs={"class": "form-control"}))
    city = CityModelChoiceField(label="Select City", empty_label="Select City", required=True,
                                queryset=City.objects.all(),
                                        widget=forms.Select(attrs={"class": "form-control"}))
    country = CountryModelChoiceField(label="Select Country", empty_label="Select Country",
                                      required=True, queryset=Country.objects.all(),
                                        widget=forms.Select(attrs={"class": "form-control"}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        else:
            self.request = None
        if kwargs.get('pk'):
            self.pk = kwargs.pop('pk')
        else:
            self.pk = None
        super(FacilityModelForm, self).__init__(*args, **kwargs)
        self.fields["name"].widget.attrs["class"] = "form-control typeahead facility-typeahead"
        self.fields["name"].widget.attrs["placeholder"] = "Facility Name"

        self.fields["facility_id"].widget.attrs["class"] = "form-control"
        self.fields["facility_id"].widget.attrs["placeholder"] = "Facility ID"

        self.fields["phone"].widget.attrs["class"] = "form-control"
        self.fields["phone"].widget.attrs["placeholder"] = "Facility Phone"
        
    def is_valid(self):
        self.error_messages = []
        self.cleaned_data = {}
        prefix = self.prefix
        id = self.data.get(prefix + "-id")
        name = self.data.get(prefix + "-name")
        facility_id = self.data.get(prefix + "-facility_id")
        country = self.data.get(prefix + "-country")
        city = self.data.get(prefix + "-city")
        phone = self.data.get(prefix + "-phone")
        mandatory_list = [ ]
        mandatory_list_name = [ ]
        if any([not field for field in mandatory_list]):
            self.errors_messages += [" ".join([word.capitalize() for word in field.split("_")]) + " is required" for field in mandatory_list_name if not field ]
            return False
            
        if country:
            try:
                country_id = int(country)
                country = Country.objects.get(pk=country_id)
                country = country.pk
            except:
                self.error_messages += [ "Invalid country id" ]
                return False
                
        if city:
            try:
                city_id = int(city)
                city = City.objects.get(pk=city_id)
                city = city.pk
            except:
                self.error_messages += [ "Invalid city id" ]
                return False
        try:
            if id:
                id = int(id)
                instance = Facility.objects.get(pk=id)
                id = instance.pk
        except:
            self.error_messages += ["Invalid facility id"]
            return False

        self.cleaned_data["id"] = id
        self.cleaned_data["name"] = name
        self.cleaned_data["facility_id"] = facility_id
        self.cleaned_data["country"] = country
        self.cleaned_data["city"] = city
        self.cleaned_data["phone"] = phone
        
        return True
        
    def save(self, commit=True):
        if self.pk:
            instance = Facility.objects.get(pk=self.pk)
        else:
            instance = Facility()
            
        id = self.cleaned_data["id"]
        name = self.cleaned_data["name"]
        facility_id = self.cleaned_data["facility_id"]
        country = self.cleaned_data["country"]
        city = self.cleaned_data["city"]
        phone = self.cleaned_data["phone"]

        if id:
            instance = Facility.objects.get(pk=id)
        else:
            instance = Facility()
        instance.name = name
        if facility_id:
            instance.facility_id = facility_id
        if phone:
            instance.phone = phone
        if country:
            instance.country_id = country
        if city:
            instance.city_id = city
        instance.save()
        return instance

    class Meta:
        model = Facility
        fields = ('id', 'country', 'city', 'name', 'facility_id', 'phone')