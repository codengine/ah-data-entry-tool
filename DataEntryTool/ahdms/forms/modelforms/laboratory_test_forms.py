from decimal import Decimal
from django import forms
from ahdms.models.laboratory_test import LaboratoryTest
from core.forms.modelform.basemodelform import BaseModelForm


class LaboratoryTestModelForm(BaseModelForm):
    id = forms.IntegerField(widget=forms.HiddenInput(attrs={"class": "form-control"}))

    unit_choices = (
        ("", "Select Unit"),
        ("% binding", "% binding"),
        ("% bound", "% bound"),
        ("% conversion", "% conversion"),
        ("% excretion", "% excretion"),
        ("% Fat", "% Fat"),
        ("% hemoglobin", "% hemoglobin"),
        ("% hemolysis", "% hemolysis"),
        ("% index", "% index"),
        ("% inhibition", "% inhibition"),
        ("% live", "% live"),
        ("% neg control", "% neg control"),
        ("% Normal", "% Normal"),
        ("% normal control", "% normal control"),
        ("% of normal", "% of normal"),
        ("% of total", "% of total"),
        ("% positive", "% positive"),
        ("% ppt (percent precipitate)", "% ppt (percent precipitate)"),
        ("% reactivity", "% reactivity"),
        ("% total", "% total"),
        ("% total Cholesterol", "% total Cholesterol"),
        ("% total Hemoglobin", "% total Hemoglobin"),
        ("% volume", "% volume"),
        ("(x10)6", "(x10)6"),
        ("/24 hours", "/24 hours"),
        ("/day", "/day"),
        ("/high power field", "/high power field"),
        ("/low power field", "/low power field"),
        ("/microliter", "/microliter"),
        ("/specimen", "/specimen"),
        ("10(3)/microliter", "10(3)/microliter"),
        ("10(6)/microliter", "10(6)/microliter"),
        ("10(-7) mole/Liter", "10(-7) mole/Liter"),
        ("a ratio", "a ratio"),
        ("ABR units", "ABR units"),
        ("ABR Units", "ABR Units"),
        ("AChR", "AChR"),
        ("Am.U/L", "Am.U/L"),
        ("Antibody Response Unit", "Antibody Response Unit"),
        ("at term", "at term"),
        ("AU/mL", "AU/mL"),
        ("bands (chromosome tests)", "bands (chromosome tests)"),
        ("C2H50 U/mL", "C2H50 U/mL"),
        ("cells", "cells"),
        ("centimeter", "centimeter"),
        ("centipoises", "centipoises"),
        ("cholesterol", "cholesterol"),
        ("cm H2O", "cm H2O"),
        ("copies/microliter", "copies/microliter"),
        ("Copies/milliliter", "Copies/milliliter"),
        ("creatinine (not referenced in reporting title)", "creatinine (not referenced in reporting title)"),
        ("Dalton(s)", "Dalton(s)"),
        ("day(s)", "day(s)"),
        ("deciliter", "deciliter"),
        ("degrees (angle measurements)", "degrees (angle measurements)"),
        ("degrees Celsius", "degrees Celsius"),
        ("delta Optical Density", "delta Optical Density"),
        ("disintegrations per minute", "disintegrations per minute"),
        ("dose(s)", "dose(s)"),
        ("EIA units", "EIA units"),
        ("EU", "EU"),
        ("EU/mL", "EU/mL"),
        ("events", "events"),
        ("Examples found in LIS/RLS/CTS", "Examples found in LIS/RLS/CTS"),
        ("femtoliter", "femtoliter"),
        ("femtomole", "femtomole"),
        ("GPL Units", "GPL Units"),
        ("gram(s)", "gram(s)"),
        ("gram/100 mL", "gram/100 mL"),
        ("gram/210 Liters", "gram/210 Liters"),
        ("hemoglobin", "hemoglobin"),
        ("hour(s)", "hour(s)"),
        ("hr,min", "hr,min"),
        ("Index", "Index"),
        ("international units", "international units"),
        ("international Units /gram Hemoglobin", "international Units /gram Hemoglobin"),
        ("international units/24 Hours", "international units/24 Hours"),
        ("IU/10(13) Cells", "IU/10(13) Cells"),
        ("kilodalton", "kilodalton"),
        ("kilogram", "kilogram"),
        ("KU/Hour", "KU/Hour"),
        ("KU/Liter", "KU/Liter"),
        ("Liter", "Liter"),
        ("liter", "liter"),
        ("mEq/30'", "mEq/30'"),
        ("mEq/L", "mEq/L"),
        ("mg every 6hours", "mg every 6hours"),
        ("mg Hb/gram", "mg Hb/gram"),
        ("mg/100 mL", "mg/100 mL"),
        ("mg/2 hours", "mg/2 hours"),
        ("mg/dL RBC", "mg/dL RBC"),
        ("mg/kg/24 hour", "mg/kg/24 hour"),
        ("mg/kg/day", "mg/kg/day"),
        ("mg/specimen", "mg/specimen"),
        ("micro Unit", "micro Unit"),
        ("microEquivalent", "microEquivalent"),
        ("microgram AHG Eq/mL", "microgram AHG Eq/mL"),
        ("microgram Eq/mL", "microgram Eq/mL"),
        ("microgram Equivalent", "microgram Equivalent"),
        ("microgram N/dL", "microgram N/dL"),
        ("microgram T4/dL", "microgram T4/dL"),
        ("microgram(s)", "microgram(s)"),
        ("microgram/g dry weight", "microgram/g dry weight"),
        ("microgram/kilogram", "microgram/kilogram"),
        ("6hour", "6hour"),
        ("micrograms", "micrograms"),
        ("microInternational Units", "microInternational Units"),
        ("microliter", "microliter"),
        ("micromole", "micromole"),
        ("micromole/gram tissue", "micromole/gram tissue"),
        ("microUnit/milliliter", "microUnit/milliliter"),
        ("milli IU/Liter", "milli IU/Liter"),
        ("milliequivalent", "milliequivalent"),
        ("milligram", "milligram"),
        ("milliinternational units", "milliinternational units"),
        ("milliliter", "milliliter"),
        ("milliliter of cells", "milliliter of cells"),
        ("millimeter", "millimeter"),
        ("millimeter duration", "millimeter duration"),
        ("millimeter H2O", "millimeter H2O"),
        ("millimole", "millimole"),
        ("milliosmole", "milliosmole"),
        ("milliunits", "milliunits"),
        ("minute(s)", "minute(s)"),
        ("mIU/L", "mIU/L"),
        ("mL/min/1.73m2", "mL/min/1.73m2"),
        ("ml/min/SA", "ml/min/SA"),
        ("mL/min/Surface Area", "mL/min/Surface Area"),
        ("mm Hg (millimeter Mercury)", "mm Hg (millimeter Mercury)"),
        ("mol/L(-7)", "mol/L(-7)"),
        ("mole", "mole"),
        ("MOM- multiple of the median", "MOM- multiple of the median"),
        ("MPL", "MPL"),
        ("nanogram", "nanogram"),
        ("nanometer", "nanometer"),
        ("nanomole", "nanomole"),
        ("ng %", "ng %"),
        ("nmol/dL glomerular filtrate", "nmol/dL glomerular filtrate"),
        ("optical density", "optical density"),
        ("parts per million", "parts per million"),
        ("percent", "percent"),
        ("pg", "pg"),
        ("pH", "pH"),
        ("picogram(s)", "picogram(s)"),
        ("picomole", "picomole"),
        ("pounds", "pounds"),
        ("protein", "protein"),
        ("ratio", "ratio"),
        ("Relative Units", "Relative Units"),
        ("Score of", "Score of"),
        ("second(s)", "second(s)"),
        ("stimulating index", "stimulating index"),
        ("surface area", "surface area"),
        ("thou cpm (thousand counts per minute)", "thou cpm (thousand counts per minute)"),
        ("thousand", "thousand"),
        ("threshold", "threshold"),
        ("titer", "titer"),
        ("torr", "torr"),
        ("Total/Mechanical", "Total/Mechanical"),
        ("TSI index", "TSI index"),
        ("U IX", "U IX"),
        ("U/10(10)", "U/10(10)"),
        ("U/10(10)cells", "U/10(10)cells"),
        ("uLEq/mL", "uLEq/mL"),
        ("unit(s)", "unit(s)"),
        ("Units", "Units"),
        ("V/V (volume/volume)", "V/V (volume/volume)"),
        ("vial(s)", "vial(s)"),
        ("volume %", "volume %"),
        ("week(s)", "week(s)"),
        ("Weeks,Days", "Weeks,Days"),
        ("x 10(12)/L", "x 10(12)/L"),
        ("x 10(9)/L", "x 10(9)/L"),
        ("x10(12)/L", "x10(12)/L"),
        ("x10(3)/microrliter", "x10(3)/microrliter"),
        ("x10(6)", "x10(6)"),
        ("year(s)", "year(s)")
    )

    unit = forms.ChoiceField(choices=unit_choices, required=False,
                                   widget=forms.Select(attrs={"class": "form-control"}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        else:
            self.request = None
        if kwargs.get('pk'):
            self.pk = kwargs.pop('pk')
        else:
            self.pk = None
        super(LaboratoryTestModelForm, self).__init__(*args, **kwargs)

        self.fields["min_value"].widget = forms.NumberInput()
        self.fields["max_value"].widget = forms.NumberInput()
        self.fields["actual_value"].widget = forms.NumberInput()
        self.fields["test"].widget = forms.TextInput()

        self.fields["test"].widget.attrs["class"] = "form-control typeahead patient-typeahead"
        self.fields["test"].widget.attrs["placeholder"] = "Enter Test Name"

        self.fields["results"].widget = forms.TextInput()
        self.fields["results"].widget.attrs["class"] = "form-control"
        self.fields["results"].widget.attrs["placeholder"] = "Laboratory Test Results"

        self.fields["unit"].widget.attrs["class"] = "form-control"
        self.fields["unit"].widget.attrs["placeholder"] = "Unit"

        self.fields["min_value"].widget.attrs["class"] = "form-control"
        self.fields["min_value"].widget.attrs["placeholder"] = "Min Value"

        self.fields["max_value"].widget.attrs["class"] = "form-control"
        self.fields["max_value"].widget.attrs["placeholder"] = "Max Value"

        self.fields["actual_value"].widget.attrs["class"] = "form-control"
        self.fields["actual_value"].widget.attrs["placeholder"] = "Actual Value"

        self.fields["notes"].widget.attrs["class"] = "form-control"
        self.fields["notes"].widget.attrs["placeholder"] = "Comments"

    def is_valid(self):
        self.errors_messages = []
        self.cleaned_data = {}
        prefix = self.prefix
        test = self.data.get(prefix + "-test")
        results = self.data.get(prefix + "-results")
        unit = self.data.get(prefix + "-unit")
        min_value = self.data.get(prefix + "-min_value")
        max_value = self.data.get(prefix + "-max_value")
        actual_value = self.data.get(prefix + "-actual_value")
        notes = self.data.get(prefix + "-notes")
        mandatory_list = []
        mandatory_list_name = []
        if any([not field for field in mandatory_list]):
            self.errors_messages += [" ".join([word.capitalize() for word in field.split("_")]) + " is required" for
                                     field in mandatory_list_name if not field]
            return False

        id = self.data.get(prefix + "-id")
        try:
            if id:
                id = int(id)
                laboratory_object = LaboratoryTest.objects.get(pk=id)
                id = laboratory_object.pk
        except:
            self.error_messages += ["Invalid laboratory test id"]
            return False

        try:
            if min_value:
                min_value = Decimal(min_value)
        except:
            self.error_messages += ["Invalid min value"]
            return False

        try:
            if max_value:
                max_value = Decimal(max_value)
        except:
            self.error_messages += ["Invalid max value"]
            return False

        try:
            if actual_value:
                actual_value = Decimal(actual_value)
        except:
            self.error_messages += ["Invalid actual value"]
            return False

        self.cleaned_data["id"] = id
        self.cleaned_data["test"] = test
        self.cleaned_data["results"] = results
        self.cleaned_data["min_value"] = min_value
        self.cleaned_data["max_value"] = max_value
        self.cleaned_data["actual_value"] = actual_value
        self.cleaned_data["unit"] = unit
        self.cleaned_data['notes'] = notes

        return True

    def save(self, commit=True):
        if self.pk:
            instance = LaboratoryTest.objects.get(pk=self.pk)
        else:
            instance = LaboratoryTest()

        id = self.cleaned_data["id"]
        test = self.cleaned_data["test"]
        results = self.cleaned_data["results"]
        min_value = self.cleaned_data["min_value"]
        max_value = self.cleaned_data["max_value"]
        actual_value = self.cleaned_data["actual_value"]
        unit = self.cleaned_data["unit"]
        notes = self.cleaned_data["notes"]

        if id:
            instance = LaboratoryTest.objects.get(pk=id)
        else:
            instance = LaboratoryTest()
        instance.test = test
        instance.results = results
        if min_value:
            instance.min_value = min_value
        if max_value:
            instance.max_value = max_value
        if actual_value:
            instance.actual_value = actual_value
        if unit:
            instance.unit = unit
        instance.notes = notes
        instance.save()
        return instance

    class Meta:
        model = LaboratoryTest
        fields = ('id', 'test', 'results', "min_value", "max_value", "actual_value", 'unit', 'notes')