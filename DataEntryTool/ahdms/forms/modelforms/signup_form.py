from django import forms
from django.contrib.auth.models import User
from django.contrib import messages
from core.forms.modelform.basemodelform import BaseModelForm


class UserSignupForm(BaseModelForm):
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={"class": "form-control"}))

    def __init__(self, *args, **kwargs):
        super(UserSignupForm, self).__init__(*args, **kwargs)
        self.fields["username"].widget.attrs["class"] = "form-control"
        self.fields["username"].widget.attrs["placeholder"] = "Username"

        self.fields["email"].widget.attrs["class"] = "form-control"
        self.fields["email"].widget.attrs["placeholder"] = "Email"

        self.fields["password"].widget = forms.PasswordInput()
        self.fields["password"].widget.attrs["class"] = "form-control"
        self.fields["password"].widget.attrs["placeholder"] = "Password"

        self.fields["password2"].widget.attrs["class"] = "form-control"
        self.fields["password2"].widget.attrs["placeholder"] = "Password Again"

    def is_valid(self):
        is_valid = super(UserSignupForm, self).is_valid()
        if is_valid:
            password = self.data.get('password')
            password2 = self.data.get('password2')
            if password != password2:
                self.add_error('password2', "password mismatch")
                return False
            return True
        else:
            return False


    class Meta:
        model = User
        fields = ("username", "email", "is_active", "password")