import os
from django.conf import settings
from django import forms
from django.core.files.base import File
import uuid
from ahdms.forms.fields.document_type_model_choice_field import DocumentTypeModelChoiceField
from ahdms.helpers.aws_s3 import AWSS3
from ahdms.models.document_type import DocumentType
from ahdms.models.document_upload import DocumentUpload
from ahdms.models.order_ticket import OrderTicket
from core.forms.modelform.basemodelform import BaseModelForm


class DocumentUploadModelForm(BaseModelForm):
    document = DocumentTypeModelChoiceField(queryset=DocumentType.objects.all(),
                                            widget=forms.Select(attrs={"class": "form-control"}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        else:
            self.request = None
        if kwargs.get('pk'):
            self.pk = kwargs.pop('pk')
        else:
            self.pk = None

        super(DocumentUploadModelForm, self).__init__(*args, **kwargs)
        self.fields["document"].widget.attrs["class"] = "form-control"
        self.fields["document"].widget.attrs["placeholder"] = "Select Document Type"

        self.fields["file"].widget.attrs["multiple"] = "true"

    def is_valid(self):
        self.errors_messages = []
        self.cleaned_data = {}
        prefix = self.prefix

        document = self.data.get("document")
        files = self.files.getlist("file")
        is_approved = self.data.get("is_approved")

        mandatory_list = [document, files]
        mandatory_list_name = ["document", "files"]
        if any([not field for field in mandatory_list]):
            self.errors_messages += [" ".join([word.capitalize() for word in field.split("_")]) + " is required" for
                                     field in mandatory_list_name if not field]
            return False

        self.cleaned_data["document"] = DocumentType.objects.get(pk=int(document))
        self.cleaned_data["files"] = files
        self.cleaned_data["is_approved"] = True if is_approved else False

        return True

    def save_in_memory_file(self, commit=True, request=None):
        for file in self.cleaned_data["files"]:
            original_file_name = file._name
            file_name = str(uuid.uuid4()) + "." + file._name[file._name.rindex(".") + 1:]
            document_instance = self.cleaned_data["document"]
            uploaded = AWSS3.upload_file_from_memory(file, file_name=file_name, resource_type=document_instance.doc_type)
            if uploaded:
                instance = DocumentUpload()
                instance.document_id = self.cleaned_data["document"].pk
                instance.original_file_name = original_file_name
                instance.file_name = file_name
                instance.is_approved = self.cleaned_data["is_approved"]
                if request:
                    if request.user.is_authenticated():
                        instance.created_by_id = request.user.pk
                instance.aws_upload_status = True

                if uploaded not in [True, False]:
                    instance.aws_resource_id = uploaded.pk
                instance.save()
            else:
                return None
        return True

    def save(self, commit=True, request=None):
        uploaded = self.save_in_memory_file(commit=commit, request=request)
        return uploaded

    def save_v1(self, commit=True, request=None):
        if self.pk:
            instance = DocumentUpload.objects.get(pk=self.pk)
        else:
            instance = DocumentUpload()

        for file in self.cleaned_data["files"]:
            instance = DocumentUpload()
            original_file_name = file._name
            file_name = str(uuid.uuid4()) + "." + file._name[file._name.rindex(".") + 1:]
            save_file_path = os.path.join(settings.UPLOAD_DIR, file_name)
            with open(save_file_path, 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
            attached_file = open(save_file_path, 'r+')
            instance.document_id = self.cleaned_data["document"].pk
            instance.original_file_name = original_file_name
            instance.file_name = file_name
            instance.file = save_file_path
            instance.is_approved = self.cleaned_data["is_approved"]
            if request:
                if request.user.is_authenticated():
                    instance.created_by_id = request.user.pk
            instance.save()

            uploaded = AWSS3.upload_file(save_file_path, file_name=file_name, resource_type=OrderTicket.__name__)
            if uploaded:
                instance.aws_upload_status = True
            else:
                instance.aws_upload_status = False
            instance.save()

        return instance

    class Meta:
        model = DocumentUpload
        fields = ("document", "file", "is_approved")