from datetime import datetime

from decimal import Decimal
from django import forms

from ahdms.helpers.enums import Gender
from ahdms.models.patient import Patient
from core.forms.modelform.basemodelform import BaseModelForm


class PatientModelForm(BaseModelForm):
    id = forms.IntegerField(widget=forms.HiddenInput(attrs={ "class": "form-control" }))

    gender_choices = (
        ("", "Select Gender"),
        (Gender.MALE.value, Gender.MALE.value.capitalize()),
        (Gender.FEMALE.value, Gender.FEMALE.value.capitalize()),
        (Gender.OTHER.value, Gender.OTHER.value.capitalize()),
    )

    gender = forms.ChoiceField(choices=gender_choices,
                               widget=forms.Select(attrs={"class": "form-control"}))

    age = forms.CharField(label="Age", widget=forms.NumberInput(attrs={"class": "form-control", "min": "0"}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        else:
            self.request = None
        if kwargs.get('pk'):
            self.pk = kwargs.pop('pk')
        else:
            self.pk = None
        super(PatientModelForm, self).__init__(*args, **kwargs)
        self.fields["name"].widget.attrs["class"] = "form-control typeahead patient-typeahead"
        self.fields["name"].widget.attrs["placeholder"] = "Patient Name"

        self.fields["patient_id"].widget.attrs["class"] = "form-control"
        self.fields["patient_id"].widget.attrs["placeholder"] = "Patient ID"

        self.fields["age"].widget.attrs["class"] = "form-control"
        self.fields["age"].widget.attrs["placeholder"] = "Age"

        self.fields["weight"].widget.attrs["class"] = "form-control"
        self.fields["weight"].widget.attrs["placeholder"] = "Weight"

        self.fields["height"].widget.attrs["class"] = "form-control"
        self.fields["height"].widget.attrs["placeholder"] = "Height"

        self.fields["bp"].widget.attrs["class"] = "form-control"
        self.fields["bp"].widget.attrs["placeholder"] = "BP eg. 120/80"

        self.fields["gender"].widget.attrs["class"] = "form-control"
        self.fields["gender"].widget.attrs["placeholder"] = "Gender"

        self.fields["date_of_birth"].widget.attrs["class"] = "form-control datepicker"
        self.fields["date_of_birth"].widget.attrs["placeholder"] = "Date of Birth(mm/dd/YYYY)"

        self.fields["phone"].widget.attrs["class"] = "form-control"
        self.fields["phone"].widget.attrs["placeholder"] = "Phone Number"

        
    def is_valid(self):
        self.errors_messages = []
        self.cleaned_data = {}
        prefix = self.prefix
        id = self.data.get(prefix + "-id")
        name = self.data.get(prefix + "-name")
        patient_id = self.data.get(prefix + "-patient_id")
        age = self.data.get(prefix + "-age")
        gender = self.data.get(prefix + "-gender")
        date_of_birth = self.data.get(prefix + "-date_of_birth")
        phone = self.data.get(prefix + "-phone")
        weight = self.data.get(prefix + "-weight")
        height = self.data.get(prefix + "-height")
        bp = self.data.get(prefix + "-bp")
        mandatory_list = [ ]
        mandatory_list_name = [ ]
        if any([not field for field in mandatory_list]):
            self.errors_messages += [" ".join([word.capitalize() for word in field.split("_")]) + " is required" for field in mandatory_list_name if not field ]
            return False
            
        try:
            if date_of_birth:
                date_of_birth = datetime.strptime(date_of_birth, "%m/%d/%Y").date()
            else:
                date_of_birth = None
        except:
            self.error_messages += [ "Invalid date of birth" ]
            return False

        try:
            if id:
                id = int(id)
                patient_object = Patient.objects.get(pk=id)
                id = patient_object.pk
        except:
            self.error_messages += ["Invalid patient id"]
            return False

        try:
            if age:
                age = int(age)
        except:
            self.error_messages += ["Invalid patient age"]
            return False

        if gender:
            if gender not in [Gender.MALE.value, Gender.FEMALE.value, Gender.OTHER.value]:
                self.error_messages += ["Invalid patient gender"]
                return False

        if weight:
            try:
                weight = Decimal(weight)
            except:
                self.error_messages += ["Invalid Weight"]
                return False

        if height:
            try:
                height = Decimal(height)
            except:
                self.error_messages += ["Invalid Height"]
                return False

        systolic = None
        diastolic = None
        if bp:
            try:
                bp_list = bp.split("/")
                systolic = Decimal(bp_list[0])
                diastolic = Decimal(bp_list[1])
            except:
                self.error_messages += ["Invalid BP"]
                return False

        self.cleaned_data["id"] = id
        self.cleaned_data["name"] = name
        self.cleaned_data["patient_id"] = patient_id
        if date_of_birth:
            self.cleaned_data["date_of_birth"] = date_of_birth
        else:
            self.cleaned_data["date_of_birth"] = None

        self.cleaned_data["phone"] = phone
        self.cleaned_data["age"] = age
        self.cleaned_data["gender"] = gender
        self.cleaned_data["weight"] = weight
        self.cleaned_data["height"] = height
        self.cleaned_data["bp"] = bp
        self.cleaned_data["systolic"] = systolic
        self.cleaned_data["diastolic"] = diastolic
        
        return True
            
    def save(self, commit=True):
        if self.pk:
            instance = Patient.objects.get(pk=self.pk)
        else:
            instance = Patient()

        id = self.cleaned_data["id"]
        name = self.cleaned_data["name"]
        patient_id = self.cleaned_data["patient_id"]
        date_of_birth = self.cleaned_data["date_of_birth"]
        phone = self.cleaned_data["phone"]
        age = self.cleaned_data["age"]
        gender = self.cleaned_data["gender"]
        weight = self.cleaned_data["weight"]
        height = self.cleaned_data["height"]
        bp = self.cleaned_data["bp"]
        systolic = self.cleaned_data["systolic"]
        diastolic = self.cleaned_data["diastolic"]

        if id:
            instance = Patient.objects.get(pk=id)
        else:
            instance = Patient()
        instance.name = name
        if patient_id:
            instance.patient_id = patient_id
        if date_of_birth:
            instance.date_of_birth = date_of_birth
        if phone:
            instance.phone = phone
        if age:
            instance.age = age
        if gender:
            instance.gender = gender
        if weight:
            instance.weight = weight
        if height:
            instance.height = height
        if bp:
            instance.bp = bp
        if systolic:
            instance.systolic = systolic
        if diastolic:
            instance.diastolic = diastolic
        instance.save()
        return instance

    class Meta:
        model = Patient
        fields = ('id', 'name', 'patient_id', 'age', 'gender', 'date_of_birth', 'phone',
                  'weight', 'height', 'bp')