from django import forms
from django.contrib.auth.models import User
from ahdms.forms.modelforms.base.document_base_model_form import DocumentBaseModelForm
from ahdms.models.aws_resource import AWSResource
from ahdms.models.document_type import DocumentType
from ahdms.models.laboratory import Laboratory


class LaboratoryModelForm(DocumentBaseModelForm):

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        else:
            self.request = None
        if kwargs.get('pk'):
            self.pk = kwargs.pop('pk')
        else:
            self.pk = None

        super(LaboratoryModelForm, self).__init__(*args, **kwargs)

        self.fields["comment"].widget = forms.HiddenInput()

        self.fields["notes"].widget.attrs["class"] = "form-control"
        self.fields["notes"].widget.attrs["placeholder"] = "Comment"

        self.fields["other_information"].widget.attrs["class"] = "form-control"
        self.fields["other_information"].widget.attrs["placeholder"] = "Other Information"

        self.fields["doc_id"].widget.attrs["class"] = "form-control"
        self.fields["ah_user_id"].widget.attrs["class"] = "form-control"
        self.fields["doc_type_id"].widget.attrs["class"] = "form-control"
        self.fields["ah_aws_resource_id"].widget.attrs["class"] = "form-control"
        self.fields["comment"].widget.attrs["class"] = "form-control"

        # Make fields required explicitely
        self.fields["ah_user_id"].widget.attrs["required"] = "required"
        self.fields["doc_type_id"].widget.attrs["required"] = "required"
        self.fields["ah_aws_resource_id"].widget.attrs["required"] = "required"
        self.fields["comment"].widget.attrs["required"] = "required"

    def is_valid(self):
        self.error_messages = []
        self.cleaned_data = {}

        doc_id = self.data.get("doc_id")
        comment = self.data.get("comment")
        ah_user_id = self.data.get("ah_user_id")
        doc_type_id = self.data.get("doc_type_id")
        ah_aws_resource_id = self.data.get("ah_aws_resource_id")
        mandatory_list = [comment, ah_user_id, doc_type_id, ah_aws_resource_id]
        mandatory_list_name = ["comment", "ah_user_id", "doc_type_id", "ah_aws_resource_id"]
        if any([not field for field in mandatory_list]):
            self.errors_messages += [" ".join([word.capitalize() for word in field.split("_")]) + " is required" for
                                     field in mandatory_list_name if not field]
            return False

        try:
            if doc_id:
                doc_id = int(doc_id)
            else:
                doc_id = None
        except:
            doc_id = None

        try:
            ah_user_id = int(ah_user_id)
            user = User.objects.get(pk=ah_user_id)
            ah_user_id = user.pk
        except:
            self.error_messages += ["Invalid user id"]
            return False

        try:
            doc_type = DocumentType.objects.get(doc_type=doc_type_id)
            doc_type_id = doc_type.pk
        except:
            self.error_messages += ["Invalid document type"]
            return False

        try:
            if ah_aws_resource_id:
                ah_aws_resource_id = int(ah_aws_resource_id)
                aws_resource = AWSResource.objects.get(pk=ah_aws_resource_id)
                ah_aws_resource_id = aws_resource.pk
        except:
            self.error_messages += ["Invalid resource id"]
            return False

        self.cleaned_data["doc_id"] = doc_id
        self.cleaned_data["comment"] = comment
        self.cleaned_data["ah_user_id"] = ah_user_id
        self.cleaned_data["doc_type_id"] = doc_type_id
        self.cleaned_data["ah_aws_resource_id"] = ah_aws_resource_id

        return True

    def save(self, commit=True):
        if self.pk:
            instance = Laboratory.objects.get(pk=self.pk)
        else:
            instance = Laboratory()

        comment = self.cleaned_data["comment"]
        ah_user_id = self.cleaned_data["ah_user_id"]
        doc_type_id = self.cleaned_data["doc_type_id"]
        ah_aws_resource_id = self.cleaned_data["ah_aws_resource_id"]

        instance.comment = comment
        instance.user_id = ah_user_id
        instance.type_id = doc_type_id
        instance.aws_resource_id = ah_aws_resource_id
        instance.save()
        return instance

    class Meta:
        model = Laboratory
        fields = ('doc_id', 'is_managed', 'ah_user_id', 'doc_type_id', 'ah_aws_resource_id', "comment", "notes", "other_information")