from django import forms
from datetime import datetime
from django.contrib.auth.models import User
from ahdms.forms.modelforms.base.document_base_model_form import DocumentBaseModelForm
from ahdms.helpers.enums import BillTo, Modality, Routing, Examination
from ahdms.models.aws_resource import AWSResource
from ahdms.models.document_type import DocumentType
from ahdms.models.order_ticket import OrderTicket


class OrderTicketModelForm(DocumentBaseModelForm):
    bill_to_choices = (
        ("", "Bill To ..."),
        (BillTo.PATIENTS.value, BillTo.PATIENTS.value.capitalize()),
        (BillTo.HOSPITAL.value, BillTo.HOSPITAL.value.capitalize()),
        (BillTo.LAB.value, BillTo.LAB.value.capitalize()),
        (BillTo.HMO.value, BillTo.HMO.value.capitalize()),
    )

    bill_to = forms.ChoiceField(choices=bill_to_choices, required=False,
                                widget=forms.Select(attrs={"class": "form-control"}))

    modality_choices = (
        ("", "Select Modality"),
        (Modality.CT.value, Modality.CT.value.capitalize()),
        (Modality.MG.value, Modality.MG.value.capitalize()),
        (Modality.MRI.value, Modality.MRI.value.capitalize()),
        (Modality.XRAY.value, Modality.XRAY.value.capitalize()),
        (Modality.US.value, Modality.US.value.capitalize()),
        (Modality.FL.value, Modality.FL.value.capitalize()),
    )

    modality = forms.ChoiceField(choices=modality_choices, required=False,
                                 widget=forms.Select(attrs={"class": "form-control"}))

    routing_choices = (
        ("", "Select Routing"),
        (Routing.APOLLO.value, Routing.APOLLO.value.capitalize()),
        (Routing.USA.value, Routing.USA.value.capitalize()),
        (Routing.RAD.value, Routing.RAD.value.capitalize()),
        (Routing.MAX_HEALTH.value, Routing.MAX_HEALTH.value.capitalize()),
    )

    routing = forms.ChoiceField(choices=routing_choices, required=False,
                                widget=forms.Select(attrs={"class": "form-control"}))

    examination_choices = (
        ("", "Select Examination"),
        (Examination.ECG.value, Examination.ECG.value.capitalize()),
        (Examination.STRESS_TEST.value, Examination.STRESS_TEST.value.capitalize()),
        (Examination.ECOCARDIOGRAPHY.value, Examination.ECOCARDIOGRAPHY.value.capitalize()),
        (Examination.OTHER.value, Examination.OTHER.value.capitalize()),
    )

    examination = forms.ChoiceField(choices=examination_choices, required=False,
                                    widget=forms.Select(attrs={"class": "form-control"}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        else:
            self.request = None
        if kwargs.get('pk'):
            self.pk = kwargs.pop('pk')
        else:
            self.pk = None

        super(OrderTicketModelForm, self).__init__(*args, **kwargs)

        self.fields["comment"].widget = forms.HiddenInput()

        self.fields["date"].widget.attrs["class"] = "form-control datepicker"
        self.fields["date"].widget.attrs["placeholder"] = "Date"

        self.fields["accession"].widget.attrs["class"] = "form-control"
        self.fields["accession"].widget.attrs["placeholder"] = "Accession"

        self.fields["clinical_history"].widget.attrs["class"] = "form-control"
        self.fields["clinical_history"].widget.attrs["placeholder"] = "Clinical Description/History"

        self.fields["body_part"].widget.attrs["class"] = "form-control"
        self.fields["body_part"].widget.attrs["placeholder"] = "Body Part"

        self.fields["series_description"].widget.attrs["class"] = "form-control"
        self.fields["series_description"].widget.attrs["placeholder"] = "Series Description"

        self.fields["images_count"].widget.attrs["class"] = "form-control"
        self.fields["images_count"].widget.attrs["placeholder"] = "Images (Number)"

        self.fields["study_datetime"].widget.attrs["class"] = "form-control datepicker"
        self.fields["study_datetime"].widget.attrs["placeholder"] = "Study Date/Time"

        self.fields["diabetic_retinopathy_screening"].widget = forms.TextInput()
        self.fields["diabetic_retinopathy_screening"].widget.attrs["class"] = "form-control"
        self.fields["diabetic_retinopathy_screening"].widget.attrs["placeholder"] = "Diabetic Retinopathy Screening"

        self.fields["eye_services_details"].widget.attrs["class"] = "form-control"
        self.fields["eye_services_details"].widget.attrs["placeholder"] = "Details"

        self.fields["dental_details"].widget.attrs["class"] = "form-control"
        self.fields["dental_details"].widget.attrs["placeholder"] = "Details"

        self.fields["examination"].widget.attrs["class"] = "form-control"
        self.fields["examination"].widget.attrs["placeholder"] = "Examination"

        self.fields["cardiac_test_details"].widget.attrs["class"] = "form-control"
        self.fields["cardiac_test_details"].widget.attrs["placeholder"] = "Cardiac Test Details"

        self.fields["other_information"].widget.attrs["class"] = "form-control"
        self.fields["other_information"].widget.attrs["placeholder"] = "Other Information"

    def is_valid(self):
        self.error_messages = []
        self.cleaned_data = {}

        doc_id = self.data.get("doc_id")
        comment = self.data.get("comment")
        ah_user_id = self.data.get("ah_user_id")
        doc_type_id = self.data.get("doc_type_id")
        ah_aws_resource_id = self.data.get("ah_aws_resource_id")

        date = self.data.get("date")
        accession = self.data.get("accession")
        clinical_history = self.data.get("clinical_history")
        bill_to = self.data.get("bill_to")

        routing = self.data.get("routing")
        body_part = self.data.get("body_part")
        series_description = self.data.get("series_description")
        images_count = self.data.get("images_count")
        study_datetime = self.data.get("study_datetime")

        modality = self.data.get("modality")

        diabetic_retinopathy_screening = self.data.get("diabetic_retinopathy_screening")
        eye_services_details = self.data.get("eye_services_details")

        dental_details = self.data.get("dental_details")

        examination = self.data.get("examination")
        cardiac_test_details = self.data.get("cardiac_test_details")

        mandatory_list = []
        mandatory_list_name = []
        if any([not field for field in mandatory_list]):
            self.errors_messages += [" ".join([word.capitalize() for word in field.split("_")]) + " is required" for
                                     field in mandatory_list_name if not field]
            return False

        try:
            if doc_id:
                doc_id = int(doc_id)
            else:
                doc_id = None
        except:
            doc_id = None

        try:
            ah_user_id = int(ah_user_id)
            user = User.objects.get(pk=ah_user_id)
            ah_user_id = user.pk
        except:
            self.error_messages += ["Invalid user id"]
            return False

        try:
            doc_type = DocumentType.objects.get(doc_type=doc_type_id)
            doc_type_id = doc_type.pk
        except Exception as exp:
            self.error_messages += ["Invalid document type"]
            return False

        try:
            if ah_aws_resource_id:
                ah_aws_resource_id = int(ah_aws_resource_id)
                aws_resource = AWSResource.objects.get(pk=ah_aws_resource_id)
                ah_aws_resource_id = aws_resource.pk
        except:
            self.error_messages += ["Invalid resource id"]
            return False

        try:
            if date:
                date = datetime.strptime(date, "%m/%d/%Y").date()
            else:
                date = None
        except:
            self.error_messages += ["Invalid date"]
            return False

        try:
            if images_count:
                images_count = int(images_count)
        except:
            self.error_messages += ["Invalid Images count"]
            return False

        try:
            if study_datetime:
                study_datetime = datetime.strptime(study_datetime, "%m/%d/%Y").date()
            else:
                study_datetime = None
        except:
            self.error_messages += ["Invalid study datetime"]
            return False

        self.cleaned_data["doc_id"] = doc_id
        self.cleaned_data["comment"] = comment
        self.cleaned_data["ah_user_id"] = ah_user_id
        self.cleaned_data["doc_type_id"] = doc_type_id
        self.cleaned_data["ah_aws_resource_id"] = ah_aws_resource_id

        self.cleaned_data["date"] = date
        self.cleaned_data["accession"] = accession
        self.cleaned_data["clinical_history"] = clinical_history
        self.cleaned_data["bill_to"] = bill_to

        self.cleaned_data["routing"] = routing
        self.cleaned_data["body_part"] = body_part
        self.cleaned_data["series_description"] = series_description
        self.cleaned_data["images_count"] = images_count

        self.cleaned_data["study_datetime"] = study_datetime

        self.cleaned_data["modality"] = modality

        self.cleaned_data["diabetic_retinopathy_screening"] = diabetic_retinopathy_screening
        self.cleaned_data["eye_services_details"] = eye_services_details

        self.cleaned_data["dental_details"] = dental_details
        self.cleaned_data["examination"] = examination
        self.cleaned_data["cardiac_test_details"] = cardiac_test_details

        return True

    def save(self, commit=True):
        if self.pk:
            instance = OrderTicket.objects.get(pk=self.pk)
        else:
            instance = OrderTicket()

        comment = self.cleaned_data["comment"]
        ah_user_id = self.cleaned_data["ah_user_id"]
        doc_type_id = self.cleaned_data["doc_type_id"]
        ah_aws_resource_id = self.cleaned_data["ah_aws_resource_id"]

        date = self.cleaned_data.get("date")
        accession = self.cleaned_data.get("accession")
        clinical_history = self.cleaned_data.get("clinical_history")
        bill_to = self.cleaned_data.get("bill_to")

        routing = self.cleaned_data.get("routing")
        body_part = self.cleaned_data.get("body_part")
        series_description = self.cleaned_data.get("series_description")
        images_count = self.cleaned_data.get("images_count")

        study_datetime = self.cleaned_data.get("study_datetime")

        modality = self.cleaned_data.get("modality")

        diabetic_retinopathy_screening = self.cleaned_data.get("diabetic_retinopathy_screening")
        eye_services_details = self.cleaned_data.get("eye_services_details")

        dental_details = self.cleaned_data.get("dental_details")

        examination = self.cleaned_data.get("examination")
        cardiac_test_details = self.cleaned_data.get("cardiac_test_details")

        instance.comment = comment
        instance.user_id = ah_user_id
        instance.type_id = doc_type_id
        instance.aws_resource_id = ah_aws_resource_id
        if date:
            instance.date = date
        instance.accession = accession
        instance.clinical_history = clinical_history
        instance.bill_to = bill_to
        instance.modality = modality
        instance.routing = routing
        instance.body_part = body_part
        instance.series_description = series_description
        if images_count:
            instance.images_count = images_count
        if study_datetime:
            instance.study_datetime = study_datetime
        instance.diabetic_retinopathy_screening = diabetic_retinopathy_screening
        instance.eye_services_details = eye_services_details
        instance.dental_details = dental_details
        instance.examination = examination
        instance.cardiac_test_details = cardiac_test_details
        instance.save()
        return instance

    class Meta:
        model = OrderTicket
        fields = ("doc_id", "date", "accession", "clinical_history", "bill_to", "comment",
                  "modality", "routing", "body_part", "series_description", "images_count",
                  "study_datetime", "diabetic_retinopathy_screening", "eye_services_details",
                  "dental_details", "examination", "cardiac_test_details", "other_information")