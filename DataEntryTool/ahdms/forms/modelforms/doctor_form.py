from django import forms
from ahdms.models.doctor import Doctor
from core.forms.modelform.basemodelform import BaseModelForm


class DoctorModelForm(BaseModelForm):
    id = forms.IntegerField(widget=forms.HiddenInput(attrs={"class": "form-control"}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        else:
            self.request = None
        if kwargs.get('pk'):
            self.pk = kwargs.pop('pk')
        else:
            self.pk = None

        super(DoctorModelForm, self).__init__(*args, **kwargs)
        self.fields["name"].widget.attrs["class"] = "form-control"
        self.fields["name"].widget.attrs["placeholder"] = "Name"

        self.fields["phone"].widget.attrs["class"] = "form-control"
        self.fields["phone"].widget.attrs["placeholder"] = "Phone"

    def is_valid(self):
        self.errors_messages = []
        self.cleaned_data = {}
        prefix = self.prefix
        id = self.data.get(prefix + "-id")
        name = self.data.get(prefix + "-name")
        phone = self.data.get(prefix + "-phone")

        mandatory_list = []
        mandatory_list_name = []
        if any([not field for field in mandatory_list]):
            self.errors_messages += [" ".join([word.capitalize() for word in field.split("_")]) + " is required" for
                                     field in mandatory_list_name if not field]
            return False

        try:
            if id:
                id = int(id)
                doctor_object = Doctor.objects.get(pk=id)
                id = doctor_object.pk
        except:
            self.error_messages += ["Invalid doctor id"]
            return False

        self.cleaned_data["id"] = id
        self.cleaned_data["name"] = name
        self.cleaned_data["phone"] = phone

        return True

    def save(self, commit=True):
        if self.pk:
            instance = Doctor.objects.get(pk=self.pk)
        else:
            instance = Doctor()

        id = self.cleaned_data["id"]
        name = self.cleaned_data["name"]
        phone = self.cleaned_data["phone"]

        if id:
            instance = Doctor.objects.get(pk=id)
        else:
            instance = Doctor()
        instance.name = name
        instance.phone = phone
        instance.save()
        return instance

    class Meta:
        model = Doctor
        fields = ('id', 'name', 'phone')