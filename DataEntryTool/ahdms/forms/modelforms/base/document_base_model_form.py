from django import forms
from core.forms.modelform.basemodelform import BaseModelForm


class DocumentBaseModelForm(BaseModelForm):
    doc_id = forms.IntegerField(widget=forms.HiddenInput())
    is_managed = forms.IntegerField(widget=forms.HiddenInput())
    ah_user_id = forms.IntegerField(widget=forms.HiddenInput())
    doc_type_id = forms.IntegerField(widget=forms.HiddenInput())
    ah_aws_resource_id = forms.IntegerField(widget=forms.HiddenInput())