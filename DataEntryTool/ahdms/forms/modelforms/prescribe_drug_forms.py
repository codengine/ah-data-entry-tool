from django import forms
from ahdms.models.medical_drug import MedicalDrug
from ahdms.models.prescribed_drug import PrescribedDrug
from core.forms.modelform.basemodelform import BaseModelForm


class PrescribedDrugModelForm(BaseModelForm):
    id = forms.IntegerField(widget=forms.HiddenInput(attrs={"class": "form-control"}))

    drug_name = forms.CharField(max_length=500, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter drug name'}))

    route_choices = (
        ("", "Select Route"),
        ("oral", "Oral"),
        ("injection", "Injection"),
        ("rectal", "Rectal"),
        ("vaginal", "Vaginal"),
        ("ocular", "Ocular"),
        ("otic", "Otic"),
        ("nasal", "Nasal"),
        ("inhalation", "Inhalation"),
        ("nebulization", "Nebulization"),
        ("transdermal", "Transdermal"),
        ("sublingual", "Sublingual"),
        ("buccally", "Buccaly")
    )

    route = forms.ChoiceField(choices=route_choices, required=False,
                             widget=forms.Select(attrs={"class": "form-control"}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        else:
            self.request = None
        if kwargs.get('pk'):
            self.pk = kwargs.pop('pk')
        else:
            self.pk = None
        super(PrescribedDrugModelForm, self).__init__(*args, **kwargs)

        self.fields["dosage_number"].widget = forms.NumberInput()
        self.fields["dosage_number"].widget.attrs["class"] = "form-control"
        self.fields["dosage_number"].widget.attrs["placeholder"] = "Dosage Number"

        self.fields["dosage_unit"].widget.attrs["class"] = "form-control"
        self.fields["dosage_unit"].widget.attrs["placeholder"] = "Dosage Unit"

        self.fields["drug_type"].widget.attrs["class"] = "form-control"
        self.fields["drug_type"].widget.attrs["placeholder"] = "Drug Type"

        self.fields["drug_details"].widget.attrs["class"] = "form-control"
        self.fields["drug_details"].widget.attrs["placeholder"] = "Drug Details"

        self.fields["frequency"].widget = forms.NumberInput()
        self.fields["frequency"].widget.attrs["class"] = "form-control"
        self.fields["frequency"].widget.attrs["placeholder"] = "Frequency"

        self.fields["duration"].widget = forms.NumberInput()
        self.fields["duration"].widget.attrs["class"] = "form-control"
        self.fields["duration"].widget.attrs["placeholder"] = "Duration"

        self.fields["route"].widget.attrs["class"] = "form-control"
        self.fields["route"].widget.attrs["placeholder"] = "Route"

        self.fields["indication"].widget = forms.TextInput()
        self.fields["indication"].widget.attrs["class"] = "form-control"
        self.fields["indication"].widget.attrs["placeholder"] = "Indication"

        self.fields["special_instruction"].widget = forms.Textarea()
        self.fields["special_instruction"].widget.attrs["class"] = "form-control"
        self.fields["special_instruction"].widget.attrs["placeholder"] = "Special Instructions"

        # self.fields["name"].widget.attrs["required"] = "required"

    def is_valid(self):
        self.errors_messages = []
        self.cleaned_data = {}
        prefix = self.prefix
        id = self.data.get(prefix + "-id")
        drug_type = self.data.get(prefix + "-drug_type")
        drug_details = self.data.get(prefix + "-drug_details")
        frequency = self.data.get(prefix + "-frequency")
        mandatory_list = []
        mandatory_list_name = []
        if any([not field for field in mandatory_list]):
            self.errors_messages += [" ".join([word.capitalize() for word in field.split("_")]) + " is required" for
                                     field in mandatory_list_name if not field]
            return False

        try:
            if frequency:
                frequency = int(frequency)
        except:
            self.error_messages += ["Invalid frequency"]
            return False

        try:
            if id:
                id = int(id)
                object = PrescribedDrug.objects.get(pk=id)
                id = object.pk
        except:
            self.error_messages += ["Invalid patient id"]
            return False

        self.cleaned_data["id"] = id
        self.cleaned_data["drug_type"] = drug_type
        self.cleaned_data["drug_details"] = drug_details
        self.cleaned_data["frequency"] = frequency

        return True

    def save(self, commit=True):
        if self.pk:
            instance = PrescribedDrug.objects.get(pk=self.pk)
        else:
            instance = PrescribedDrug()

        id = self.cleaned_data["id"]
        drug_type = self.cleaned_data['drug_type']
        drug_details = self.cleaned_data["drug_details"]
        frequency = self.cleaned_data["frequency"]

        if id:
            instance = PrescribedDrug.objects.get(pk=id)
        else:
            instance = PrescribedDrug()

        if frequency:
            instance.frequency = frequency
        if drug_type:
            instance.drug_type = drug_type
        if drug_details:
            instance.drug_details = drug_details
        instance.save()
        return instance

    class Meta:
        model = PrescribedDrug
        fields = ('id', 'drug_name', 'dosage_number', 'dosage_unit', 'frequency',
                  'duration', 'route', 'indication', 'special_instruction', 'drug_type',
                  'drug_details')