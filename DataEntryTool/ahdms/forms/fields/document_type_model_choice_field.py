from core.forms.fields.base_model_choice import BaseModelChoice


class DocumentTypeModelChoiceField(BaseModelChoice):
    def label_from_instance(self, obj):
        return obj.doc_type