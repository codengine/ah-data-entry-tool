from core.forms.fields.base_model_choice import BaseModelChoice


class CityModelChoiceField(BaseModelChoice):
    def label_from_instance(self, obj):
        return obj.name