from django import forms
from allauth.account.forms import LoginForm


class CustomLoginForm(LoginForm):
    def __init__(self, *args, **kwargs):
        super(CustomLoginForm, self).__init__(*args, **kwargs)
        self.fields['login'].label = ''
        self.fields['login'].widget = forms.TextInput(attrs={
            'placeholder': 'Email',
            'autofocus': 'autofocus',
            'class': 'form-control top',
            })
        self.fields['password'].label = ''
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'placeholder': 'Password',
            'class': 'form-control bottom',
            })