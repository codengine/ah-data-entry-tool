from django.db import models
from core.models.base_entity import BaseEntity


class MedicalDrug(BaseEntity):
    name = models.CharField(max_length=500, blank=True, null=True)