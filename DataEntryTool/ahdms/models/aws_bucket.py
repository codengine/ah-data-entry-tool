from django.db import models
from core.models.base_entity import BaseEntity


class AWSBucket(BaseEntity):
    bucket_name = models.CharField(max_length=500)

    class Meta:
        app_label = 'ahdms'