from django.contrib.auth.models import User
from django.db import models
from ahdms.models.aws_resource import AWSResource
from core.models.base_entity import BaseEntity


class UserSkippedResource(BaseEntity):
    user = models.ForeignKey(User)
    resource = models.ForeignKey(AWSResource)
    
    class Meta:
        app_label = 'ahdms'