from django.db import models
from ahdms.models.medical_drug import MedicalDrug
from core.models.base_entity import BaseEntity


class PrescribedDrug(BaseEntity):
    drug_type = models.CharField(max_length=200, null=True, blank=True)
    drug_details = models.TextField(null=True, blank=True)
    drug = models.ForeignKey(MedicalDrug, null=True)
    dosage_number = models.IntegerField(null=True, blank=True)
    dosage_unit = models.CharField(max_length=255, null=True, blank=True)
    frequency = models.IntegerField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    route = models.CharField(max_length=255, blank=True, null=True)
    indication = models.TextField(null=True, blank=True)
    special_instruction = models.TextField(null=True, blank=True)