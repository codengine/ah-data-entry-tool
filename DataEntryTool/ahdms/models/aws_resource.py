from ahdms.models.aws_resource_abstract import AWSResourceAbstract


class AWSResource(AWSResourceAbstract):
    pass

    class Meta:
        app_label = 'ahdms'

    def object2serialized(self):
        return {
            "resource_id": self.pk,
            "bucket_name": self.bucket.bucket_name,
            "resource_type": self.resource_type.pk,
            "resource_type_name": self.resource_type.doc_type,
            "patient_id": self.resource_id if self.resource_id else "",
            "resource_name": self.resource_name,
            "resource_uuid": self.resource_name[:self.resource_name.rindex(".")],
            "resource_url": self.resource_url,
            "status": self.status
        }
