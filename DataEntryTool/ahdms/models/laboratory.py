from django.db import models
from ahdms.models.aws_document import AWSDocument
from ahdms.models.laboratory_test import LaboratoryTest


class Laboratory(AWSDocument):
    tests = models.ManyToManyField(LaboratoryTest)

    class Meta:
        app_label = 'ahdms'

    def as_text(self, fields=[], m2m_fields={}, date_formats={}):
        fields = ["patients", "facilities", "physicians", "tests", "type__doc_type:Document Type"]
        dt_formats = {}
        m2m = {
            "patients": ["name", "patient_id", "age", "date_of_birth", "phone", "weight", "height", "bp", "systolic", "diastolic"],
            "facilities": ["name", "facility_id", "country_id", "country__name", "city_id", "city__name", "phone"],
            "physicians": ["name", "physician_id", "phone", "affilicated_facility"],
            "tests": ["test", "results", "min_value", "max_value", "actual_value",
                      "unit", "notes"]
        }
        return super(Laboratory, self).as_text(fields=fields, m2m_fields=m2m, date_formats=dt_formats)

    def get_match_fields(self):
        return {
            "type_id": self.get_match_field_regular(),
            "patients": self.get_match_field_m2m(fields=["name", "patient_id", "age", "date_of_birth", "phone", "weight", "height", "bp", "systolic", "diastolic"]),
            "facilities": self.get_match_field_m2m(
                fields=["name", "facility_id", "country_id", "country__name", "city_id", "city__name", "phone"]),
            "physicians": self.get_match_field_m2m(
                fields=["name", "physician_id", "phone", "affilicated_facility"]),
            "tests": self.get_match_field_m2m(
                fields=["test", "results", "min_value", "max_value", "actual_value",
                      "unit", "notes"])
        }