from ahdms.models.aws_document import AWSDocument


class OtherDoc(AWSDocument):

    class Meta:
        app_label = 'ahdms'

    def as_text(self, fields=[], m2m_fields={}, date_formats={}):
        fields = ["type__doc_type:Document Type", "other_information"]
        dt_formats = {}
        m2m = {
        }
        return super(OtherDoc, self).as_text(fields=fields, m2m_fields=m2m, date_formats=dt_formats)

    def get_match_fields(self):
        return {
            "type_id": self.get_match_field_regular(),
            "other_information": self.get_match_field_regular()
        }