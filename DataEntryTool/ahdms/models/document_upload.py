import os
from django.contrib.auth.models import User
from django.db import models
from ahdms.helpers.enums import RequestSource
from ahdms.models.aws_resource import AWSResource
from ahdms.models.document_type import DocumentType
from core.models.base_entity import BaseEntity
from django.conf import settings


class DocumentUpload(BaseEntity):
    document = models.ForeignKey(DocumentType)
    original_file_name = models.CharField(max_length=500, null=True, blank=True)
    file_name = models.CharField(max_length=500, null=True, blank=True)
    file = models.FileField(max_length=500, upload_to=os.path.join(settings.MEDIA_ROOT, "uploads"))
    is_approved = models.BooleanField(default=False)
    approved_by = models.ForeignKey(User, null=True)
    request_source = models.CharField(max_length=200, default=RequestSource.WEB.value)
    aws_upload_status = models.BooleanField(default=False)
    aws_resource = models.ForeignKey(AWSResource, null=True)