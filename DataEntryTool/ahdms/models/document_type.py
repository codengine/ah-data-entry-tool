from django.db import models
from core.models.base_entity import BaseEntity


class DocumentType(BaseEntity):
    doc_type = models.CharField(max_length=200)

    class Meta:
        app_label = 'ahdms'
        
    @classmethod
    def get_all_document_types(cls):
        doc_type_objects = cls.objects.all()
        d = {}
        for doc_type_object in doc_type_objects:
            d[doc_type_object.doc_type] = doc_type_object
        return d