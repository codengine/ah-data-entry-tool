from ahdms.models.doctor import Doctor
from ahdms.models.document_type import DocumentType
from ahdms.models.laboratory_test import LaboratoryTest
from django.db import models
from ahdms.models.aws_document import AWSDocument


class OrderTicket(AWSDocument):
    date = models.DateTimeField(null=True, blank=True)
    accession = models.TextField(null=True, blank=True)
    clinical_history = models.TextField(null=True, blank=True)
    doctors = models.ManyToManyField(Doctor)
    bill_to = models.CharField(max_length=200, null=True, blank=True)

    #Radiology
    modality = models.CharField(max_length=200, null=True, blank=True) #Modality.CT.value
    routing = models.CharField(max_length=200, null=True, blank=True)
    body_part = models.TextField(null=True, blank=True)
    series_description = models.TextField(null=True, blank=True)
    images_count = models.IntegerField(null=True, blank=True)
    study_datetime = models.DateTimeField(null=True, blank=True)

    #Laboratory
    tests_required = models.ManyToManyField(LaboratoryTest)

    #Eye Services
    diabetic_retinopathy_screening = models.TextField(null=True, blank=True)
    eye_services_details = models.TextField(null=True, blank=True)

    #Dental Services
    dental_details = models.TextField(null=True, blank=True)

    #Cardiac Tests
    examination = models.CharField(max_length=200, null=True, blank=True) # Examination.ECG.value
    cardiac_test_details = models.TextField(null=True, blank=True)

    def as_text(self, fields=[], m2m_fields={}, date_formats={}):
        fields = ["patients", "facilities", "physicians", "type__doc_type:Document Type", "date",
                  "accession", "clinical_history", "bill_to", "modality", "routing",
                  "body_part", "series_description", "images_count", "study_datetime",
                  "diabetic_retinopathy_screening", "eye_services_details", "dental_details",
                  "examination", "cardiac_test_details"]

        dt_formats = {"date": "%m/%d/%Y", "study_datetime": "%m/%d/%Y"}
        m2m = {
            "patients": ["name", "patient_id", "age", "date_of_birth", "phone", "weight", "height", "bp", "systolic",
                         "diastolic"],
            "facilities": ["name", "facility_id", "country_id", "country__name", "city_id", "city__name", "phone"],
            "physicians": ["name", "physician_id", "phone", "affilicated_facility"]
        }
        return super(OrderTicket, self).as_text(fields=fields, m2m_fields=m2m, date_formats=dt_formats)

    class Meta:
        app_label = 'ahdms'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        doc_type_objects = DocumentType.objects.filter(doc_type=self.__class__.__name__)

        if doc_type_objects.exists():
            doc_type_object = doc_type_objects.first()
            self.type_id = doc_type_object.pk
        super(OrderTicket, self).save(force_insert=False, force_update=False, using=None,
                                      update_fields=None)

    def get_match_fields(self):
        return {
            "type_id": self.get_match_field_regular(),
            "date": self.get_match_field_regular(),
            "accession": self.get_match_field_regular(),
            "clinical_history": self.get_match_field_regular(),
            "bill_to": self.get_match_field_regular(),
            "modality": self.get_match_field_regular(),
            "routing": self.get_match_field_regular(),
            "body_part": self.get_match_field_regular(),
            "series_description": self.get_match_field_regular(),
            "images_count": self.get_match_field_regular(),
            "study_datetime": self.get_match_field_regular(),
            "diabetic_retinopathy_screening": self.get_match_field_regular(),
            "eye_services_details": self.get_match_field_regular(),
            "dental_details": self.get_match_field_regular(),
            "examination": self.get_match_field_regular(),
            "cardiac_test_details": self.get_match_field_regular(),
            "patients": self.get_match_field_m2m(
                fields=["name", "patient_id", "age", "date_of_birth", "phone", "weight", "height", "bp", "systolic",
                         "diastolic"]),
            "physicians": self.get_match_field_m2m(fields=["name", "physician_id", "phone", "affilicated_facility"]),
            "facilities": self.get_match_field_m2m(
                fields=["name", "facility_id", "country_id", "country__name", "city_id", "city__name", "phone"])
        }

