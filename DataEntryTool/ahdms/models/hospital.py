from django.db import models
from core.models.base_entity import BaseEntity
from core.models.city import City
from core.models.country import Country


class Hospital(BaseEntity):
    name = models.CharField(max_length=128)
    city = models.ForeignKey(City, null=True)
    country = models.ForeignKey(Country, null=True)
    signature_id = models.IntegerField(blank=True, null=True)
    referral_code = models.IntegerField(blank=True, null=True)
    provider_id = models.IntegerField(blank=True, null=True)
    radiologist = models.SmallIntegerField()

    class Meta:
        app_label = 'ahdms'