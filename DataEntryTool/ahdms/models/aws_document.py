from django.db import models
from ahdms.models.aws_resource import AWSResource
from ahdms.models.document import Document


class AWSDocument(Document):
    aws_resource = models.ForeignKey(AWSResource)
    other_information = models.TextField(blank=True, null=True)
    source_document_type = models.CharField(max_length=200, null=True, blank=True)
    source_document_id = models.BigIntegerField(null=True, blank=True) # In case of manager submission it holds the previous document id
    approval_source = models.CharField(max_length=255, blank=True, null=True) #DocumentApprovalSource Enum
    
    class Meta:
        abstract = True