from django.db import models
from core.models.base_entity import BaseEntity


class LaboratoryTest(BaseEntity):
    test_name = models.CharField(max_length=500, null=True, blank=True)
    test = models.TextField(blank=True, null=True)
    results = models.TextField(blank=True, null=True)
    min_value = models.DecimalField(decimal_places=10, max_digits=20, null=True, blank=True)
    max_value = models.DecimalField(decimal_places=10, max_digits=20, null=True, blank=True)
    actual_value = models.DecimalField(decimal_places=10, max_digits=20, null=True, blank=True)
    unit = models.IntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)