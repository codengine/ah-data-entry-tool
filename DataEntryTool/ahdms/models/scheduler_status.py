from django.db import models
from core.models.base_entity import BaseEntity
from engine.clock import Clock


class SchedulerStatus(BaseEntity):
    details = models.TextField(null=True, blank=True)

    @classmethod
    def update_status(cls, text=None):
        instance = cls()
        if text:
            instance.details = text
        instance.last_updated = Clock.utc_now()
        instance.save()

    @classmethod
    def get_last_schedule_time(cls):
        instances = cls.objects.all().order_by('-date_created')
        if instances.exists():
            return instances.first().last_updated.strftime("%d/%m/%Y")
        return "Not Available"