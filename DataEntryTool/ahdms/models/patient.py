from django.db import models
from core.models.base_entity import BaseEntity


class Patient(BaseEntity):
    name = models.CharField(max_length=128, blank=True, null=True)
    patient_id = models.CharField(max_length=128, blank=True, null=True)
    age = models.CharField(max_length=10, blank=True, null=True)
    gender = models.CharField(max_length=100, null=True, blank=True)  # Gender.MALE.value
    date_of_birth = models.DateField(max_length=128, blank=True, null=True)
    phone = models.CharField(max_length=128, blank=True, null=True)
    weight = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    height = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    systolic = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True) # For bp like 120/80, 120 is the systolic and 80 is the diastolic
    diastolic = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    bp = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        app_label = 'ahdms'