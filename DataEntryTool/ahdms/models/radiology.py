from django.db import models
from ahdms.models.aws_document import AWSDocument
from ahdms.models.document_type import DocumentType
from core.models.country import Country


class Radiology(AWSDocument):
    country = models.ForeignKey(Country, blank=True, null=True)
    radio_type = models.CharField(max_length=100, blank=True, null=True)
    clinic_info = models.TextField(max_length=1000, blank=True, null=True)
    comparison = models.TextField(max_length=1000, blank=True, null=True)
    findings = models.TextField(max_length=1000, blank=True, null=True)
    impression = models.TextField(max_length=1000, blank=True, null=True)
    imaging_date = models.DateField(max_length=1000, blank=True, null=True)
    procedure = models.TextField(blank=True, null=True)
    reporting_at_date = models.DateField(null=True, blank=True)
    reporting_radiologist_name = models.CharField(max_length=100, blank=True, null=True)

    def as_text(self, fields=[], m2m_fields={}, date_formats={}):
        fields = ["patients", "facilities", "physicians", "type__doc_type:Document Type", "imaging_date",
                  "clinic_info", "findings", "impression", "procedure", "reporting_at_date",
                  "reporting_radiologist_name"]

        dt_formats = {"imaging_date": "%m/%d/%Y", "reporting_at_date": "%m/%d/%Y"}
        m2m = {
            "patients": ["name", "patient_id", "age", "date_of_birth", "phone", "weight", "height", "bp", "systolic", "diastolic"],
            "facilities": ["name", "facility_id", "country_id", "country__name", "city_id", "city__name", "phone"],
            "physicians": ["name", "physician_id", "phone", "affilicated_facility"]
        }
        return super(Radiology, self).as_text(fields=fields, m2m_fields=m2m, date_formats=dt_formats)

    class Meta:
        app_label = 'ahdms'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        doc_type_objects = DocumentType.objects.filter(doc_type=self.__class__.__name__)
        if doc_type_objects.exists():
            doc_type_object = doc_type_objects.first()
            self.type_id = doc_type_object.pk
        super(Radiology, self).save(force_insert=False, force_update=False, using=None,
             update_fields=None)

    def get_match_fields(self):
        return {
            "type_id": self.get_match_field_regular(),
            "procedure": self.get_match_field_regular(),
            "imaging_date": self.get_match_field_regular(),
            "impression": self.get_match_field_regular(),
            "findings": self.get_match_field_regular(),
            "clinic_info": self.get_match_field_regular(),
            "reporting_at_date": self.get_match_field_regular(),
            "reporting_radiologist_name": self.get_match_field_regular(),
            "patients": self.get_match_field_m2m(fields=["name", "patient_id", "age", "date_of_birth", "phone", "weight", "height", "bp", "systolic", "diastolic"]),
            "physicians": self.get_match_field_m2m(fields=["name", "physician_id", "phone", "affilicated_facility"]),
            "facilities": self.get_match_field_m2m(fields=["name", "facility_id", "country_id", "country__name", "city_id", "city__name", "phone"])
        }