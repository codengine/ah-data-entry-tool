from django.db import models
from core.models.base_entity import BaseEntity
from core.models.city import City
from core.models.country import Country


class Facility(BaseEntity):
    name = models.CharField(max_length=128, blank=True, null=True)
    facility_id = models.CharField(max_length=128, blank=True, null=True)
    country = models.ForeignKey(Country, null=True, blank=True)
    city = models.ForeignKey(City, null=True, blank=True)
    phone = models.CharField(max_length=128, blank=True, null=True)
    email = models.EmailField(null=True, blank=True)
    address = models.TextField(max_length=500, blank=True, null=True)

    class Meta:
        app_label = 'ahdms'