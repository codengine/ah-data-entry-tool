from django.db import models
from ahdms.helpers.enums import DocumentStatus
from core.models.base_entity import BaseEntity
from ahdms.models.aws_bucket import AWSBucket
from ahdms.models.document_type import DocumentType


class AWSResourceAbstract(BaseEntity):
    bucket = models.ForeignKey(AWSBucket)
    resource_type = models.ForeignKey(DocumentType)
    resource_id = models.CharField(max_length=400, null=True, blank=True)
    resource_name = models.CharField(max_length=400)
    resource_uuid = models.CharField(max_length=400)
    resource_url = models.CharField(max_length=500)
    status = models.CharField(max_length=100, default=DocumentStatus.UNASSIGNED.value)

    class Meta:
        abstract = True

    def get_date_added(self):
        return self.date_created.strftime("%d/%m/%Y")
