from django.contrib.auth.models import User
from django.db import models

from ahdms.helpers.enums import DocumentStatus
from ahdms.models.aws_resource import AWSResource
from core.models.base_entity import BaseEntity


class UserResource(BaseEntity):
    user = models.ForeignKey(User)
    resource = models.ForeignKey(AWSResource)
    status = models.CharField(max_length=200, default=DocumentStatus.ASSIGNED.value) # Assigned, Skipped, Pending, Processed
    assign_time = models.DateTimeField(null=True, blank=True)
    last_ping_time = models.DateTimeField(null=True, blank=True)
    
    class Meta:
        app_label = 'ahdms'