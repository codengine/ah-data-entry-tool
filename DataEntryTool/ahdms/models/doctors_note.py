from django.db import models
from ahdms.models.aws_document import AWSDocument


class DoctorsNote(AWSDocument):
    history = models.TextField(blank=True, null=True)
    history_note = models.TextField(blank=True, null=True)

    class Meta:
        app_label = 'ahdms'

    def as_text(self, fields=[], m2m_fields={}, date_formats={}):
        fields = ["history", "history_note", "patients", "facilities", "physicians", "type__doc_type:Document Type"]
        dt_formats = {}
        m2m = {
            "patients": ["name", "patient_id", "age", "date_of_birth", "phone", "weight", "height", "bp", "systolic", "diastolic"],
            "facilities": ["name", "facility_id", "country_id", "country__name", "city_id", "city__name", "phone"],
            "physicians": ["name", "physician_id", "phone", "affilicated_facility"]
        }
        return super(DoctorsNote, self).as_text(fields=fields, m2m_fields=m2m, date_formats=dt_formats)

    def get_match_fields(self):
        return {
            "history": self.get_match_field_regular(),
            "history_note": self.get_match_field_regular(),
            "type_id": self.get_match_field_regular(),
            "patients": self.get_match_field_m2m(fields=["name", "patient_id", "age", "date_of_birth", "phone", "weight", "height", "bp", "systolic", "diastolic"]),
            "facilities": self.get_match_field_m2m(fields=["name", "facility_id", "country_id", "country__name", "city_id", "city__name", "phone"]),
            "physicians": self.get_match_field_m2m(fields=["name", "physician_id", "phone", "affilicated_facility"])
        }