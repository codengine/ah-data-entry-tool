from ahdms.models.aws_resource_abstract import AWSResourceAbstract


class AWSResourceArchive(AWSResourceAbstract):
    pass

    class Meta:
        app_label = 'ahdms'
