from django.contrib.auth.models import User
from django.db import models

from ahdms.helpers.enums import MatchFieldType
from ahdms.models.facility import Facility
from ahdms.models.physician import Physician
from core.models.base_entity import BaseEntity
from ahdms.models.document_type import DocumentType
from ahdms.models.patient import Patient


class Document(BaseEntity):
    user = models.ForeignKey(User)
    patients = models.ManyToManyField(Patient)
    facilities = models.ManyToManyField(Facility)
    physicians = models.ManyToManyField(Physician)
    type = models.ForeignKey(DocumentType)
    comment = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=100)
    processed_document_id = models.IntegerField(default=0)
    notes = models.TextField(blank=True, null=True)

    def get_doc_type_verbal(self):
        return self.type.doc_type

    def get_match_field_regular(self):
        REGULAR = {
            "type": MatchFieldType.REGULAR.value,
            "fields": None
        }
        return REGULAR

    def get_match_field_m2m(self, fields=[]):
        M2M = {
            "type": MatchFieldType.M2M.value,
            "fields": fields
        }
        return M2M

    def get_match_fields(self):
        return {
            # "user_id": { "type": "regular", "fields": None or [] }
            # #type can be MatchFieldType.REGULAR, MatchFieldType.M2M, MatchFieldType.FKEY
        }

    class Meta:
        abstract = True