from django.contrib.auth.models import User
from django.db import models
from core.models.base_entity import BaseEntity
from core.models.city import City
from core.models.country import Country
from ahdms.models.hospital import Hospital


class Doctor(BaseEntity):
    guid = models.CharField(max_length=32)
    user = models.ForeignKey(User, null=True, blank=True)
    hospital = models.ForeignKey(Hospital, null=True, blank=True)
    name = models.CharField(max_length=128)
    specialty = models.CharField(max_length=128, blank=True, null=True)
    city = models.ForeignKey(City, null=True, blank=True)
    country = models.ForeignKey(Country, null=True, blank=True)
    signature_id = models.IntegerField(blank=True, null=True)
    referral_code = models.IntegerField(blank=True, null=True)
    provider_id = models.IntegerField(blank=True, null=True)
    radiologist = models.SmallIntegerField(null=True, blank=True)
    phone = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        app_label = 'ahdms'