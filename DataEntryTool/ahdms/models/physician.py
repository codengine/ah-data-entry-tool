from django.db import models
from core.models.base_entity import BaseEntity


class Physician(BaseEntity):
    name = models.CharField(max_length=128, null=True, blank=True)
    physician_id = models.CharField(max_length=128, blank=True, null=True)
    phone = models.CharField(max_length=128, blank=True, null=True)
    affilicated_facility = models.CharField(max_length=128, blank=True, null=True)
    spe = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(max_length=400, blank=True, null=True)
    address = models.TextField(max_length=500, blank=True, null=True)

    class Meta:
        app_label = 'ahdms'