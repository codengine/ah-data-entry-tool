from django.db.models.query_utils import Q
from django.db import transaction
from django.conf import settings
from ahdms.helpers.enums import DocumentStatus, DocumentTypeClassName, MatchFieldType
from ahdms.models.aws_resource import AWSResource
from ahdms.models.doctors_note import DoctorsNote
from ahdms.models.laboratory import Laboratory
from ahdms.models.order_ticket import OrderTicket
from ahdms.models.other_doc import OtherDoc
from ahdms.models.prescription import Prescription
from ahdms.models.radiology import Radiology
from ahdms.models.user_resource import UserResource


class DocumentFormManager(object):

    def get_doc_type_class(self, doc_type):
        if doc_type == DocumentTypeClassName.RADIOLOGY.value:
            return Radiology
        elif doc_type == DocumentTypeClassName.PRESCRIPTION.value:
            return Prescription
        elif doc_type == DocumentTypeClassName.LABORATORY.value:
            return Laboratory
        elif doc_type == DocumentTypeClassName.DOCTORS_NOTE.value:
            return DoctorsNote
        elif doc_type == DocumentTypeClassName.OTHER_DOC.value:
            return OtherDoc
        elif doc_type == DocumentTypeClassName.ORDER_TICKET.value:
            return OrderTicket

    def check_attribute_in_queue(self, queue, instance, attr_name, index):
        attr_value = getattr(instance, attr_name)
        if index == 0:
            queue[attr_name] = attr_value
            return queue
        attr_in_queue = queue.get(attr_name)
        if attr_in_queue:
            if attr_in_queue != attr_value:
                return False
        else:
            if attr_value:
                return False
        return queue

    def check_instance_match_in_list(self, instances, match_instance, attrs=[]):
        matched_index = None
        match_queue = {}
        for index, instance in enumerate(instances):
            matched_found = True
            for attr in attrs:
                match_queue = self.check_attribute_in_queue(match_queue, instance, attr, index)
                if match_queue is False:
                    matched_found = False
                    break
            if matched_found:
                matched_index = index
                break

        if matched_index is not None:
            instances = instances[:matched_index] + instances[matched_index + 1:]
            return instances, True
        else:
            return instances, False

    def check_many_to_many_unmatched(self, match_queue, instance, m2m_instances, m2m_attr_name, index, attrs=[]):
        if index == 0:
            match_queue[m2m_attr_name] = {"count": m2m_instances.count(), "instances": m2m_instances}
            return match_queue
        attrs_in_queue = match_queue.get(m2m_attr_name)
        if attrs_in_queue:
            instances_count = m2m_instances.count()
            count_in_queue = attrs_in_queue.get("count")
            if count_in_queue != instances_count:
                return True
        else:
            if m2m_instances.count() > 0:
                return True

        match_m2m = True
        queue_instances = list(attrs_in_queue.get("instances", []))
        for index2, instance in enumerate(list(m2m_instances)):
            queue_instances, match_m2m = self.check_instance_match_in_list(queue_instances, instance, attrs)
            if not match_m2m:
                break

        if not match_m2m:
            return True
        else:
            return False

    def update_resource_status(self, aws_resource_id, status):
        resource_objects = AWSResource.objects.filter(pk=aws_resource_id, status=DocumentStatus.ASSIGNED.value)
        if resource_objects.exists():
            resource_objects.update(status=status)

    def build_error_list(self, *forms):
        error_list = []
        return error_list

    def check_if_finalized(self, aws_resource_id, document_class):
        DocumentClass = document_class
        instances = DocumentClass.objects.filter(
            Q(status=DocumentStatus.FINAL.value) & Q(aws_resource_id=aws_resource_id) & Q(processed_document_id=0))
        return instances.exists()

    def check_if_pending_yet(self, aws_resource_id, document_class):
        DocumentClass = document_class
        finalized = self.check_if_finalized(aws_resource_id=aws_resource_id, document_class=DocumentClass)
        return not finalized

    def check_if_first_instance(self, instance, document_class):
        DocumentClass = document_class
        all_document_instances = DocumentClass.objects.filter(aws_resource_id=instance.aws_resource_id)
        total_instance_count = all_document_instances.count()
        if total_instance_count == 1:
            document_instance = all_document_instances.first()
            if instance.pk == document_instance.pk:
                return True
            else:
                return False
        elif total_instance_count > 1:
            return False

    def check_if_form_should_save(self, request, resource_object_id, document_class):
        DocumentClass = document_class
        pending = self.check_if_pending_yet(resource_object_id, document_class=DocumentClass)
        instances = DocumentClass.objects.filter(~Q(status=DocumentStatus.FINAL.value) &
                                                 Q(aws_resource_id=resource_object_id) & Q(processed_document_id=0))
        if pending and instances.count() < settings.MAX_OPERATOR_FORM_MATCH:
            return True
        return False

    def commit_document_flow(self, doc_instance):
        DocumentModel = self.get_doc_type_class(doc_instance.type.doc_type)
        all_instances = DocumentModel.objects.filter(aws_resource_id=doc_instance.aws_resource_id).exclude(
            pk=doc_instance.pk)
        all_instances.update(status=DocumentStatus.PROCESSED.value, processed_document_id=doc_instance.pk)

        self.update_resource_status(doc_instance.aws_resource_id, DocumentStatus.PROCESSED.value)

    def check_if_manage_form_should_save(self, aws_resource_id, document_class):
        DocumentClass = document_class
        return not DocumentClass.objects.filter(aws_resource_id=aws_resource_id, status=DocumentStatus.FINAL.value).exists()

    def check_unmatched(self, instance):

        DocumentClass = self.get_doc_type_class(instance.type.doc_type)

        if DocumentClass.objects.filter(status=DocumentStatus.UNMATCHED.value, aws_resource_id=instance.aws_resource_id,
                                        processed_document_id=0).exists():
            return True  # Because already unmatched. So the new instance will also get the unmatch.

        unmatch_found = False
        all_document_instances = DocumentClass.objects.filter(
            ~Q(status=DocumentStatus.FINAL.value) & Q(aws_resource_id=instance.aws_resource_id) & Q(
                processed_document_id=0))

        match_queue = {}

        match_fields = None

        for index, document_instance in enumerate(all_document_instances):

            if not match_fields:
                match_fields = document_instance.get_match_fields()

            if not match_fields:
                continue

            for field_name, match_option in match_fields.items():

                if match_option["type"] == MatchFieldType.REGULAR.value:
                    match_queue = self.check_attribute_in_queue(match_queue, document_instance, field_name, index)
                    if match_queue is False:
                        unmatch_found = True
                        break

                elif match_option["type"] == MatchFieldType.M2M.value:
                    m2m_related_manager = getattr(document_instance, field_name)
                    all_m2m_objects = m2m_related_manager.all()
                    attrs = match_option.get("fields", [])
                    m2m_unmatched = self.check_many_to_many_unmatched(match_queue, document_instance, all_m2m_objects,
                                                                           field_name,
                                                                           index, attrs=attrs)
                    if m2m_unmatched is True:
                        unmatch_found = True
                        break

            if unmatch_found:
                break

        return unmatch_found

    def save_unmatched(self, request, instance):
        DocumentClass = self.get_doc_type_class(instance.type.doc_type)
        if not request.user.is_superuser:

            if self.check_if_first_instance(instance, document_class=DocumentClass):
                instance.status = DocumentStatus.PENDING.value
                instance.save()

                user_resource_objects = UserResource.objects.filter(user_id=request.user.pk,
                                                                    resource_id=instance.aws_resource_id)
                if user_resource_objects.exists():
                    user_resource_objects.update(status=DocumentStatus.PENDING.value)

            else:
                unmatch_found = self.check_unmatched(instance)
                # Find all other same instances
                if unmatch_found:
                    all_document_instances = DocumentClass.objects.filter(
                        ~Q(status=DocumentStatus.FINAL.value) & Q(aws_resource_id=instance.aws_resource_id) & Q(
                            processed_document_id=0))
                    all_document_instances.update(status=DocumentStatus.UNMATCHED.value)

                    user_resource_objects = UserResource.objects.filter(user_id=request.user.pk,
                                                                        resource_id=instance.aws_resource_id)
                    if user_resource_objects.exists():
                        user_resource_objects.update(status=DocumentStatus.PENDING.value)

                else:
                    all_document_instances = DocumentClass.objects.filter(
                        ~Q(status=DocumentStatus.FINAL.value) & Q(aws_resource_id=instance.aws_resource_id) & Q(
                            processed_document_id=0))
                    all_document_instances.update(status=DocumentStatus.MATCHED.value)
                    if all_document_instances.count() == settings.MAX_OPERATOR_FORM_MATCH:
                        # Create a new instance
                        if all_document_instances.exists():
                            new_instance = all_document_instances.first()
                            new_instance.pk = None
                            new_instance.status = DocumentStatus.FINAL.value
                            new_instance.comment = "Matched and Processed with this new instance"
                            new_instance.save()

                            all_document_instances.update(status=DocumentStatus.PROCESSED.value,
                                                          processed_document_id=new_instance.pk)

                            self.update_resource_status(instance.aws_resource_id, DocumentStatus.PROCESSED.value)

                            user_resource_objects = UserResource.objects.filter(resource_id=instance.aws_resource_id)
                            if user_resource_objects.exists():
                                user_resource_objects.update(status=DocumentStatus.PROCESSED.value)
        else:
            pass

    """
    {
        "base_form": RadiologyModelForm,
        "formsets": [{"class": PatientFormSet, "related_m2m_field": "patients", "form_prefix": "radiology-patient-form"}]
    }
    """
    def pass_total_forms(self):
        return {

        }

    def handle_form_save(self, request, document_class, manage=False):
        DocumentClass = document_class

        forms_to_be_processed = self.pass_total_forms()
        BaseForm = forms_to_be_processed.get("base_form")
        if not BaseForm:
            return None
        formsets = forms_to_be_processed.get("formsets", [])

        post_data = request.POST

        m2m_field_mapping = {}
        related_m2m_field_mapping = {}

        base_form = BaseForm(post_data)
        all_formset = []
        for formset_dict in formsets:
            FormsetClass = formset_dict.get("class")
            related_m2m_field = formset_dict.get("related_m2m_field")
            form_prefix = formset_dict.get("form_prefix")
            form_kwargs = formset_dict.get("form_kwargs")

            if any([not FormsetClass, not related_m2m_field, not form_prefix]):
                return None

            all_formset += [FormsetClass(post_data, prefix=form_prefix,
                           form_kwargs=form_kwargs)]

            m2m_field_mapping[FormsetClass.__name__] = related_m2m_field
            related_m2m_field_mapping[related_m2m_field] = FormsetClass

        if base_form.is_valid() and all([formset_instance.is_valid() for formset_instance in all_formset]):
            with transaction.atomic():
                if manage:
                    form_should_save = self.check_if_manage_form_should_save(
                        base_form.cleaned_data["ah_aws_resource_id"],
                        document_class=DocumentClass)

                    if not form_should_save:
                        return 999

                    previous_document_id = base_form.cleaned_data.get("doc_id")
                    if not previous_document_id:
                        return 1002

                    m2m_instance_mapping = {}

                    for formset_instance in all_formset:
                        formset_class = formset_instance.__class__
                        formset_class_name = formset_class.__name__

                        for form in formset_instance.forms:
                            form.pk = None
                            if formset_class_name not in m2m_instance_mapping:
                                m2m_instance_mapping[formset_class_name] = [form.save()]
                            else:
                                m2m_instance_mapping[formset_class_name] += [form.save()]

                    previous_document_id = base_form.cleaned_data.get("doc_id")
                    base_form.pk = None
                    base_form.comment = "Unmatch commit by manager. %s document edited by manager %s" % (
                        previous_document_id, request.user.email)

                    base_form_instance = base_form.save()

                    base_form_instance.status = DocumentStatus.FINAL.value
                    base_form_instance.save()

                    for formset_class_name, m2m_instances in m2m_instance_mapping.items():
                        related_m2m_field_name = m2m_field_mapping.get(formset_class_name)
                        if not related_m2m_field_name:
                            return None

                        m2m_manager = getattr(base_form_instance, related_m2m_field_name)
                        m2m_manager.add(*m2m_instances)

                    self.commit_document_flow(doc_instance=base_form_instance)

                    return base_form_instance

                else:

                    base_form_instance = base_form.save()

                    m2m_instance_mapping = {}

                    for formset_instance in all_formset:
                        formset_class = formset_instance.__class__
                        formset_class_name = formset_class.__name__

                        for form in formset_instance.forms:
                            form.pk = None
                            if formset_class_name not in m2m_instance_mapping:
                                m2m_instance_mapping[formset_class_name] = [form.save()]
                            else:
                                m2m_instance_mapping[formset_class_name] += [form.save()]

                    for formset_class_name, m2m_instances in m2m_instance_mapping.items():
                        related_m2m_field_name = m2m_field_mapping.get(formset_class_name)
                        if not related_m2m_field_name:
                            return None

                        m2m_manager = getattr(base_form_instance, related_m2m_field_name)
                        m2m_manager.add(*m2m_instances)

                    # self.save_unmatched(request, instance=base_form_instance)

                    return base_form_instance
        else:
            self.error_list = self.build_error_list()
            return None

    def save_form(self, request, document_class):
        try:
            if request.user.is_superuser:

                is_manage = request.POST.get("is_managed")
                try:
                    is_manage = int(is_manage)
                    if is_manage != 1:
                        return 1002
                except Exception as exp:
                    return 1001

                saved_instance = self.handle_form_save(request=request, document_class=document_class,
                                                       manage=True)

                return saved_instance

            else:
                aws_resource_id = request.POST.get("ah_aws_resource_id")
                if not aws_resource_id:
                    return None

                try:
                    aws_resource_id = int(aws_resource_id)
                except Exception as exp:
                    return None

                saved_instance = self.handle_form_save(request=request, document_class=document_class,
                                                       manage=False)

                self.save_unmatched(request=request, instance=saved_instance)

                return saved_instance
        except Exception as exp:
            pass
