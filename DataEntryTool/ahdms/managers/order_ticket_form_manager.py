from ahdms.forms.formsets.doctor_model_formset import DoctorModelFormSet
from ahdms.forms.formsets.facility_formset import FacilityFormSet
from ahdms.forms.formsets.patient_formset import PatientFormSet
from ahdms.forms.formsets.physician_formset import PhysicianFormSet
from ahdms.forms.modelforms.order_ticket_forms import OrderTicketModelForm
from ahdms.managers.document_form_manager import DocumentFormManager


class OrderTicketFormManager(DocumentFormManager):

    def __init__(self, **kwargs):
        pass

    def pass_total_forms(self):
        return {
            "base_form": OrderTicketModelForm,
            "formsets": [
                {
                    "class": PatientFormSet,
                    "form_prefix": "order-ticket-patient-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "patients"
                },
                {
                    "class": FacilityFormSet,
                    "form_prefix": "order-ticket-facility-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "facilities"
                },
                {
                    "class": PhysicianFormSet,
                    "form_prefix": "order-ticket-physician-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "physicians"
                },
                {
                    "class": DoctorModelFormSet,
                    "form_prefix": "order-ticket-doctor-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "doctors"
                },
            ]
        }