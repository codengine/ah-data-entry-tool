from ahdms.forms.formsets.facility_formset import FacilityFormSet
from ahdms.forms.formsets.patient_formset import PatientFormSet
from ahdms.forms.formsets.physician_formset import PhysicianFormSet
from ahdms.forms.modelforms.radiology_forms import RadiologyModelForm
from ahdms.managers.document_form_manager import DocumentFormManager


class RadiologyFormManager(DocumentFormManager):

    def __init__(self, **kwargs):
        pass

    def pass_total_forms(self):
        return {
            "base_form": RadiologyModelForm,
            "formsets": [
                {
                    "class": PatientFormSet,
                    "form_prefix": "radiology-patient-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "patients"
                },
                {
                    "class": FacilityFormSet,
                    "form_prefix": "radiology-facility-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "facilities"
                },
                {
                    "class": PhysicianFormSet,
                    "form_prefix": "radiology-physician-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "physicians"
                }
            ]
        }



