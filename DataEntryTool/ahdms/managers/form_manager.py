from django.urls.base import reverse

from ahdms.forms.formsets.doctor_model_formset import DoctorModelFormSet
from ahdms.forms.formsets.facility_formset import FacilityFormSet
from ahdms.forms.formsets.lab_test_formset import LaboratoryTestFormSet
from ahdms.forms.formsets.patient_formset import PatientFormSet
from ahdms.forms.formsets.physician_formset import PhysicianFormSet
from ahdms.forms.formsets.prescribed_drug_formset import PrescribedDrugFormSet
from ahdms.forms.modelforms.doctor_form import DoctorModelForm
from ahdms.forms.modelforms.doctors_note_form import DoctorsNoteModelForm
from ahdms.forms.modelforms.laboratory_forms import LaboratoryModelForm
from ahdms.forms.modelforms.order_ticket_forms import OrderTicketModelForm
from ahdms.forms.modelforms.other_doc_forms import OtherDocModelForm
from ahdms.forms.modelforms.prescription_form import PrescriptionModelForm
from ahdms.forms.modelforms.radiology_forms import RadiologyModelForm

# radiology_form, other_form, prescription_form, laboratory_form, doctors_note_form,
# doctor_form, physician_form, patient_form, facility_formsss
from ahdms.helpers.enums import DocumentType
from core.renderers.template_renderer import TemplateRenderer

radiology_form = "%s_form" % DocumentType.RADIOLOGY.value
other_doc_form = "%s_form" % DocumentType.OTHER_DOC.value
prescription_form = "%s_form" % DocumentType.PRESCRIPTION.value
laboratory_form = "%s_form" % DocumentType.LABORATORY.value
doctors_note_form = "%s_form" % DocumentType.DOCTORS_NOTE.value
doctor_form = "doctor_form"
physician_form = "physician_form"
patient_form = "patient_form"
facility_form = "facility_form"
prescribed_drug_form = "prescribed_drug_form"
order_ticket_form = "order_ticket_form"


class FormManager(object):

    @classmethod
    def find_common_initial(cls, instance):
        initial = {}
        initial["ah_user_id"] = instance.user_id
        initial["doc_id"] = instance.pk
        initial["ah_aws_resource_id"] = instance.aws_resource_id
        initial["notes"] = instance.notes
        initial["other_information"] = instance.other_information
        if instance.patients.exists():
            patients = instance.patients.all()
            patients_initial = []
            for patient in patients:
                patients_initial += [
                    {
                        "name": patient.name,
                        "patient_id": patient.patient_id,
                        "date_of_birth": patient.date_of_birth.strftime("%m/%d/%Y") if patient.date_of_birth else "",
                        "phone": patient.phone
                    }
                ]
            initial["patients"] = patients_initial
        else:
            initial["patients"] = []

        return initial

    @classmethod
    def prepare_patient_initial_formset(cls, instance, prefix):
        prefix = str(prefix)
        patients_count = instance.patients.count()
        patients_data = {
            '%s-TOTAL_FORMS' % prefix: '%s' % patients_count,
            '%s-INITIAL_FORMS' % prefix: '%s' % patients_count,
            '%s-MAX_NUM_FORMS' % prefix: '%s' % 100,
        }
        for index, patient_instance in enumerate(instance.patients.all()):
            date_of_birth = patient_instance.date_of_birth
            if date_of_birth:
                date_of_birth = date_of_birth.strftime("%m/%d/%Y")
            patients_data["%s-%s-id" % (prefix, index)] = patient_instance.pk
            patients_data["%s-%s-name" % (prefix, index)] = patient_instance.name
            patients_data["%s-%s-patient_id" % (prefix, index)] = patient_instance.patient_id
            patients_data["%s-%s-date_of_birth" % (prefix, index)] = date_of_birth
            patients_data["%s-%s-phone" % (prefix, index)] = patient_instance.phone

        patient_formset_instance = PatientFormSet(prefix=prefix, data=patients_data)

        return patient_formset_instance

    @classmethod
    def prepare_order_ticket_doctor_initial_formset(cls, instance, prefix):
        prefix = str(prefix)
        doctors_count = instance.doctors.count()
        doctors_data = {
            '%s-TOTAL_FORMS' % prefix: '%s' % doctors_count,
            '%s-INITIAL_FORMS' % prefix: '%s' % doctors_count,
            '%s-MAX_NUM_FORMS' % prefix: '%s' % 100,
        }
        for index, doctor_instance in enumerate(instance.doctors.all()):
            doctors_data["%s-%s-id" % (prefix, index)] = doctor_instance.pk
            doctors_data["%s-%s-name" % (prefix, index)] = doctor_instance.name
            doctors_data["%s-%s-phone" % (prefix, index)] = doctor_instance.phone

        doctor_formset_instance = DoctorModelFormSet(prefix=prefix, data=doctors_data)

        return doctor_formset_instance

    @classmethod
    def prepare_facilities_initial_formset(cls, instance, prefix):
        facilities_count = instance.facilities.count()
        facilities_data = {
            '%s-TOTAL_FORMS' % prefix: '%s' % facilities_count,
            '%s-INITIAL_FORMS' % prefix: '%s' % facilities_count,
            '%s-MAX_NUM_FORMS' % prefix: '%s' % 100,
        }

        for index, facility_instance in enumerate(instance.facilities.all()):
            facilities_data['%s-%s-id' % (prefix, index)] = facility_instance.pk
            facilities_data['%s-%s-country' % (prefix, index)] = facility_instance.country_id
            facilities_data['%s-%s-city' % (prefix, index)] = facility_instance.city_id
            facilities_data['%s-%s-name' % (prefix, index)] = facility_instance.name
            facilities_data['%s-%s-facility_id' % (prefix, index)] = facility_instance.facility_id
            facilities_data['%s-%s-phone' % (prefix, index)] = facility_instance.phone
            facilities_data['%s-%s-email' % (prefix, index)] = facility_instance.email
            facilities_data['%s-%s-address' % (prefix, index)] = facility_instance.address

        facility_formset_instance = FacilityFormSet(prefix=prefix, data=facilities_data)

        return facility_formset_instance

    @classmethod
    def prepare_physician_initial_formset(cls, instance, prefix):
        physicians_count = instance.physicians.count()
        physicians_data = {
            '%s-TOTAL_FORMS' % prefix: '%s' % physicians_count,
            '%s-INITIAL_FORMS' % prefix: '%s' % physicians_count,
            '%s-MAX_NUM_FORMS' % prefix: '%s' % 100,
        }

        for index, physician_instance in enumerate(instance.physicians.all()):
            physicians_data['%s-%s-id' % (prefix, index)] = physician_instance.pk
            physicians_data['%s-%s-name' % (prefix, index)] = physician_instance.name
            physicians_data['%s-%s-physician_id' % (prefix, index)] = physician_instance.physician_id
            physicians_data['%s-%s-phone' % (prefix, index)] = physician_instance.phone
            physicians_data['%s-%s-affilicated_facility' % (prefix, index)] = physician_instance.affilicated_facility
            physicians_data['%s-%s-spe' % (prefix, index)] = physician_instance.spe
            physicians_data['%s-%s-email' % (prefix, index)] = physician_instance.email
            physicians_data['%s-%s-address' % (prefix, index)] = physician_instance.address

        physician_formset_instance = PhysicianFormSet(prefix=prefix, data=physicians_data)

        return physician_formset_instance

    @classmethod
    def prepare_prescribe_drug_initial_formset(cls, instance, prefix):
        drugs_count = instance.drugs.count()

        drugs_data = {
            '%s-TOTAL_FORMS' % prefix: '%s' % drugs_count,
            '%s-INITIAL_FORMS' % prefix: '%s' % drugs_count,
            '%s-MAX_NUM_FORMS' % prefix: '%s' % 100,
        }

        for index, drug_instance in enumerate(instance.drugs.all()):
            drugs_data["%s-%s-id" % (prefix, index)] = drug_instance.pk
            drugs_data["%s-%s-drug_name" % (prefix, index)] = drug_instance.drug_name
            drugs_data["%s-%s-dosage_number" % (prefix, index)] = drug_instance.dosage_number
            drugs_data["%s-%s-dosage_unit" % (prefix, index)] = drug_instance.dosage_unit
            drugs_data["%s-%s-frequency" % (prefix, index)] = drug_instance.frequency
            drugs_data["%s-%s-duration" % (prefix, index)] = drug_instance.duration
            drugs_data["%s-%s-route" % (prefix, index)] = drug_instance.route
            drugs_data["%s-%s-indication" % (prefix, index)] = drug_instance.indication
            drugs_data["%s-%s-special_instruction" % (prefix, index)] = drug_instance.special_instruction

        drug_formset_instance = PrescribedDrugFormSet(prefix=prefix, data=drugs_data)

        return drug_formset_instance

    @classmethod
    def prepare_lab_test_initial_formset(cls, instance, prefix):
        lab_tests_count = instance.tests.count()

        lab_test_data = {
            '%s-TOTAL_FORMS' % prefix: '%s' % lab_tests_count,
            '%s-INITIAL_FORMS' % prefix: '%s' % lab_tests_count,
            '%s-MAX_NUM_FORMS': '%s' % 100
        }

        for index, lab_test_instance in enumerate(instance.tests.all()):
            lab_test_data["%s-%s-id" % (prefix, index)] = lab_test_instance.pk
            lab_test_data["%s-%s-test" % (prefix, index)] = lab_test_instance.test
            lab_test_data["%s-%s-results" % (prefix, index)] = lab_test_instance.results
            lab_test_data["%s-%s-min_value" % (prefix, index)] = lab_test_instance.min_value
            lab_test_data["%s-%s-max_value" % (prefix, index)] = lab_test_instance.max_value
            lab_test_data["%s-%s-actual_value" % (prefix, index)] = lab_test_instance.actual_value
            lab_test_data["%s-%s-unit" % (prefix, index)] = lab_test_instance.unit
            lab_test_data["%s-%s-notes" % (prefix, index)] = lab_test_instance.notes

        lab_test_formset_instance = LaboratoryTestFormSet(prefix=prefix, data=lab_test_data)

        return lab_test_formset_instance

    @classmethod
    def document_radiology_initial(cls, instance, form_title, self_instance=True):
        if self_instance:
            radiology_data = {
                "doc_id": instance.pk,
                "ah_user_id": instance.user_id,
                "doc_type_id": instance.type.doc_type,
                "ah_aws_resource_id": instance.aws_resource.pk,
                "other_information": instance.other_information,
                "comment": instance.comment,
                "notes": instance.notes,
                "radio_type": instance.radio_type,
                "clinic_info": instance.clinic_info,
                "comparison": instance.comparison,
                "findings": instance.findings,
                "impression": instance.impression,
                "imaging_date": instance.imaging_date.strftime("%m/%d/%Y") if instance.imaging_date else None
            }
            form_instance = RadiologyModelForm(instance=instance, data=radiology_data)

            patient_formset_instance = cls.prepare_patient_initial_formset(instance=instance,
                                                                           prefix="radiology-patient-form")

            facility_formset_instance = cls.prepare_facilities_initial_formset(instance=instance,
                                                                               prefix="radiology-facility-form")

            physician_formset_instance = cls.prepare_physician_initial_formset(instance=instance,
                                                                               prefix="radiology-physician-form")

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance,
                "patient_formset": patient_formset_instance,
                "facility_formset": facility_formset_instance,
                "physician_formset": physician_formset_instance,
                "form_id": "id_radiologyform",
                "doc_type": "radiology",
                "status": "unmatched"
            }
        else:
            form_initial = cls.find_common_initial(instance=instance)
            form_instance = RadiologyModelForm(data=form_initial)
            patients = form_initial.get("patients", [])
            if patients:
                patient_count = len(patients)
                patients_data = {
                    'radiology-patient-form-TOTAL_FORMS': '%s' % patient_count,
                    'radiology-patient-form-INITIAL_FORMS': '%s' % patient_count,
                    'radiology-patient-form-MAX_NUM_FORMS': '%s' % 100,
                }
            else:
                patients_data = {

                }
            for index, patient_initial in enumerate(patients):
                patients_data["radiology-patient-form-%s-name" % index] = patient_initial["name"]
                patients_data["radiology-patient-form-%s-patient_id" % index] = patient_initial["patient_id"]
                patients_data["radiology-patient-form-%s-date_of_birth" % index] = patient_initial["date_of_birth"]
                patients_data["radiology-patient-form-%s-phone" % index] = patient_initial["phone"]

            patient_formset_instance = PatientFormSet(prefix="radiology-patient-form", data=patients_data)

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance,
                "patient_formset": patient_formset_instance,
                "facility_formset": FacilityFormSet(prefix="radiology-facility-form"),
                "physician_formset": PhysicianFormSet(prefix="radiology-physician-form"),
                "form_id": "id_radiologyform",
                "doc_type": "radiology",
                "status": "unmatched"
            }

    @classmethod
    def document_prescription_initial(cls, instance, form_title, self_instance=True):
        if self_instance:
            prescription_data = {
                "doc_id": instance.pk,
                "ah_user_id": instance.user_id,
                "doc_type_id": instance.type.doc_type,
                "ah_aws_resource_id": instance.aws_resource.pk,
                "other_information": instance.other_information,
                "comment": instance.comment,
                "notes": instance.notes,
                "ordering_date": instance.ordering_date.strftime("%m/%d/%Y") if instance.ordering_date else "",
                "special_instruction": instance.special_instruction
            }
            form_instance = PrescriptionModelForm(instance=instance, data=prescription_data)

            patient_formset_instance = cls.prepare_patient_initial_formset(instance=instance,
                                                                           prefix="prescription-patient-form")

            facility_formset_instance = cls.prepare_facilities_initial_formset(instance=instance,
                                                                               prefix="prescription-facility-form")

            physician_formset_instance = cls.prepare_physician_initial_formset(instance=instance,
                                                                               prefix="prescription-physician-form")

            drug_formset_instance = cls.prepare_prescribe_drug_initial_formset(instance=instance,
                                                                               prefix="prescription-prescribed-drug-form")

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance,
                "patient_formset": patient_formset_instance,
                "facility_formset": facility_formset_instance,
                "physician_formset": physician_formset_instance,
                "prescribed_drug_formset": drug_formset_instance,
                "form_id": "id_prescriptionform",
                "doc_type": "prescription",
                "status": "unmatched"
            }
        else:
            form_initial = cls.find_common_initial(instance=instance)
            form_instance = PrescriptionModelForm(data=form_initial)
            patients = form_initial.get("patients", [])
            if patients:
                patient_count = len(patients)
                patients_data = {
                    'prescription-patient-form-TOTAL_FORMS': '%s' % patient_count,
                    'prescription-patient-form-INITIAL_FORMS': '%s' % patient_count,
                    'prescription-patient-form-MAX_NUM_FORMS': '%s' % 100,
                }
            else:
                patients_data = {
                }
            for index, patient_initial in enumerate(patients):
                patients_data["prescription-patient-form-%s-name" % index] = patient_initial["name"]
                patients_data["prescription-patient-form-%s-patient_id" % index] = patient_initial["patient_id"]
                patients_data["prescription-patient-form-%s-date_of_birth" % index] = patient_initial["date_of_birth"]
                patients_data["prescription-patient-form-%s-phone" % index] = patient_initial["phone"]

            patient_formset_instance = PatientFormSet(prefix="prescription-patient-form", data=patients_data)

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance,
                "patient_formset": patient_formset_instance,
                "facility_formset": FacilityFormSet(prefix="prescription-facility-form"),
                "physician_formset": PhysicianFormSet(prefix="prescription-physician-form"),
                "prescribed_drug_formset": PrescribedDrugFormSet(prefix="prescription-prescribed-drug-form"),
                "form_id": "id_prescriptionform",
                "doc_type": "prescription",
                "status": "unmatched"
            }

    @classmethod
    def document_laboratory_initial(cls, instance, form_title, self_instance=True):
        if self_instance:
            laboratory_data = {
                "doc_id": instance.pk,
                "ah_user_id": instance.user_id,
                "doc_type_id": instance.type.doc_type,
                "ah_aws_resource_id": instance.aws_resource.pk,
                "other_information": instance.other_information,
                "comment": instance.comment,
                "notes": instance.notes
            }
            form_instance = PrescriptionModelForm(instance=instance, data=laboratory_data)

            patient_formset_instance = cls.prepare_patient_initial_formset(instance=instance
                                                                           , prefix="laboratory-patient-form")

            facility_formset_instance = cls.prepare_facilities_initial_formset(instance=instance,
                                                                               prefix="laboratory-facility-form")

            physician_formset_instance = cls.prepare_physician_initial_formset(instance=instance,
                                                                               prefix="laboratory-physician-form")

            lab_test_formset_instance = cls.prepare_lab_test_initial_formset(instance=instance,
                                                                             prefix="laboratory-test-form")

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance,
                "patient_formset": patient_formset_instance,
                "facility_formset": facility_formset_instance,
                "physician_formset": physician_formset_instance,
                "laboratory_test_formset": lab_test_formset_instance,
                "form_id": "id_laboratoryform",
                "doc_type": "laboratory",
                "status": "unmatched"
            }

        else:
            form_initial = cls.find_common_initial(instance=instance)
            form_instance = LaboratoryModelForm(data=form_initial)
            patients = form_initial.get("patients", [])
            if patients:
                patient_count = len(patients)
                patients_data = {
                    'laboratory-patient-form-TOTAL_FORMS': '%s' % patient_count,
                    'laboratory-patient-form-INITIAL_FORMS': '%s' % patient_count,
                    'laboratory-patient-form-MAX_NUM_FORMS': '%s' % 100,
                }
            else:
                patients_data = {
                }
            for index, patient_initial in enumerate(patients):
                patients_data["laboratory-patient-form-%s-name" % index] = patient_initial["name"]
                patients_data["laboratory-patient-form-%s-patient_id" % index] = patient_initial["patient_id"]
                patients_data["laboratory-patient-form-%s-date_of_birth" % index] = patient_initial["date_of_birth"]
                patients_data["laboratory-patient-form-%s-phone" % index] = patient_initial["phone"]

            patient_formset_instance = PatientFormSet(prefix="laboratory-patient-form", data=patients_data)

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance,
                "patient_formset": patient_formset_instance,
                "facility_formset": FacilityFormSet(prefix="laboratory-facility-form"),
                "physician_formset": PhysicianFormSet(prefix="laboratory-physician-form"),
                "laboratory_test_formset": LaboratoryTestFormSet(prefix="laboratory-test-form"),
                "form_id": "id_laboratoryform",
                "doc_type": "laboratory",
                "status": "unmatched"
            }

    @classmethod
    def document_doctors_note_initial(cls, instance, form_title, self_instance=True):
        if self_instance:
            doctors_note_data = {
                "doc_id": instance.pk,
                "ah_user_id": instance.user_id,
                "doc_type_id": instance.type.doc_type,
                "ah_aws_resource_id": instance.aws_resource.pk,
                "other_information": instance.other_information,
                "comment": instance.comment,
                "notes": instance.notes,
                "history": instance.history,
                "history_note": instance.history_note
            }
            form_instance = DoctorsNoteModelForm(instance=instance, data=doctors_note_data)

            patient_formset_instance = cls.prepare_patient_initial_formset(instance=instance,
                                                                           prefix="doctors-note-patient-form")

            facility_formset_instance = cls.prepare_facilities_initial_formset(instance=instance,
                                                                               prefix="doctors-note-facility-form")

            physician_formset_instance = cls.prepare_physician_initial_formset(instance=instance,
                                                                               prefix="doctors-note-physician-form")

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance,
                "patient_formset": patient_formset_instance,
                "facility_formset": facility_formset_instance,
                "physician_formset": physician_formset_instance,
                "form_id": "id_doctorsnoteform",
                "doc_type": "doctors-note",
                "status": "unmatched"
            }
        else:
            form_initial = cls.find_common_initial(instance=instance)
            form_instance = DoctorsNoteModelForm(data=form_initial)
            patients = form_initial.get("patients", [])
            if patients:
                patient_count = len(patients)
                patients_data = {
                    'doctors-note-patient-form-TOTAL_FORMS': '%s' % patient_count,
                    'doctors-note-patient-form-INITIAL_FORMS': '%s' % patient_count,
                    'doctors-note-patient-form-MAX_NUM_FORMS': '%s' % 100,
                }
            else:
                patients_data = {
                }
            for index, patient_initial in enumerate(patients):
                patients_data["doctors-note-patient-form-%s-name" % index] = patient_initial["name"]
                patients_data["doctors-note-patient-form-%s-patient_id" % index] = patient_initial["patient_id"]
                patients_data["doctors-note-patient-form-%s-date_of_birth" % index] = patient_initial["date_of_birth"]
                patients_data["doctors-note-patient-form-%s-phone" % index] = patient_initial["phone"]

            patient_formset_instance = PatientFormSet(prefix="doctors-note-patient-form", data=patients_data)

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance,
                "patient_formset": patient_formset_instance,
                "facility_formset": FacilityFormSet(prefix="doctors-note-facility-form"),
                "physician_formset": PhysicianFormSet(prefix="doctors-note-physician-form"),
                "form_id": "id_doctorsnoteform",
                "doc_type": "doctors-note",
                "status": "unmatched"
            }

    @classmethod
    def document_other_doc_initial(cls, instance, form_title, self_instance=True):
        if self_instance:
            other_doc_data = {
                "doc_id": instance.pk,
                "ah_user_id": instance.user_id,
                "doc_type_id": instance.type.doc_type,
                "ah_aws_resource_id": instance.aws_resource.pk,
                "other_information": instance.other_information,
                "notes": instance.notes
            }
            form_instance = OtherDocModelForm(instance=instance, data=other_doc_data)

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance
            }
        else:
            form_initial = cls.find_common_initial(instance=instance)
            form_instance = OtherDocModelForm(data=form_initial)

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance
            }

    @classmethod
    def document_order_ticket_initial(cls, instance, form_title, self_instance=True):
        if self_instance:
            order_ticket_data = {
                "doc_id": instance.pk,
                "ah_user_id": instance.user_id,
                "doc_type_id": instance.type.doc_type,
                "ah_aws_resource_id": instance.aws_resource.pk,
                "other_information": instance.other_information,
                "notes": instance.notes
            }
            form_instance = OrderTicketModelForm(instance=instance, data=order_ticket_data)

            doctor_formset_instance = cls.prepare_order_ticket_doctor_initial_formset(instance=instance,
                                                                                      prefix="order-ticket-doctor-form")

            patient_formset_instance = cls.prepare_patient_initial_formset(instance=instance,
                                                                           prefix="order-ticket-patient-form")

            facility_formset_instance = cls.prepare_facilities_initial_formset(instance=instance,
                                                                               prefix="order-ticket-facility-form")

            physician_formset_instance = cls.prepare_physician_initial_formset(instance=instance,
                                                                           prefix="order-ticket-physician-form")

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance,
                'doctor_formset': doctor_formset_instance,
                "patient_formset": patient_formset_instance,
                "facility_formset": facility_formset_instance,
                "physician_formset": physician_formset_instance,
                "form_id": "id_orderticketform",
                "doc_type": "order-ticket",
                "status": "unmatched"
            }
        else:
            form_initial = cls.find_common_initial(instance=instance)
            form_instance = OrderTicketModelForm(data=form_initial)

            return {
                "form_title": form_title,
                "document_id": instance.pk,
                "post_url": reverse("document_submit_view"),
                "form": form_instance,
                'doctor_formset': DoctorModelFormSet(prefix='order-ticket-doctor-form'),
                "patient_formset": PatientFormSet(prefix="order-ticket-patient-form"),
                "facility_formset": FacilityFormSet(prefix="order-ticket-facility-form"),
                "physician_formset": PhysicianFormSet(prefix="order-ticket-physician-form"),
                "form_id": "id_orderticketform",
                "doc_type": "order-ticket",
                "status": "unmatched"
            }

    @classmethod
    def document_form_with_initial_data(cls, doc_type, instance):
        doc_type_instance = instance.type.doc_type
        if doc_type == "Radiology":
            form_title = "Radiology"
            self_instance = True
            if doc_type != doc_type_instance:
                self_instance = False
            return cls.document_radiology_initial(instance=instance, form_title=form_title,
                                                  self_instance=self_instance)
        elif doc_type == "Prescription":
            form_title = "Prescription"
            self_instance = True
            if doc_type != doc_type_instance:
                self_instance = False
            return cls.document_prescription_initial(instance=instance, form_title=form_title,
                                                     self_instance=self_instance)
        elif doc_type == "Laboratory":
            form_title = "Laboratory"
            self_instance = True
            if doc_type != doc_type_instance:
                self_instance = False
            return cls.document_laboratory_initial(instance=instance, form_title=form_title,
                                                   self_instance=self_instance)
        elif doc_type == "DoctorsNote":
            form_title = "Doctor's Note"
            self_instance = True
            if doc_type != doc_type_instance:
                self_instance = False
            return cls.document_doctors_note_initial(instance=instance, form_title=form_title,
                                                     self_instance=self_instance)
        elif doc_type == "OtherDoc":
            form_title = "Other Documents"
            self_instance = True
            if doc_type != doc_type_instance:
                self_instance = False
            return cls.document_other_doc_initial(instance=instance, form_title=form_title,
                                                  self_instance=self_instance)
        elif doc_type == "OrderTicket":
            form_title = "Order Ticket"
            self_instance = True
            if doc_type != doc_type_instance:
                self_instance = False
            return cls.document_order_ticket_initial(instance=instance, form_title=form_title,
                                                  self_instance=self_instance)

    @classmethod
    def document_forms_with_post_urls(cls, initial=True, instance=None, **kwargs):
        if initial:
            return {
                radiology_form: {
                    "post_url": reverse("document_submit_view"),
                    "form": RadiologyModelForm,
                    "patient_formset": PatientFormSet(prefix="radiology-patient-form"),
                    "facility_formset": FacilityFormSet(prefix="radiology-facility-form"),
                    "physician_formset": PhysicianFormSet(prefix="radiology-physician-form"),
                    "form_id": "id_radiologyform",
                    "doc_type": "radiology",
                    "status": "unmatched"
                },
                prescription_form: {
                    "post_url": reverse("document_submit_view"),
                    "form": PrescriptionModelForm,
                    "patient_formset": PatientFormSet(prefix="prescription-patient-form"),
                    "facility_formset": FacilityFormSet(prefix="prescription-facility-form"),
                    "physician_formset": PhysicianFormSet(prefix="prescription-physician-form"),
                    "prescribed_drug_formset": PrescribedDrugFormSet(prefix="prescription-prescribed-drug-form"),
                    "form_id": "id_prescriptionform",
                    "doc_type": "prescription",
                    "status": "unmatched"
                },
                laboratory_form: {
                    "post_url": reverse("document_submit_view"),
                    "form": LaboratoryModelForm,
                    "patient_formset": PatientFormSet(prefix="laboratory-patient-form"),
                    "facility_formset": FacilityFormSet(prefix="laboratory-facility-form"),
                    "physician_formset": PhysicianFormSet(prefix="laboratory-physician-form"),
                    "laboratory_test_formset": LaboratoryTestFormSet(prefix="laboratory-test-form"),
                    "form_id": "id_laboratoryform",
                    "doc_type": "laboratory",
                    "status": "unmatched"
                },
                doctors_note_form: {
                    "post_url": reverse("document_submit_view"),
                    "form": DoctorsNoteModelForm,
                    "patient_formset": PatientFormSet(prefix="doctors-note-patient-form"),
                    "facility_formset": FacilityFormSet(prefix="doctors-note-facility-form"),
                    "physician_formset": PhysicianFormSet(prefix="doctors-note-physician-form"),
                    "form_id": "id_doctorsnoteform",
                    "doc_type": "doctors-note",
                    "status": "unmatched"
                },
                other_doc_form: {
                    "post_url": reverse("document_submit_view"),
                    "form": OtherDocModelForm
                },
                order_ticket_form: {
                    "post_url": reverse("document_submit_view"),
                    "form": OrderTicketModelForm,
                    'doctor_formset': DoctorModelFormSet(prefix='order-ticket-doctor-form'),
                    "patient_formset": PatientFormSet(prefix="order-ticket-patient-form"),
                    "facility_formset": FacilityFormSet(prefix="order-ticket-facility-form"),
                    "physician_formset": PhysicianFormSet(prefix="order-ticket-physician-form"),
                    "form_id": "id_orderticketform",
                    "doc_type": "order-ticket",
                    "status": "unmatched"
                }
            }
        else:
            forms = {}
            if not instance:
                return forms
            forms[radiology_form] = cls.document_form_with_initial_data(doc_type="Radiology", instance=instance)
            forms[prescription_form] = cls.document_form_with_initial_data(doc_type="Prescription", instance=instance)
            forms[laboratory_form] = cls.document_form_with_initial_data(doc_type="Laboratory", instance=instance)
            forms[doctors_note_form] = cls.document_form_with_initial_data(doc_type="DoctorsNote", instance=instance)
            forms[other_doc_form] = cls.document_form_with_initial_data(doc_type="OtherDoc", instance=instance)
            forms[order_ticket_form] = cls.document_form_with_initial_data(doc_type="OrderTicket", instance=instance)
            return forms

    @classmethod
    def get_document_form_template_name(cls, doc_type):
        if doc_type == radiology_form or doc_type == DocumentType.RADIOLOGY.value:
            return "documents/radiology.html"
        elif doc_type == prescription_form or doc_type == DocumentType.PRESCRIPTION.value:
            return "documents/prescription.html"
        elif doc_type == laboratory_form or doc_type == DocumentType.LABORATORY.value:
            return "documents/laboratory.html"
        elif doc_type == doctors_note_form or doc_type == DocumentType.DOCTORS_NOTE.value:
            return "documents/doctors_note.html"
        elif doc_type == other_doc_form or doc_type == DocumentType.OTHER_DOC.value:
            return "documents/other_documents.html"
        elif doc_type == order_ticket_form or doc_type == DocumentType.ORDER_TICKET.value:
            return "documents/order_ticket_document.html"

    @classmethod
    def render_form_to_html(cls, template_name, form_dict, initial=True, form_context={}, **kwargs):
        if "preview" in kwargs:
            preview = kwargs.pop("preview")
        else:
            preview = False
        preview_context = {"preview": preview}
        context = {**form_dict, **form_context, **preview_context}
        rendered_html = TemplateRenderer.render_template(template_name=template_name, context=context)
        return rendered_html

    @classmethod
    def render_update_form_to_html(cls, doc_type, instance, form_context={}):
        form_dict = cls.document_form_with_initial_data(doc_type=doc_type, instance=instance)
        context = {**form_dict, **form_context}
        template_name = cls.get_document_form_template_name(doc_type=doc_type)
        rendered_html = TemplateRenderer.render_template(template_name=template_name, context=context)
        return rendered_html

    @classmethod
    def get_document_forms(cls, request=None, instance=None, **kwargs):
        if "preview" in kwargs:
            preview = kwargs.pop("preview")
        else:
            preview = False
        forms = {}
        if instance:
            document_forms = FormManager.document_forms_with_post_urls(initial=False, instance=instance)
        else:
            document_forms = FormManager.document_forms_with_post_urls()
        for form_name, form_context in document_forms.items():
            template_name = cls.get_document_form_template_name(form_name)
            rendered_html = FormManager.render_form_to_html(template_name=template_name,form_dict=form_context, preview=preview)
            forms[form_name] = rendered_html
        return forms
