from ahdms.forms.formsets.facility_formset import FacilityFormSet
from ahdms.forms.formsets.lab_test_formset import LaboratoryTestFormSet
from ahdms.forms.formsets.patient_formset import PatientFormSet
from ahdms.forms.formsets.physician_formset import PhysicianFormSet
from ahdms.forms.modelforms.laboratory_forms import LaboratoryModelForm
from ahdms.managers.document_form_manager import DocumentFormManager


class LaboratoryFormManager(DocumentFormManager):

    def __init__(self, **kwargs):
        pass

    def pass_total_forms(self):
        return {
            "base_form": LaboratoryModelForm,
            "formsets": [
                {
                    "class": PatientFormSet,
                    "form_prefix": "laboratory-patient-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "patients"
                },
                {
                    "class": FacilityFormSet,
                    "form_prefix": "laboratory-facility-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "facilities"
                },
                {
                    "class": PhysicianFormSet,
                    "form_prefix": "laboratory-physician-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "physicians"
                },
                {
                    "class": LaboratoryTestFormSet,
                    "form_prefix": "laboratory-test-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "tests"
                }
            ]
        }