from ahdms.forms.formsets.facility_formset import FacilityFormSet
from ahdms.forms.formsets.patient_formset import PatientFormSet
from ahdms.forms.formsets.physician_formset import PhysicianFormSet
from ahdms.forms.modelforms.doctors_note_form import DoctorsNoteModelForm
from ahdms.managers.document_form_manager import DocumentFormManager


class DoctorsNoteFormManager(DocumentFormManager):

    def __init__(self, **kwargs):
        pass

    def pass_total_forms(self):
        return {
            "base_form": DoctorsNoteModelForm,
            "formsets": [
                {
                    "class": PatientFormSet,
                    "form_prefix": "doctors-note-patient-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "patients"
                },
                {
                    "class": FacilityFormSet,
                    "form_prefix": "doctors-note-facility-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "facilities"
                },
                {
                    "class": PhysicianFormSet,
                    "form_prefix": "doctors-note-physician-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "physicians"
                }
            ]
        }