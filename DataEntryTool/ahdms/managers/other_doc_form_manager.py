from ahdms.forms.modelforms.other_doc_forms import OtherDocModelForm
from ahdms.managers.document_form_manager import DocumentFormManager


class OtherDocFormManager(DocumentFormManager):

    def __init__(self, **kwargs):
        pass

    def pass_total_forms(self):
        return {
            "base_form": OtherDocModelForm,
            "formsets": [

            ]
        }