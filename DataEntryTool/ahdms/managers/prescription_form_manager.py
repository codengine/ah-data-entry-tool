from ahdms.forms.formsets.facility_formset import FacilityFormSet
from ahdms.forms.formsets.patient_formset import PatientFormSet
from ahdms.forms.formsets.physician_formset import PhysicianFormSet
from ahdms.forms.formsets.prescribed_drug_formset import PrescribedDrugFormSet
from ahdms.forms.modelforms.prescription_form import PrescriptionModelForm
from ahdms.managers.document_form_manager import DocumentFormManager


class PrescriptionFormManager(DocumentFormManager):

    def __init__(self, **kwargs):
        pass

    def pass_total_forms(self):
        return {
            "base_form": PrescriptionModelForm,
            "formsets": [
                {
                    "class": PatientFormSet,
                    "form_prefix": "prescription-patient-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "patients"
                },
                {
                    "class": FacilityFormSet,
                    "form_prefix": "prescription-facility-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "facilities"
                },
                {
                    "class": PhysicianFormSet,
                    "form_prefix": "prescription-physician-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "physicians"
                },
                {
                    "class": PrescribedDrugFormSet,
                    "form_prefix": "prescription-prescribed-drug-form",
                    "form_kwargs": {'request': None},
                    "related_m2m_field": "drugs"
                }
            ]
        }