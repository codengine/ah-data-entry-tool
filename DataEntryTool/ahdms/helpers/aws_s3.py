import os
import boto
from django.conf import settings
from django.db.models import Q
from django.db.models.aggregates import Count
from boto.s3.key import Key
from ahdms.helpers.enums import DocumentStatus
from ahdms.models.aws_bucket import AWSBucket
from ahdms.models.aws_resource import AWSResource
from ahdms.models.aws_resource_archive import AWSResourceArchive
from ahdms.models.document_type import DocumentType
from ahdms.models.document_upload import DocumentUpload
from ahdms.models.user_resource import UserResource
from ahdms.models.user_skipped_resource import UserSkippedResource
from engine.clock import Clock


class AWSS3(object):
    def __init__(self):
        pass

    @classmethod
    def connect_to_s3(cls):
        s3_access_key = settings.S3_ACCESS_KEY
        s3_secret_key = settings.S3_SECRET_KEY
        s3_bucket_name = settings.S3_BUCKET_NAME

        conn = boto.connect_s3(s3_access_key, s3_secret_key)
        bucket = conn.get_bucket(s3_bucket_name)
        return conn, bucket
        
    @classmethod
    def read_resources(cls):
        try:
            conn, bucket = cls.connect_to_s3()
            bucket_entries = bucket.list()
            return bucket_entries
        except Exception as exp:
            return None

    @classmethod
    def upload_file_from_memory(cls, memory_file, file_name, resource_type, file_upload_dir=None, content_type=None):
        conn, bucket = cls.connect_to_s3()
        file_size = memory_file._size
        k = Key(bucket)
        k.key = file_name
        if content_type:
            k.set_metadata('Content-Type', content_type)

        callback = None
        md5 = None
        reduced_redundancy = None
        sent = k.set_contents_from_string(memory_file.read(), policy='public-read')
        if sent == file_size:
            aws_bucket_objects = AWSBucket.objects.filter(bucket_name=settings.S3_BUCKET_NAME)
            if aws_bucket_objects.exists():
                # Resource url
                resource_url = settings.S3_BASE + file_name
                aws_bucket_object = aws_bucket_objects.first()
                resource_objects = AWSResource.objects.filter(bucket_id=aws_bucket_object.pk,
                                                              resource_type__doc_type=resource_type,
                                                              resource_url=resource_url)
                if not resource_objects.exists():
                    doc_type_object = DocumentType.objects.filter(doc_type=resource_type).first()
                    if doc_type_object:
                        resource_object = AWSResource(bucket_id=aws_bucket_object.pk,
                                                      resource_type_id=doc_type_object.pk, resource_url=resource_url)
                        resource_object.resource_id = None
                        resource_object.resource_name = file_name
                        resource_object.resource_uuid = file_name[:file_name.rindex(".")]
                        resource_object.status = DocumentStatus.UNASSIGNED.value
                        resource_object.save()

                        return resource_object

            return True
        return False

    @classmethod
    def upload_file(cls, file, file_name, resource_type, file_upload_dir=None, content_type=None):
        conn, bucket = cls.connect_to_s3()

        # Get the file size
        try:
            file_instance = open(file, 'r+')
            size = os.fstat(file_instance.fileno()).st_size
            file_instance.close()
        except:
            # Not all file objects implement fileno(),
            # so we fall back on this
            file_instance = open(file, 'r+')
            file_instance.seek(0, os.SEEK_END)
            size = file_instance.tell()
            file_instance.close()

        k = Key(bucket)
        k.key = file_name
        if content_type:
            k.set_metadata('Content-Type', content_type)
        callback = None
        md5 = None
        reduced_redundancy = None
        file_instance = open(file, 'r+')
        sent = k.set_contents_from_filename(file, policy='public-read')
        file_instance.close()
        # file.seek(0)

        if sent == size:

            # Uploaded Successfully. Now save them in application database.
            aws_bucket_objects = AWSBucket.objects.filter(bucket_name=settings.S3_BUCKET_NAME)
            if aws_bucket_objects.exists():

                # Resource url
                resource_url = settings.S3_BASE + file_name

                aws_bucket_object = aws_bucket_objects.first()
                resource_objects = AWSResource.objects.filter(bucket_id=aws_bucket_object.pk,
                                                              resource_type__doc_type=resource_type,
                                                              resource_url=resource_url)
                if not resource_objects.exists():
                    doc_type_object = DocumentType.objects.filter(doc_type=resource_type).first()
                    if doc_type_object:
                        resource_object = AWSResource(bucket_id=aws_bucket_object.pk,
                                                      resource_type_id=doc_type_object.pk, resource_url=resource_url)
                        resource_object.resource_id = None
                        resource_object.resource_name = file_name
                        resource_object.resource_uuid = file_name[:file_name.rindex(".")]
                        resource_object.status = DocumentStatus.UNASSIGNED.value
                        resource_object.save()

                        doc_upload_objects = DocumentUpload.objects.filter(file_name=file_name)
                        if doc_upload_objects.exists():
                            doc_upload_object = doc_upload_objects.first()
                            doc_upload_object.aws_resource_id = resource_object.pk
                            doc_upload_object.save()

                        try:
                            os.remove(file)
                        except OSError:
                            pass

            return True
        return False
            
    @classmethod
    def mapDocumentType(cls, folder_name):
        folder_name = folder_name.lower()
        if folder_name == "doctor-notes" or folder_name == "doctors-notes":
            return "DoctorsNote"
        elif folder_name == "others":
            return "OtherDoc"
        elif folder_name == "prescription":
            return "Prescription"
        elif folder_name == "radiology":
            return "Radiology"
        elif folder_name == "laboratory":
            return "Laboratory"
        elif folder_name == "order-ticket":
            return "OrderTicket"

    @classmethod
    def get_next_resource(cls, user_id):
        try:
            # Get all user resources with assigned status
            ten_seconds_before = Clock.utc_datetime_before(seconds=10)
            all_assigned_resources = UserResource.objects.filter(status=DocumentStatus.ASSIGNED.value,
                                                                 last_ping_time__isnull=False,
                                                                 last_ping_time__lt=ten_seconds_before)

            all_assigned_resources.delete()

            all_unavailable_resource_ids = []

            all_unavailable_resource_ids += UserResource.objects.filter(Q(user_id=user_id) & Q(status=DocumentStatus.SKIPPED.value))\
                .values_list('resource_id', flat=True)

            all_unavailable_resource_ids += UserResource.objects.filter(Q(status=DocumentStatus.PROCESSED.value))\
                .values_list('resource_id', flat=True)

            all_unavailable_resource_ids += UserResource.objects.filter(status=DocumentStatus.ASSIGNED.value) \
                .values('resource_id').annotate(assigned_count=Count('resource_id')).filter(
                assigned_count__gte=settings.MAX_OPERATOR_FORM_MATCH).values_list('resource_id', flat=True)

            all_unavailable_resource_ids += UserResource.objects.filter(status=DocumentStatus.PENDING.value) \
                .values('resource_id').annotate(assigned_count=Count('resource_id')).filter(
                assigned_count__gte=settings.MAX_OPERATOR_FORM_MATCH).values_list('resource_id', flat=True)

            all_unavailable_resource_ids += UserResource.objects.filter(Q(user_id=user_id) &
                                                                        Q(status=DocumentStatus.PENDING.value)).values_list('resource_id', flat=True)

            all_unavailable_resource_ids = list(set(all_unavailable_resource_ids))

            # Get all approved resources.
            unapproved_resource_ids = DocumentUpload.objects.filter(is_approved=False,
                                                                    aws_resource__isnull=False).values_list('aws_resource_id', flat=True)

            unapproved_resource_ids = list(set(unapproved_resource_ids))

            all_unavailable_resource_ids = all_unavailable_resource_ids + unapproved_resource_ids

            aws_resources = AWSResource.objects.filter(~Q(pk__in=all_unavailable_resource_ids))

            aws_resource = aws_resources.first()

            # Update resource status
            if not UserResource.objects.filter(user_id=user_id, resource_id=aws_resource.pk).exists():
                user_resource_object = UserResource(user_id=user_id, resource_id=aws_resource.pk,
                                                    status=DocumentStatus.ASSIGNED.value,
                                                    assign_time=Clock.utc_now(), last_ping_time=Clock.utc_now())
                user_resource_object.save()

                aws_resource.status = DocumentStatus.ASSIGNED.value
                aws_resource.save()

            return aws_resource

        except Exception as exp:
            print(str(exp))
            pass
            
    @classmethod
    def get_next_resource_v1(cls, user_id):
        # Get all resources processed by the user
        try:
            # Delete all expired resources.
            ten_seconds_before = Clock.utc_datetime_before(seconds=10)

            all_expired_resources = UserResource.objects.filter(last_ping_time__isnull=False, last_ping_time__lt=ten_seconds_before)\
                .exclude(Q(resource__status=DocumentStatus.PROCESSED.value) | Q(resource__status=DocumentStatus.PENDING.value))
            all_expired_resources.delete()

            all_unavailable_resources = UserResource.objects.filter(last_ping_time__isnull=False, last_ping_time__gte=ten_seconds_before) \
                .exclude(Q(resource__status=DocumentStatus.PROCESSED.value) | Q(resource__status=DocumentStatus.PENDING.value))\
                .values('resource_id').annotate(assigned_count=Count('resource_id')).filter(assigned_count__gte=settings.MAX_OPERATOR_FORM_MATCH)

            all_resource_ids_assigned_to_user = UserResource.objects.filter(user_id=user_id).values_list('pk', flat=True)

            all_unavailable_resource_ids = []

            for resource in all_unavailable_resources:
                all_unavailable_resource_ids += [resource['resource_id']]

            all_skipped_resource_ids = UserSkippedResource.objects.filter(user_id=user_id).values_list('resource_id', flat=True)

            aws_resources = AWSResource.objects.filter(~Q(status=DocumentStatus.PROCESSED.value)\
                                                       & ~Q(status=DocumentStatus.PENDING.value)\
                                                       & ~Q(pk__in=all_unavailable_resource_ids)\
                                                       & ~Q(pk__in=all_skipped_resource_ids))

            aws_resource = aws_resources.first()

            # Update resource status
            if not UserResource.objects.filter(user_id=user_id, resource_id=aws_resource.pk).exists():
                user_resource_object = UserResource(user_id=user_id, resource_id=aws_resource.pk, assign_time=Clock.utc_now(), last_ping_time=Clock.utc_now())
                user_resource_object.save()

            aws_resource.status = DocumentStatus.ASSIGNED.value
            aws_resource.save()

            return aws_resource
        except Exception as exp:
            pass

    @classmethod
    def extract_resource(cls, resource_url):
        # https://s3-ap-southeast-1.amazonaws.com/ahpatientapp/018dcc10-d3b9-11e7-8dd7-61603bb75c8f/doctor-notes/15ad4b27-fe5c-4d2f-97b2-09fe4e524218.jpg
        try:
            image_name = None
            document_type = None
            folder_name = None
            resource_split = resource_url.split("/")
            if len(resource_split) < 3:
                return {}
            length = len(resource_split)
            image_name = resource_split[length - 1]
            document_type = resource_split[length - 2]
            folder_name = resource_split[length - 3]
            return {
                "image_name": image_name,
                "document_type": document_type,
                "folder_name": folder_name
            }
        except Exception as exp:
            return {}
            
    @classmethod
    def save_resources(cls, resource_list):
        try:
            doc_types = DocumentType.get_all_document_types()
            s3_bucket_name = settings.S3_BUCKET_NAME
            aws_bucket_objects = AWSBucket.objects.filter(bucket_name=s3_bucket_name)
            if aws_bucket_objects.exists():
                aws_bucket_object = aws_bucket_objects.first()
                active_resource_ids = []
                for resource in resource_list:
                    resource_extract = cls.extract_resource(resource_url = resource.name)
                    if resource_extract.get('image_name') and resource_extract.get('document_type')\
                            and resource_extract.get('folder_name'):
                        doc_type = resource_extract.get("document_type")
                        resource_type = AWSS3.mapDocumentType(doc_type)
                        folder_id = resource_extract.get("folder_name")
                        image_name = resource_extract.get("image_name")
                        s3_base = settings.S3_BASE
                        resource_url = s3_base + resource.name
                        resource_objects = AWSResource.objects.filter(bucket_id=aws_bucket_object.pk, resource_type__doc_type=resource_type, resource_url=resource_url)
                        if not resource_objects.exists():
                            doc_type_object = doc_types.get(resource_type)
                            if doc_type_object:
                                resource_object = AWSResource(bucket_id=aws_bucket_object.pk, resource_type_id=doc_type_object.pk, resource_url=resource_url)
                                resource_object.resource_id = folder_id
                                resource_object.resource_name = image_name
                                resource_object.resource_uuid = image_name[:image_name.rindex(".")]
                                resource_object.status = DocumentStatus.UNASSIGNED.value
                                resource_object.save()
                                active_resource_ids += [resource_object.pk]
                        else:
                            resource_object = resource_objects.first()
                            active_resource_ids += [resource_object.pk]

                archived_resources = AWSResource.objects.filter(~Q(pk__in=active_resource_ids))

                for archive_resource in archived_resources:
                    archive_instance = AWSResourceArchive()
                    archive_instance.bucket_id = archive_resource.bucket_id
                    archive_instance.resource_type_id = archive_resource.resource_type_id
                    archive_instance.resource_id = archive_resource.resource_id
                    archive_instance.resource_name = archive_resource.resource_name
                    archive_instance.resource_uuid = archive_resource.resource_uuid
                    archive_instance.resource_url = archive_resource.resource_url
                    archive_instance.status = archive_resource.status
                    archive_instance.save()

                archived_resources.delete()


        except Exception as exp:
            pass
