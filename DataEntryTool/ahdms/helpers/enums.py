from enum import Enum


class DocumentStatus(Enum):
    UNASSIGNED = "unassigned"
    PENDING = "pending"
    ASSIGNED = "assigned"
    UNMATCHED = "unmatched"
    MATCHED = "matched"
    PROCESSED = "processed"
    FINAL = "final"
    SUBMITTED = "submitted"
    SKIPPED = "skipped"


class DocumentType(Enum):
    RADIOLOGY = "radiology"
    PRESCRIPTION = "prescription"
    LABORATORY = "laboratory"
    DOCTORS_NOTE = "doctors_note"
    OTHER_DOC = "other_doc"
    ORDER_TICKET = "order_ticket"


class DocumentTypeClassName(Enum):
    RADIOLOGY = "Radiology"
    PRESCRIPTION = "Prescription"
    LABORATORY = "Laboratory"
    DOCTORS_NOTE = "DoctorsNote"
    OTHER_DOC = "OtherDoc"
    ORDER_TICKET = "OrderTicket"


class MatchFieldType(Enum):
    REGULAR = "regular"
    M2M = "many_to_many"
    FKEY = "foreign_key"


class DocumentApprovalSource(Enum):
    SOURCE_APPROVE = "approve"
    SOURCE_EDIT = "edit"


class Gender(Enum):
    MALE = "male"
    FEMALE = "female"
    OTHER = "other"


class BillTo(Enum):
    PATIENTS = "Patients"
    HOSPITAL = "Hospital"
    LAB = "Lab"
    HMO = "HMO"


class Modality(Enum):
    CT = "ct"
    MG = "mg"
    MRI = "mri"
    XRAY = "xray"
    US = "us"
    FL = "fl"


class Examination(Enum):
    ECG = "ecg"
    STRESS_TEST = "stress_test"
    ECOCARDIOGRAPHY = "echocardiography"
    OTHER = "other"


class Routing(Enum):
    APOLLO = "Apollo"
    USA = "USA"
    RAD = "RAD"
    MAX_HEALTH = "max_health"


class TESTS(Enum):
    BLOOD = "Blood"
    SUGAR = "Sugar"


class RequestSource(Enum):
    WEB = "Web"
    API = "API"
