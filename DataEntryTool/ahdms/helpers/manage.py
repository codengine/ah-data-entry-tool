from django.urls.base import reverse


class MenuHelper(object):

    @classmethod
    def prepare_child_url(cls, manage_area, status, doc_type):
        return reverse("manage_view", kwargs={"manage_area": manage_area}) + "?doc-type=%s&status=%s" % ( doc_type, status)

    @classmethod
    def prepare_label(cls, label):
        new_label = " ".join(label.split("_"))
        new_label = new_label.capitalize()
        return new_label

    @classmethod
    def url_to_name(cls, label):
        return "_".join(label.split("-"))

    @classmethod
    def name_to_url(cls, name):
        return "-".join(name.split("_"))

    @classmethod
    def prepare_menu_item(cls, manage_area, status, children=[]):
        return {
            "label": cls.prepare_label(status),
            "name": status,
            "children": [
                {
                    "label": cls.prepare_label(child),
                    "url": cls.prepare_child_url(manage_area, status, cls.name_to_url(child)),
                    "name": child
                } for child in children
                ]
        }

    @classmethod
    def prepare_left_menu_resource_items(cls, manage_area):
        all_resource = "all"
        processed_resource = "processed"
        archived_resource = "archived"
        resource_processing = "assigned"

        doc_type_radiology = "radiology"
        doc_type_prescription = "prescription"
        doc_type_laboratory = "laboratory"
        doc_type_doctors_note = "doctors_note"
        doc_type_other_document = "other_document"

        doc_types = [doc_type_radiology, doc_type_prescription, doc_type_laboratory, doc_type_doctors_note,
                     doc_type_other_document]


    @classmethod
    def prepare_left_menu_document_items(cls, manage_area):
        unassigned_status = "unassigned"
        assigned_status = "assigned"
        pending_status = "pending"
        mismatched_status = "unmatched"
        matched_status = "matched"
        processed_status = "processed"

        doc_type_radiology = "radiology"
        doc_type_prescription = "prescription"
        doc_type_laboratory = "laboratory"
        doc_type_doctors_note = "doctors_note"
        doc_type_other_document = "other_document"

        doc_types = [doc_type_radiology, doc_type_prescription, doc_type_laboratory, doc_type_doctors_note,
                     doc_type_other_document]

        return {
            unassigned_status: cls.prepare_menu_item(manage_area, unassigned_status, doc_types),
            assigned_status: cls.prepare_menu_item(manage_area, assigned_status, doc_types),
            pending_status: cls.prepare_menu_item(manage_area, pending_status, doc_types),
            mismatched_status: cls.prepare_menu_item(manage_area, mismatched_status, doc_types),
            matched_status: cls.prepare_menu_item(manage_area, matched_status, doc_types),
            processed_status: cls.prepare_menu_item(manage_area, processed_status, doc_types)
        }
