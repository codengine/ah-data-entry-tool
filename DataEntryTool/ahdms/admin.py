from django.contrib import admin
from ahdms.models.document_type import DocumentType
from ahdms.models.aws_bucket import AWSBucket
from ahdms.models.doctor import Doctor
from ahdms.models.doctors_note import DoctorsNote
from ahdms.models.facility import Facility
from ahdms.models.laboratory import Laboratory
from ahdms.models.other_doc import OtherDoc
from ahdms.models.patient import Patient
from ahdms.models.physician import Physician
from ahdms.models.radiology import Radiology

admin.site.register(DocumentType)
admin.site.register(AWSBucket)
admin.site.register(Doctor)
admin.site.register(Patient)
admin.site.register(Physician)
admin.site.register(Facility)
admin.site.register(Radiology)
admin.site.register(Laboratory)
admin.site.register(DoctorsNote)
admin.site.register(OtherDoc)
