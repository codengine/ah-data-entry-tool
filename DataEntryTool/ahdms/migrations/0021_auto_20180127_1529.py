# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-01-27 15:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ahdms', '0020_documentupload_original_file_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='awsresource',
            name='resource_id',
            field=models.CharField(blank=True, max_length=400, null=True),
        ),
        migrations.AlterField(
            model_name='awsresourcearchive',
            name='resource_id',
            field=models.CharField(blank=True, max_length=400, null=True),
        ),
    ]
