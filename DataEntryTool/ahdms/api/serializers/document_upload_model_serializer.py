from rest_framework import serializers
from ahdms.models.document_upload import DocumentUpload
from core.api.serializers.base_model_serializer import BaseModelSerializer


class DocumentUploadModelSerializer(BaseModelSerializer):
    doc_id = serializers.IntegerField(required=False)

    class Meta:
        model = DocumentUpload
        fields = ('doc_id', 'original_file_name')

    def create(self, validated_data):
        pass