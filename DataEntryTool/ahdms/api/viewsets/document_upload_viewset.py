from ahdms.api.serializers.document_upload_model_serializer import DocumentUploadModelSerializer
from ahdms.models.document_upload import DocumentUpload
from core.api.viewsets.base_model_viewset import BaseModelViewSet


class DocumentUploadViewset(BaseModelViewSet):
    serializer_class = DocumentUploadModelSerializer
    queryset = DocumentUpload.objects.all()

    # def create(self, request):
    #     pass