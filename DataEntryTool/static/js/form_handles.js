$(document).ready(function () {

    $("#id_resource_image_container").sticky({topSpacing:0});

    window.refresh_disabled = function() {
        $(".form-control").prop("disabled", false);
        $(".hide").find(".form-control").prop("disabled", true);
        $(".hide").find(".form-control").removeClass("error");
    };

    var resource_available = false;

    function ping_resource() {

        if(resource_available) {
            var url = $("#id_resource_container").data("resource-ping-url");
            var resource_id = $("#id_aws_resource_id_global").val();
            if(resource_id != "") {
                var request_url = url + "?resource-id=" + resource_id;
                call_ajax("GET", request_url, {}, function (data) {

                },
                function (jqxhr, status, error) {

                },
                function (msg) {

                });
            }
        }

    }

    setInterval(ping_resource, 4500);

    function handle_skip_resource() {
        $(".skip-btn").prop("disabled", true);
         var skip_url = $("#id_resource_container").data("resource-skip-url");
         var resource_id = $("#id_aws_resource_id_global").val();
         if(resource_id != "") {
                var request_url = skip_url + "?resource-id=" + resource_id;
                call_ajax("GET", request_url, {}, function (data) {
                    if(data.status == "SUCCESS") {
                        handle_process_next_resource();
                    }
                    $(".skip-btn").prop("disabled", false);
                },
                function (jqxhr, status, error) {
                    $(".skip-btn").prop("disabled", false);
                },
                function (msg) {

                });
            }
    }

    $(".skip-btn").click(function (e) {
        e.preventDefault();
        handle_skip_resource();
        return false;
    });

    function show_form(option, clear_fields) {
        $(".document-form").addClass("hide");
        var form_id = "";
        if(option == "Radiology") {
            form_id = "id_radiology_form";
        }
        else if(option == "Prescription") {
            form_id = "id_prescription_form";
        }
        else if(option == "Laboratory") {
            form_id = "id_laboratory_form";
        }
        else if(option == "DoctorsNote") {
            form_id = "id_doctors_note_form";
        }
        else if(option == "OtherDoc") {
            form_id = "id_other_doc_form";
        }
        else if(option == "OrderTicket") {
            form_id = "id_order_ticket_form";
        }
        if(form_id != "") {

            if(clear_fields) {
                $("#"+form_id).find(".form-control").val("");
            }

            $("#"+form_id).removeClass("hide").fadeIn(1000);
            $("#"+form_id).find("#id_doc_type").val(option);
            $("#"+form_id).find("#id_doc_type_id").val(option);
            $("#"+form_id).find("#id_ah_user_id").val(window.id_user_id);
            $("#"+form_id).find("#id_ah_aws_resource_id").val(window.id_aws_resource_id);
            $("#"+form_id).find("#id_resource_url").val(window.id_resource_url);
            $("#"+form_id).find("#id_patient_id").val(window.id_patient_id);
        }
        setTimeout(refresh_disabled, 1100);
    }

    $("#id_doc_type").change(function (e) {
        e.preventDefault();
        var selected_item = $(this).val();
        show_form(selected_item, true);
    });

    window.get_aws_resource = function(tier) {
        var next_resource_url = $("#id_resource_container").data("next-resource-url");
        var is_manage = $("#id_resource_container").data("manage");
        var request_data = {tier: tier};
        if(is_manage == 1) {
            var doc_type = $("#id_resource_container").data("doc-type");
            var doc_id = $("#id_resource_container").data("doc-id");
            request_data = { tier: tier, doc_type: doc_type, doc_id: doc_id };
        }
        call_ajax("GET", next_resource_url, request_data, function (data) { //tier 1,2,3. 1 means initial, 2 means previous, 3 means next
            console.log(request_data);
            if(data.code == 4004) {
                console.log("No Data");
                alert("No document found to process.");
                $(document).find(".process-next-resource").remove();
                $(document).find(".skip-btn").remove();
                if(request_data.tier != 1) {
                    location.reload();
                }
                $("#id_doc_type").html("");
                $("#id_ah_user_id").val("");
                $("#id_ah_aws_resource_id").val("");
                $("#id_patient_id").val("");
                $("#id_aws_resource_id_global").val("");
                $("#id_resource_url").attr("src", "");
                $(".document-form").addClass("hide");
                $("#id_loading_container").hide();
                hide_overlay();
                return;
            }
            var resource_url = data.resource_url;
            var patient_id = data.patient_id;
            var resource_id = data.resource_id;
            var resource_type = data.resource_type_name;
            var user_id = data.user_id;
            var is_superuser = data.is_superuser;

            $("#id_doc_type").val(resource_type);
            $("#id_doc_type_id").val(resource_type);
            if(is_superuser == 0) {
                $('#id_doc_type').html('<option selected value="'+ resource_type +'">'+ resource_type +'</option>');
            }

            $("#id_ah_user_id").val(user_id);
            $("#id_ah_aws_resource_id").val(resource_id);

            $("#id_resource_url").prop("src", resource_url);
            if(patient_id != "") {
                $("#id_patient_id").text(patient_id);
            }
            else {
                $("#id_patient_id").parent().hide();
            }
            $("#id_loading_container").hide();
            $("#id_resource_container").fadeIn(1000);

            resource_available = true;
            $("#id_aws_resource_id_global").val(resource_id);

            window.id_doc_type = resource_type;
            window.id_doc_type_id = resource_type;
            window.id_user_id = user_id;
            window.id_aws_resource_id = resource_id;
            window.id_resource_url = resource_url;
            window.id_patient_id = patient_id;

            if(is_superuser == 0) {
                show_form(id_doc_type, false);
            }
            else {
                show_form(id_doc_type, false);
            }

            $(".skip-btn").parent().parent().parent().show();

            hide_overlay();
        },
        function (jqxhr, status, error) {

        },
        function (msg) {
            $("#id_loading_conid_resource_urlroptainer").hide();
            $("#id_resource_container").fadeIn(1000);
        })
    }

    function trigger_resource_download() {

        var resource_url = $("#id_resource_container").data("resource-download-url");

        call_ajax("GET", resource_url, {}, function (data) {
            var tier = 1;
            get_aws_resource(tier);
        },
        function (jqxhr, status, error) {

        },
        function (msg) {

        })
    }

    var is_manage = $("#id_resource_container").data("manage");
    if(is_manage == "1") {
        var doc_type = $("#id_resource_container").data("doc-type");
        $("#id_loading_container").hide();
        show_form(doc_type, false);
        $("#id_resource_container").fadeIn(1000);
    }
    else {
        setTimeout(trigger_resource_download, 1000);
    }


    //$(".select2").select2();

    $(document).on("click", ".overlay-close", function (e) {
        hide_overlay();
        return false;
    });

    function handle_process_next_resource() {
        resource_available = false;
        hide_overlay();
        show_overlay_only_for("loading");
        var tier = 1;
        get_aws_resource(tier);
    }

    $(document).on("click", ".process-next-resource", function (e) {
        handle_process_next_resource();
        return false;
    });

});