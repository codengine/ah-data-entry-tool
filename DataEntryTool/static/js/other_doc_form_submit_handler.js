$(document).ready(function () {
    function submit_handler_other_doc(e) {
        e.preventDefault();

        var form_id = "id_otherdocform";

        var $form_instance = $("#"+form_id);

        var confidence = "";
        if($(this).hasClass("sure")) {
            confidence = "sure";
        }
        else if($(this).hasClass("slight-unsure")) {
            confidence = "slight unsure";
        }
        else {
            confidence = "unsure";
        }

        var is_manage = $("#id_resource_container").data("manage");

        $form_instance.find("#id_is_managed").val(is_manage);

        $form_instance.find("#id_comment").val(confidence);

        var valid = window.validate_form_submission(form_id);

        if(valid) {
            $("#id_full_overlay").show();
            $("#id_full_overlay").find(".loader").show();
            $("#id_full_overlay").find(".saving-text").show();
            $("#id_error_overlay").hide();
            $("#id_success_overlay").hide();
            var action_url = $form_instance.data("action");
            var form_data = $form_instance.serialize();
            call_ajax("POST", action_url, form_data, function (data) {
                if(data.status == "SUCCESS") {
                    if(data.code == 1000) {
                        show_overlay_only_for("success");
                    }
                    else {
                        show_overlay_only_for("server_error");
                    }
                }
                else {
                    show_overlay_only_for("server_error");
                }
            },
            function (jqxhr, status, error) {
                show_overlay_only_for("server_error");
            },
            function (msg) {

            })
        }
        else {
            window.show_overlay_only_for("form_error");
            window.show_form_errors(form_id);
        }

        return false;
    }

    $(document).on("click", ".other-doc-submit", submit_handler_other_doc);
});