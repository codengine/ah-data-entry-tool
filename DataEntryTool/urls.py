from django.conf.urls import url, include
from django.contrib import admin
from ahdms.views.home_view import HomeView, AWSImageDownloadView, AWSFetchAWSResource, DocumentFormSubmitView, FetchEntityView, \
    PingResourceView, SkipResourceView
from ahdms.views.maintanence_views import RunMigrationsView
from ahdms.views.manage_views import DocumentListView, ManageUserCreateView, FetchAllSubmissionView, \
    FetchDocumentSubmissionContent, DocumentEditView, ApproveDocumentSubmission, ManageDocumentFetchListView, \
    DocumentUploadView, ManageDocumentListView, ManageDocumentPreview

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^aws-resource-download/$', AWSImageDownloadView.as_view(), name='aws_resource_download'),
    url(r'fetch-next-resource/$', AWSFetchAWSResource.as_view(), name='aws_fetch_next_resource'),
    url(r'fetch-resource/$', AWSFetchAWSResource.as_view(), name='aws_fetch_resource'),
    url(r'ping-resource/$', PingResourceView.as_view(), name='aws_ping_resource_view'),
    url(r'skip-resource/$', SkipResourceView.as_view(), name='aws_skip_resource_view'),
    url(r'manage/submissions/(?P<doc_type>[^/]+)/(?P<doc_id>[^/]+)/$', FetchAllSubmissionView.as_view(),
        name='aws_fetch_all_submissions_view'),
    url(r'manage/document-content/(?P<doc_type>[^/]+)/$', FetchDocumentSubmissionContent.as_view(),
        name='aws_fetch_submission_content_view'),
    url(r'manage/edit/documents/(?P<doc_type>[^/]+)/(?P<pk>\d+)/$', DocumentEditView.as_view(),
        name='aws_manage_edit_content_view'),
    url(r'manage/preview/documents/(?P<doc_type>[^/]+)/(?P<pk>\d+)/$', ManageDocumentPreview.as_view(),
        name='aws_manage_preview_content_view'),
    url(r'document-submit/$', DocumentFormSubmitView.as_view(), name="document_submit_view"),
    url(r'manage/document-fetch/$', ManageDocumentFetchListView.as_view(), name="manage_document_fetch_view"),
    url(r'document-submit/handle-action/(?P<action_type>[^/]+)$', ApproveDocumentSubmission.as_view(),
        name="document_submit_action_view"),
    url(r'fetch-entities/$', FetchEntityView.as_view(), name="document_fetch_view"),
    url(r'manage/document-list/$', ManageDocumentListView.as_view(), name="manage_document_list_view"),
    url(r'manage/upload-document/$', DocumentUploadView.as_view(), name="manage_document_upload_view"),
    url(r'manage/(?P<manage_area>[^/]+)/$', DocumentListView.as_view(),
        name="manage_view"),
    url(r'manage/users/create/$', ManageUserCreateView.as_view(),
        name="manage_users_create_view"),
    # url(r'^save_data/', views.save_data, name='save_data'),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^run-migrations/', RunMigrationsView.as_view(),
        name="management_run_migrations"),
]